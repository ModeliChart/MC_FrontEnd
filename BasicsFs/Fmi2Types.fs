namespace BasicsFs

module Fmi2Types =

    // The status enum from the standard
    type Fmi2Status =
        | Fmi2OK = 0
        | Fmi2Warning = 1
        | Fmi2Discard = 2
        | Fmi2Error = 3
        | Fmi2Fatal = 4
        | Fmi2Pending = 5

    // Information that can be contained in a Fm2Variable

    type IntAttributes = {
        Unit : string
        min : int
        max : int
    }

    type RealAttributes = {
        Unit : string
        min : double
        max : double
    }

    type EnumItemAttributes = {
        Name : string
        Value : int
        Description : string
    }

    type EnumAttributes = {
        Quantity : string
        Items : seq<EnumItemAttributes>
    }

    type VariableAttributes =
        | Integer of IntAttributes
        | Real of RealAttributes
        | Bool
        | String
        | Enumeration of EnumAttributes

    type SimpleType = {
        Name : string
        Description : string
        Attributes : VariableAttributes
    }  
    
    type Causality =
        | Parameter
        | CalculatedParameter
        | Input
        | Output
        | Local
        | Independent

    type Variablity =
        | ConstantExpression
        | Fixed
        | Tunable
        | Discrete
        | Continous

    // Full description of an Fmi2Variable
    type Fmi2Variable = {
        Name : string
        ValueReference : uint32
        Description : string
        Causality : Causality
        Variablity : Variablity
        Type : SimpleType
    }

    type Fmi2ModelDescription = {
        FmiVersion : string
        ModelName : string
        Guid : string
        Description : string
        Variables : seq<Fmi2Variable>
    }