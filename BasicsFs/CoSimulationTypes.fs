﻿namespace BasicsFs

module CoSimulationTypes =
    open BasicsFs.Fmi2Types  
    open FmuCli

    type FmuInstance = {
        Name : string
        Guid : string
        BinaryLocation : string
        ResourceLocation : string
    }

    // When instantiated 
    type InstantiatedState = ManagedFmu list

    // Simplified states of a CoSimulation
    type CoSimState =
        | InitialState
        | Instantiated of InstantiatedState
        | InitializationMode
        | Terminated