﻿using ModeliChart.Basics;
using System.IO.Compression;

namespace ModeliChart.FmuTools
{
    /// <summary>
    /// This class allows to repackage a model to an fmu.
    /// </summary>
    public static class FmuZipper
    {
        public static void PackToFile(IModel model, string path)
        {
            ZipFile.CreateFromDirectory(model.Location, path);
        }

        public static TempFile PackToTempFile(IModel model)
        {
            var tempFile = new TempFile();
            PackToFile(model, tempFile.PathToFile);
            return tempFile;
        }
    }
}
