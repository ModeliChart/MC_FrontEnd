﻿using ModeliChart.Basics;
using ModeliChart.Log;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModeliChart.FmuTools
{
    public static class FmuChannels
    {
        /// <summary>
        /// Converts the ModelVariables (ScalarVariables) to IChannels.
        /// Note that the DataSource has to be assigned manually.
        /// </summary>
        /// <param name="fmu">The FMUInstance which contains the information</param>
        /// <returns></returns>
        public static IEnumerable<IChannel> CreateChannels(fmiModelDescription description)
        {
            StatusLogger.Status = "Creating Channels from FMU ModelDescription.";
            // Use thread safe collection
            ConcurrentBag<IChannel> channelBag = new ConcurrentBag<IChannel>();
            var scalarVariable = description.ModelVariables.ScalarVariable;
            // Sort in the valueRefs, the order is not essential to stay the same yet
            if (scalarVariable == null)
            {
                // Can be null if there haven't been any exported Variables
                ConsoleLogger.WriteLine("FMUTools", "Warning, the selected FMU file does not contain any ScalarVariables!");
                return Enumerable.Empty<IChannel>();
            }
            // Parse the channels parallel, order does not matter
            Parallel.ForEach(scalarVariable, (item) =>
            {
                // Channel attributes
                var channel = new Basics.Channel(
                    item.name, item.valueReference, item.description, isChannelSettable(item.causality, item.variability));
                //RealChannel
                if (item.Item.GetType() == typeof(fmi2ScalarVariableReal))
                {
                    channel.ChannelType = ChannelType.Real;
                    fmi2ScalarVariableReal t = (fmi2ScalarVariableReal)item.Item;
                    if (t.declaredType != null)
                    {
                        channel.DisplayedUnit = getUnitString(description, t.declaredType);
                    }
                    else if (t.unit != null)
                    {
                        channel.DisplayedUnit = t.unit;
                    }
                }
                //IntegerChannel
                else if (item.Item.GetType() == typeof(fmi2ScalarVariableInteger))
                {
                    channel.ChannelType = ChannelType.Integer;
                    fmi2ScalarVariableInteger t = (fmi2ScalarVariableInteger)item.Item;
                    if (t.declaredType != null)
                    {
                        channel.DisplayedUnit = getUnitString(description, t.declaredType);
                    }
                }
                //EnumerationChannel
                else if (item.Item.GetType() == typeof(fmi2ScalarVariableEnumeration))
                {
                    channel.ChannelType = ChannelType.Enum;
                    fmi2ScalarVariableEnumeration t = (fmi2ScalarVariableEnumeration)item.Item;
                    Dictionary<int, string> entrys = new Dictionary<int, string>();
                    fmi2SimpleTypeEnumeration enumerationType = (fmi2SimpleTypeEnumeration)description.TypeDefinitions.SimpleType.ToDictionary(i => i.name)[t.declaredType].Item;
                    foreach (var enumEntry in enumerationType.Item)
                    {
                        entrys.Add(enumEntry.value, enumEntry.name);
                    }
                    if (t.declaredType != null)
                    {
                        channel.DisplayedUnit = getUnitString(description, t.declaredType);
                    }
                    channel = new EnumerationChannel(channel, entrys);
                }
                // Boolean Channel
                else if (item.Item.GetType() == typeof(fmi2ScalarVariableBoolean))
                {
                    channel.ChannelType = ChannelType.Boolean;
                    fmi2ScalarVariableBoolean t = (fmi2ScalarVariableBoolean)item.Item;
                    if (t.declaredType != null)
                    {
                        channel.DisplayedUnit = getUnitString(description, t.declaredType);
                    }
                }
                else
                {
                    ConsoleLogger.WriteLine("FmuTools",
                        "Warning: channel " + item.name + " (" + item.description +
                        ") is not available since only real, integer, boolean and enumeration channels are supported yet. This Channel is of type"
                        + item.Item.GetType().ToString());
                    // Do not add
                    return;
                }
                // Add the channel
                channelBag.Add(channel);
            });
            // Order by name
            return channelBag;
        }

        /// <summary>
        /// Helper Method: Will return true when the Channel is settable
        /// </summary>
        /// <param name="causality"></param>
        /// <param name="variability"></param>
        /// <returns></returns>
        private static bool isChannelSettable(fmi2ScalarVariableCausality causality, fmi2ScalarVariableVariability variability)
        {
            if (causality == fmi2ScalarVariableCausality.input ||
                                (causality == fmi2ScalarVariableCausality.parameter && variability == fmi2ScalarVariableVariability.tunable))
            {
                return true;
            }
            else { return false; }

        }

        /// <summary>
        /// Helper Method to receive the unitString from the ModelDescription String
        /// </summary>
        /// <param name="declaredType"></param>
        /// <returns></returns>
        private static string getUnitString(fmiModelDescription description, string declaredType)
        {
            string unitString = "";
            if (declaredType.Length > 0)
            {
                // Linq query to find the fitting type
                var query = from item
                            in description.TypeDefinitions.SimpleType
                            where item.name.Equals(declaredType)
                            select item;
                foreach (var item in query)
                {
                    if (item.Item.GetType() == typeof(fmi2SimpleTypeReal))
                    {
                        fmi2SimpleTypeReal t = (fmi2SimpleTypeReal)item.Item;
                        unitString = t.unit;
                    }
                    else
                    {
                        ConsoleLogger.WriteLine("FMUTools",
                            "Warning: could not get unit for " + declaredType + " since only SimpleTypeReal is implemented yet.");
                    }
                    break;
                }
            }
            return unitString;
        }

        /// <summary>
        /// Extracts all value references that belong to a integer channel (including enums).
        /// </summary>
        /// <param name="channels"></param>
        /// <returns></returns>
        public static IEnumerable<uint> GetIntVrs(IEnumerable<IChannel> channels) =>
            channels
            .Where(c => c.ChannelType == ChannelType.Integer || c.ChannelType == ChannelType.Enum)
            .Select(c => c.ValueRef);


        /// <summary>
        /// Extracts all value references that belong to a real channel.
        /// </summary>
        /// <param name="channels"></param>
        /// <returns></returns>
        public static IEnumerable<uint> GetRealVrs(IEnumerable<IChannel> channels) =>
          channels
          .Where(c => c.ChannelType == ChannelType.Real)
          .Select(c => c.ValueRef);


        /// <summary>
        /// Extracts all value references that belong to a boolean channel.
        /// </summary>
        /// <param name="channels"></param>
        /// <returns></returns>
        public static IEnumerable<uint> GetBoolVrs(IEnumerable<IChannel> channels) =>
         channels
         .Where(c => c.ChannelType == ChannelType.Boolean)
         .Select(c => c.ValueRef);
    }
}
