﻿using System;
using System.IO;

namespace ModeliChart.FmuTools
{
    /// <summary>
    /// Small IDisposable wrapper for a temporary file.
    /// The file will be deletet on disposal.
    /// </summary>
    public sealed class TempFile : IDisposable
    {
        public string PathToFile { get; private set; }

        public TempFile()
        {
            PathToFile = Path.GetTempFileName();
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        public void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }
                File.Delete(PathToFile);
                disposedValue = true;
            }
        }

        ~TempFile()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
