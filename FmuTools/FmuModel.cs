﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;

namespace ModeliChart.FmuTools
{
    /// <summary>
    /// Represents a fmu model which has been extracted and is ready to use.
    /// </summary>
    public class FmuModel : IModel
    {
        /// <summary>
        /// Create a new instance from an extracted fmu.
        /// Throws ArgumentException if the path does not contain a fmu.
        /// </summary>
        /// <param name="extractedPath"></param>
        public FmuModel(string extractedPath)
        {
            // Check the model
            if (!FmuLoader.DirectoryIsFmu(extractedPath))
            {
                throw new ArgumentException("The path is not an extracted fmu.");
            }
            Location = extractedPath;
            // Load the model
            var fmiModelDescription = FmuLoader.LoadModelDescriptionFromFolder(Location);
            Name = fmiModelDescription.modelName;
            Guid = fmiModelDescription.guid;
            if(fmiModelDescription.CoSimulation != null && fmiModelDescription.CoSimulation.Length > 0)
            {
                ModelIdentifier = fmiModelDescription.CoSimulation[0].modelIdentifier;
            }
            Channels = FmuChannels.CreateChannels(fmiModelDescription);
        }

        public string Name
        {
            get;
            private set;
        }

        public string Location { get; private set; }

        public IEnumerable<IChannel> Channels { get; private set; }

        public string ModelIdentifier { get; private set; }

        public string Guid { get; private set; }
    }
}
