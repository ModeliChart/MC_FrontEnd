﻿using System.IO;
using System.IO.Compression;
using System.Xml.Serialization;
using ModeliChart.Basics;

namespace ModeliChart.FmuTools
{
    /// <summary>
    /// Offers static functions to load the model description and the binary from a fmu.
    /// If no
    /// </summary>
    public static class FmuLoader
    {
        private const string MODELDESCRIPTION_FILENAME = "modelDescription.xml";
        private static string GetModelDescriptionPath(string directory) => Path.Combine(directory, MODELDESCRIPTION_FILENAME);

        /// <summary>
        /// Searches the model description xml.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>True if the model description exists.</returns>
        public static bool DirectoryIsFmu(string path) => File.Exists(GetModelDescriptionPath(path));

        /// <summary>
        /// Extracts only the model description without unzipping the whole fmu.
        /// </summary>
        /// <param name="pathToFmu"></param>
        public static fmiModelDescription LoadModelDescriptionFromArchive(string pathToFmu)
        {
            var archive = ZipFile.OpenRead(pathToFmu);
            using (var stream = archive.GetEntry(MODELDESCRIPTION_FILENAME).Open())
            {
                return ReadModelDescription(stream);
            }
        }

        public static fmiModelDescription LoadModelDescriptionFromFolder(string extractedModelPath)
        {
            using (var stream = File.OpenRead(GetModelDescriptionPath(extractedModelPath)))
            {
                return ReadModelDescription(stream);
            }
        }

        // Independent from source. Any stream can be deserialized.
        private static fmiModelDescription ReadModelDescription(Stream input)
        {
            var serializer = new XmlSerializer(typeof(fmiModelDescription));
            return (fmiModelDescription)serializer.Deserialize(input);
        }

        private static string CombineProcessDir(IModel model, string processDir) =>
            Path.Combine(model.Location, "binaries", processDir, model.ModelIdentifier);

        public static string GetBinaryPath(IModel model)
        {
            if (System.Environment.Is64BitProcess)
            {
                return CombineProcessDir(model, "win64");
            }
            else
            {
                return CombineProcessDir(model, "win32");
            }
        }

    }
}
