
namespace ModeliChart.Basics

module DataRepository =
    
    let AddModelInstance name instance = Map.add name instance

    let RemoveModelInstance name = Map.remove name

    let SetValue valueRef (instance:IModelInstance) value = instance.SetValueAsync(valueRef, value)

    let SetValue (channel:IChannel) = SetValue channel.ValueRef 

    let SetValue map (channel:IChannel) = 
        match Map.tryFind channel.ModelInstanceName map with
        | Some instance -> SetValue channel instance
        | None ->