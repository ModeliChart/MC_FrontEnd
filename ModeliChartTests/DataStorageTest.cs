﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModeliChart.Basics;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ModeliChartTests
{
    [TestClass]
    public class DataStorageTest
    {
        [TestMethod]
        public async Task TestBufferedSamplesTable()
        {
            // Some constants for easier test implementation
            const int BUFFER_SIZE = 10;
            const int VALUE_COUNT = 30;
            var valueRefs = new List<uint>();
            for (uint i = 0; i < 10; i++)
            {
                // Channels can share valueRefs so check adding them multiple times
                valueRefs.Add(i);
                valueRefs.Add(i);
            }

            // Create table (disposable!)
            using (var bufferedSamplesTable = new BufferedSamplesTable(valueRefs, BUFFER_SIZE))
            {
                // Run the sequence
                await testSequence(bufferedSamplesTable, valueRefs, BUFFER_SIZE, VALUE_COUNT);
                // Clear the storage
                bufferedSamplesTable.Clear();
                // Run the sequence on the cleared instance
                await testSequence(bufferedSamplesTable, valueRefs, BUFFER_SIZE, VALUE_COUNT);
            }
        }

        async Task testSequence(ISamplesStorage storage, IEnumerable<uint> valueRefs, int BUFFER_SIZE, int VALUE_COUNT)
        {
            // Test empty table
            foreach (uint vr in valueRefs)
            {
                Assert.AreEqual((0, 0), storage.GetLast(vr));
                Assert.AreEqual(0, storage.GetBufferedSamples(vr).Count());
                Assert.AreEqual(0, (await storage.GetPeristentSamplesAsync(vr)).Count());
            }

            // Add data
            for (int i = 0; i < VALUE_COUNT; i++)
            {
                var values = new List<double>();
                foreach (uint vr in valueRefs)
                {
                    // Data defined by valueRef and time. Different data for different valueRefs leads to undefined behaviour
                    values.Add(vr * i);
                }
                storage.AddSamples(i, valueRefs, values);
            }

            // Read buffered data
            foreach (uint vr in valueRefs)
            {
                var values = storage.GetBufferedSamples(vr).ToList();
                Assert.AreEqual(BUFFER_SIZE, values.Count);
                for (int i = 0; i < BUFFER_SIZE; i++)
                {
                    // Tuples (Time, Value)
                    Assert.AreEqual((i + VALUE_COUNT - BUFFER_SIZE, (i + +VALUE_COUNT - BUFFER_SIZE) * vr), values[i]);
                }
            }

            // Read stored data
            foreach (uint vr in valueRefs)
            {
                var values = (await storage.GetPeristentSamplesAsync(vr)).ToList();
                Assert.AreEqual(VALUE_COUNT, values.Count);
                for (int i = 0; i < BUFFER_SIZE; i++)
                {
                    // Tuples (Time, Value)
                    Assert.AreEqual((i, vr * i), values[i]);
                }
            }
        }
    }
}

