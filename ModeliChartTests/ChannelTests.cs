﻿//using System;
//using System.Text;
//using System.Collections.Generic;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using ModeliChart;
//using ModeliChart.Basics;

//namespace ModeliChartTests
//{
//    [TestClass]
//    public class ChannelTests
//    {
//        [TestMethod]
//        public void TestNearestNeighborInterpolation()
//        {
//            //Setup
//            IChannel TestChannel = new Channel("Test Channel", "Test Channel", "[test Unit]", 0, false, true, ChannelType.Real, null);
//            TestChannel.Add(new Sample(1.0, 1.0));
//            TestChannel.Add(new Sample(2.0, 3.0));
//            TestChannel.Add(new Sample(3.0, 7.0));
//            TestChannel.Add(new Sample(3.5, 8.0));
//            TestChannel.Add(new Sample(4.4, 9.0));
//            //Assert
//            //Einfache Standardfälle
//            ISample result = TestChannel.GetNearestNeighbourAtTime(1.0);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//            result = TestChannel.GetNearestNeighbourAtTime(1.9);
//            Assert.AreEqual(result, new Sample(2.0, 3.0));
//            result = TestChannel.GetNearestNeighbourAtTime(2.4);
//            Assert.AreEqual(result, new Sample(2.0, 3.0));
//            //Sonderfälle:
//            //Zeit liegt genau in der Interval-Mitte->Wir "runden" auf
//            result = TestChannel.GetNearestNeighbourAtTime(2.5);
//            Assert.AreEqual(result, new Sample(3.0, 7.0));
//            result = TestChannel.GetNearestNeighbourAtTime(3.25);
//            Assert.AreEqual(result, new Sample(3.5, 8.0));
//            //Zeit liegt außerhalb des eingeschlossen Intervalls:
//            result = TestChannel.GetNearestNeighbourAtTime(0.5);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//            result = TestChannel.GetNearestNeighbourAtTime(5.0);
//            Assert.AreEqual(result, new Sample(4.4, 9.0));
//            result = TestChannel.GetNearestNeighbourAtTime(-1.5);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//        }
//        [TestMethod]
//        public void TestLinearInterpolation()
//        {
//            /*
//            //Setup
//            Channel TestChannel = new Channel("Test Channel", "Test Channel", "[test Unit]", 0, null);
//            Assert.IsNull(SampleInterpolator.GetInterpolatedSampleAtTime(TestChannel.GetInterpolatedSampleAtTime(2.0));
//            TestChannel.Add(new Sample(1.0, 1.0));
//           ISample result = TestChannel.GetInterpolatedSampleAtTime(1.0);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//            TestChannel.Add(new Sample(2.0, 3.0));
//            TestChannel.Add(new Sample(3.0, 7.0));
//            TestChannel.Add(new Sample(3.5, 8.0));

//            //Assert
//            //Einfache Standardfälle
//            result = TestChannel.GetInterpolatedSampleAtTime(1.0);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//            result = TestChannel.GetInterpolatedSampleAtTime(1.5);
//            Assert.AreEqual(result, new Sample(1.5, 2.0));
//            result = TestChannel.GetInterpolatedSampleAtTime(1.9);
//            Assert.AreEqual(result, new Sample(1.9, 2.8));
//            //Zeit liegt außerhalb des eingeschlossen Intervalls:
//            result = TestChannel.GetInterpolatedSampleAtTime(0.5);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//            result = TestChannel.GetInterpolatedSampleAtTime(4.0);
//            Assert.AreEqual(result, new Sample(3.5, 8.0));
//            result = TestChannel.GetInterpolatedSampleAtTime(-1.5);
//            Assert.AreEqual(result, new Sample(1.0, 1.0));
//             * */
//        }
//        [TestMethod]
//        public void TestSampleIsEqual()
//        {
//            //Setup
//            ISample sample1 = new Sample(1.0, 0.5);
//            ISample sample2 = new Sample(1.0, 0.5);
//            ISample sample3 = new Sample(1.0, 0.4);
//            ISample sample4 = new Sample(0.9, 0.5);
//            //Assert
//            Assert.AreEqual(sample1, sample2);
//            Assert.AreNotEqual(sample1, sample3);
//            Assert.AreNotEqual(sample1, sample4);
//            Assert.AreNotEqual(sample1, new object());
//        }

//        [TestMethod]
//        public void TestGetRecentSample()
//        {
//            //Setup ("Test Channel", "Test Channel", "[test Unit]", null);
//            Channel TestChannel = new Channel("Test Channel", "Test Channel", "[test Unit]", 0, false, true, ChannelType.Real, null);
//            List<ISample> demand = new List<ISample>();

//            //Assert
//            // Keine Samples vorhanden
//            Assert.IsTrue(AreSampleListsEqual(demand, TestChannel.GetRecentPast(0.5)));

//            TestChannel.Add(new Sample(1.0, 1.0));
//            TestChannel.Add(new Sample(2.0, 3.0));
//            TestChannel.Add(new Sample(3.0, 7.0));
//            TestChannel.Add(new Sample(3.5, 8.0));
//            TestChannel.Add(new Sample(4.0, 8.0));

//            //Abbilden aller Samples 
//            demand.Add(new Sample(1.0, 1.0));
//            demand.Add(new Sample(2.0, 3.0));
//            demand.Add(new Sample(3.0, 7.0));
//            demand.Add(new Sample(3.5, 8.0));
//            demand.Add(new Sample(4.0, 8.0));
//            Assert.IsTrue(AreSampleListsEqual(demand, TestChannel.GetRecentPast(4.0)));

//            //Zeit bis in den negativen Bereich
//            Assert.IsTrue(AreSampleListsEqual(demand, TestChannel.GetRecentPast(5.0)));
//            demand.Clear();

//            /************** Mit dem aktuellen Design von Recent Past existiert diese Fälle nicht mehr ********************
//            //Startzeitpunkt liegt in der Zukunft mit Überlappung
//            demand.Add(new Sample(2.0, 3.0));
//            demand.Add(new Sample(3.0, 7.0));
//            demand.Add(new Sample(3.5, 8.0));
//            demand.Add(new Sample(4.0, 8.0));
//            Assert.IsTrue(AreSampleListsEqual(demand, TestChannel.GetRecentPast(5.0, 3.0)));
//            demand.Clear();
            
//            //Werte liegen ausschließlich in der Zukunft
//            Assert.IsTrue(AreSampleListsEqual(demand, TestChannel.GetRecentPast(5.0, 0.5)));
          
//            //Werte liegen ausschließlich in der Vergangenheit
//            Assert.IsTrue(AreSampleListsEqual(demand, TestChannel.GetRecentPast(0.75, 0.5)));
             
//            **********************************************************************************************************/
//        }

//        [TestMethod]
//        public void TestSampleData()
//        {
//            // Create a default sampleDaza with Cache Size of 3000;
//            using (var sampleData = new ChannelData())
//            {
//                // Test empty
//                Assert.IsTrue(sampleData.CacheSize == 3000);
//                List<ISample> testList = new List<ISample>();
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));

//                // Create a few samples
//                for (int i = 0; i < 10; i++)
//                {
//                    var newSample = new Sample(i, i);
//                    testList.Add(newSample);
//                    sampleData.addSample(newSample);
//                }

//                // Test equality
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.GetCache()));

//                // Cut the Cache with some samples remaining in the cache and some in csv
//                List<ISample> halfList = testList.Skip(5).ToList();
//                sampleData.CacheSize = 5;
//                // No sample shall be lost
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                // Cache size changed
//                Assert.IsTrue(AreSampleListsEqual(halfList, sampleData.GetCache()));

//                // Decrease the Size again
//                halfList = halfList.Skip(2).ToList();
//                sampleData.CacheSize = 3;
//                // No sample shall be lost
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                // Cache size changed
//                Assert.IsTrue(AreSampleListsEqual(halfList, sampleData.GetCache()));

//                // Decrease the Size again - negative -> One remains in Cache
//                halfList = testList.Skip(9).ToList();
//                sampleData.CacheSize = -3;
//                // No sample shall be lost
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                // Cache size changed
//                Assert.IsTrue(AreSampleListsEqual(halfList, sampleData.GetCache()));

//                // Increase the Size, remain inside the cache
//                halfList = testList.Skip(5).ToList();
//                sampleData.CacheSize = 5;
//                // No sample shall be lost
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                // Cache size changed
//                Assert.IsTrue(AreSampleListsEqual(halfList, sampleData.GetCache()));

//                // Increase the Size again to full cache
//                sampleData.CacheSize = 10;
//                // No sample shall be lost
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                // Cache size changed
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.GetCache()));

//                // Increase the Size again to size larger than total sample count
//                sampleData.CacheSize = 15;
//                // No sample shall be lost
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.DataList));
//                // Cache size changed
//                Assert.IsTrue(AreSampleListsEqual(testList, sampleData.GetCache()));

//                // NOTE: Find specific sample is tested in Channel tests
//            }
//        }
//        public bool AreSampleListsEqual(List<ISample> demand, List<ISample> result)
//        {
//            bool sampleListsAreEqual = true;
//            int numElements = result.Count();
//            if (demand.Count() == numElements)
//            {
//                int index = 0;
//                while (index < numElements)
//                {
//                    if (demand[index].Time != result[index].Time || demand[index].Value != result[index].Value)
//                    {
//                        return sampleListsAreEqual = false;
//                    }
//                    index++;
//                }
//                return sampleListsAreEqual;
//            }
//            else
//            {
//                return sampleListsAreEqual = false;
//            }
//        }
//    }

//}
