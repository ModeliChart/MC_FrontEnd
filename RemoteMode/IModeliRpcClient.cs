﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModeliChart.RemoteMode
{
    /// <summary>
    /// Interface to call Simulation methods on a remote (asynchronously).
    /// IDisposable suts down the connection.
    /// </summary>
    public interface IModeliRpcClient : IDisposable
    {
        /// <summary>
        /// Gets fired when new values arrived from the backend.
        /// </summary>
        event EventHandler<RemoteValuesEventArgs> NewValuesArrived;

        /// <summary>
        /// Gets fired when logging  messages arrive from the backend.
        /// </summary>
        event EventHandler<RemoteLogEventArgs> LogArrived;

        /// <summary>
        /// Connect to the target without timeout.
        /// </summary>
        /// <param name="targetUri"></param>
        /// <returns></returns>
        Task ConnectAsync();

        /// <summary>
        /// The resolved target in Uri format
        /// </summary>
        string TargetUri { get; }

        /// <summary>
        /// Play the simulation, returns a fmi2Status
        /// </summary>
        /// <returns>Fmi2Status</returns>
        Task<int> PlayAsync();

        /// <summary>
        ///  Fast forward the simulation, returns a fmi2Status.
        /// </summary>
        /// <param name="timeToPlay"></param>
        /// <returns>Fmi2Status</returns>
        Task<int> PlayFastAsync(double timeToPlay);

        /// <summary>
        /// Pause the simulation without resetting it.
        /// </summary>
        /// <returns></returns>
        Task PauseAsync();

        /// <summary>
        /// Stop & reset the simulation, returns a fmi2Status
        /// </summary>
        /// <returns>Fmi2Status</returns>
        Task<int> StopAsync();

        /// <summary>
        /// Add a fmu to the simulation, returns false on fail.
        /// </summary>
        /// <param name="instanceName"></param>
        /// <param name="path">The local file.</param>
        /// <returns></returns>
        Task<bool> AddFmuAsync(string instanceName, string path);

        /// <summary>
        /// Remove a fmu to the simulation, returns false on fail.
        /// </summary>
        /// <param name="instanceName"></param>
        /// <returns></returns>
        Task<bool> RemoveFmuAsync(string instanceName);

        /// <summary>
        /// Add a ChannelLink to the simulation.
        /// </summary>
        /// <param name="channelLink"></param>
        /// <returns>true on success.</returns>
        Task<bool> AddChannelLinkAsync(LinearChannelLink channelLink);

        /// <summary>
        /// Remove a ChannelLink from the simulation.
        /// </summary>
        /// <param name="channelLink"></param>
        /// <returns>true on success.</returns>
        Task<bool> RemoveChannelLinkAsync(LinearChannelLink channelLink);

        Task<int> SetIntValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<int> values);
        Task<int> SetRealValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<double> values);
        Task<int> SetBoolValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<int> values);
        Task<int> SetStringValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<string> values);

        Task SetStepSizeAsync(double stepSize);
    }
}
