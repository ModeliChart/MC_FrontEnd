﻿using ModeliChart.Basics;
using ModeliChart.FmuTools;
using ModeliChart.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace ModeliChart.RemoteMode
{
    public sealed class RemoteSimulation : ISimulation, IRpcSimulation
    {
        // Execute RPC calls
        private ModeliGrpcClient rpcClient;
        // Enforce that each instance name only exists once  by using it as key.
        private readonly Dictionary<string, RemoteInstance> modelInstances = new Dictionary<string, RemoteInstance>();
        private readonly List<LinearChannelLink> channelLinks = new List<LinearChannelLink>();
        private double stepSize = 0.01;
        private double currentTime = 0;

        public event EventHandler<NewValuesEventArgs> NewValuesAvailable;


        /// <summary>
        /// Create a new ISimulation instance that also implements IRocSimulation.
        /// Tries to connect the rpc system and throws an exception if it fails.
        /// </summary>
        /// <param name="targetUri"></param>
        public RemoteSimulation(string targetUri)
        {
            rpcClient = new ModeliGrpcClient(targetUri);
            rpcClient.LogArrived += Rpc_LogArrived;
            rpcClient.NewValuesArrived += RpcClient_NewValuesArrived;
        }

        private void RpcClient_NewValuesArrived(object sender, RemoteValuesEventArgs e)
        {
            // Assemble the valuerefs
            var valueRefs = new List<uint>();
            valueRefs.AddRange(e.IntegerValueRefs);
            valueRefs.AddRange(e.RealValueRefs);
            valueRefs.AddRange(e.BoolValueRefs);
            // Assemble as double values (oxyplot in mind)
            var values = new List<double>();
            values.AddRange(e.IntegerValues.Select(value => Convert.ToDouble(value)));
            values.AddRange(e.RealValues);
            values.AddRange(e.BoolValues.Select(value => Convert.ToDouble(value)));
            Interlocked.Exchange(ref currentTime, e.Timestamp);
            NewValuesAvailable?.Invoke(this,
                new NewValuesEventArgs
                {
                    ModelInstanceName = e.InstanceName,
                    Time = e.Timestamp,
                    ValueRefs = valueRefs,
                    Values = values
                });
        }

        private void Rpc_LogArrived(object sender, RemoteLogEventArgs e)
        {
            ConsoleLogger.WriteLine(e.InstanceName, "status: " + e.Status
                + ", category: " + e.Category + ", message: " + e.Message);
        }

        public IModeliRpcClient RpcClient => rpcClient;

        public string TargetUri => rpcClient.TargetUri;

        public double GetStepSize() => stepSize;

        public async Task SetStepSize(double stepSize)
        {
            if (stepSize > 0)
            {
                this.stepSize = stepSize;
                await rpcClient.SetStepSizeAsync(stepSize).ConfigureAwait(false);
            }
        }

        public IEnumerable<LinearChannelLink> ChannelLinks => channelLinks;

        public IEnumerable<IModelInstance> ModelInstances => modelInstances.Values;

        public async Task Pause()
        {
            await rpcClient.PauseAsync().ConfigureAwait(false);
        }

        public async Task Play()
        {
            // TODO include status definition in protocol
            if (await rpcClient.PlayAsync().ConfigureAwait(false) != 0)
            {
                ConsoleLogger.WriteLine("RemoteSimulation", "Play failed.");
            }
        }

        public async Task PlayFast(double interval)
        {
            // TODO include status definition in protocol
            if (await rpcClient.PlayFastAsync(interval).ConfigureAwait(false) != 0)
            {
                ConsoleLogger.WriteLine("RemoteSimulation", "PlayFast failed.");
            }
        }

        public async Task Stop()
        {
            // TODO include status definition in protocol
            if (await rpcClient.StopAsync().ConfigureAwait(false) != 0)
            {
                ConsoleLogger.WriteLine("RemoteSimulation", "Sttop failed.");
            }
            currentTime = 0;
        }

        public async Task ConnectAsync()
        {
            await rpcClient.ConnectAsync().ConfigureAwait(false);
        }

        public async Task AddChannelLink(LinearChannelLink channelLink)
        {
            // Only transfer enabled links
            if (channelLink.Enabled)
            {
                await rpcClient.AddChannelLinkAsync(channelLink).ConfigureAwait(false);
            }
            // Always add it to the local list it can be enabled later.
            channelLinks.Add(channelLink);
        }

        public async Task RemoveChannelLink(LinearChannelLink channelLink)
        {
            // Only linear channel links supported
            if (channelLink is LinearChannelLink linearChannelLink)
            {
                if (await rpcClient.RemoveChannelLinkAsync(linearChannelLink).ConfigureAwait(false))
                {
                    channelLinks.Remove(channelLink);
                }
            }
        }

        public async Task<IModelInstance> AddModelInstance(IModel model, string instanceName)
        {
            // Each datasource only once
            if (modelInstances.ContainsKey(instanceName))
            {
                return modelInstances[instanceName];
            }
            // Create and transfer
            var remoteInstance = new RemoteInstance(instanceName, model, rpcClient);
            // The fmu file must be zipped for transfer
            using (var tempFmu = FmuZipper.PackToTempFile(model))
            {
                if (await rpcClient.AddFmuAsync(instanceName, model.Location).ConfigureAwait(false))
                {
                    // Transfer is successfull
                    modelInstances.Add(instanceName, remoteInstance);
                    return remoteInstance;
                }
            }
            return null;
        }

        public async Task RemoveModelInstance(IModelInstance modelInstance)
        {
            if (await rpcClient.RemoveFmuAsync(modelInstance.Name).ConfigureAwait(false))
            {
                modelInstances.Remove(modelInstance.Name);
            }
        }

        //public void Dispose()
        //{
        //    // Depends on channels
        //    while (channelLinks.Count > 0)
        //    {
        //        RemoveChannelLink(channelLinks[0]).Wait();
        //    }
        //    var instances = modelInstances.Values;
        //    foreach (var instance in instances)
        //    {
        //        RemoveModelInstance(instance).Wait();
        //    }
        //    // Last
        //    rpcClient.Dispose();
        //}

        public Task SetValue(IChannel channel, double value) =>
            modelInstances[channel.ModelInstanceName].SetValueAsync(channel.ValueRef, value);

        public double CurrentTime => currentTime;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects).
                    // Depends on channels
                    while (channelLinks.Count > 0)
                    {
                        RemoveChannelLink(channelLinks[0]).Wait();
                    }
                    var instances = modelInstances.Values;
                    foreach (var instance in instances)
                    {
                        RemoveModelInstance(instance).Wait();
                    }
                    // Last
                    rpcClient.Dispose();
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}