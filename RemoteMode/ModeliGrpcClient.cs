﻿using Google.Protobuf;
using Grpc.Core;
using ModeliChart.Basics;
using ModeliRpc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ModeliChart.RemoteMode
{
    public sealed class ModeliGrpcClient : IModeliRpcClient
    {
        // rpc interfaces
        private ModeliBackend.ModeliBackendClient _client;
        private Grpc.Core.Channel _channel;
        // Receiver tasks
        private Task logTask;
        private Task newValuesTask;

        public string TargetUri => _channel.ResolvedTarget;

        public event EventHandler<RemoteValuesEventArgs> NewValuesArrived;
        public event EventHandler<RemoteLogEventArgs> LogArrived;

        /// <summary>
        /// Instance of ModeliGrpc that is bound to the targetUri
        /// </summary>
        public ModeliGrpcClient(string targetUri)
        {
            // Start up connection
            _channel = new Grpc.Core.Channel(targetUri, ChannelCredentials.Insecure);
            _client = new ModeliBackend.ModeliBackendClient(_channel);
        }

        /// <summary>
        /// Connects asynchronous without timeout
        /// </summary>
        /// <returns></returns>
        public async Task ConnectAsync()
        {
            await _channel.ConnectAsync().ConfigureAwait(false);
            // Start stream receiving
            logTask = Task.Run(ReadLogs);
            newValuesTask = Task.Run(ReadValues);
        }

        private async Task ReadLogs()
        {
            using (var call = _client.Log(new LogRequest()))
            {
                while (await call.ResponseStream.MoveNext(_channel.ShutdownToken).ConfigureAwait(false))
                {
                    var log = call.ResponseStream.Current;
                    LogArrived?.Invoke(this, new RemoteLogEventArgs
                    {
                        InstanceName = log.InstanceName,
                        Status = log.Status,
                        Message = log.Message,
                        Category = log.Category
                    });
                }
            }
        }

        private async Task ReadValues()
        {
            using (var call = _client.NewValues(new NewValuesRequest()))
            {
                while (await call.ResponseStream.MoveNext(_channel.ShutdownToken).ConfigureAwait(false))
                {
                    var response = call.ResponseStream.Current;
                    NewValuesArrived?.Invoke(this, new RemoteValuesEventArgs
                    {
                        InstanceName = response.InstanceName,
                        Timestamp = response.Timestamp,
                        IntegerValueRefs = response.IntValues.Vrs,
                        IntegerValues = response.IntValues.Values,
                        RealValueRefs = response.RealValues.Vrs,
                        RealValues = response.RealValues.Values,
                        BoolValueRefs = response.BoolValues.Vrs,
                        BoolValues = response.BoolValues.Values,
                        StringValueRefs = response.StringValues.Vrs,
                        StringValues = response.StringValues.Values
                    });
                }

            }
        }

        public async Task<bool> AddChannelLinkAsync(LinearChannelLink channelLink)
        {
            // Check if all data is available and it 
            if (channelLink.MasterChannel != null && channelLink.SlaveChannel != null)
            {
                var response = await _client
                    .AddChannelLinkAsync(new AddChannelLinkRequest
                    {
                        ChannelLink = new ChannelLink
                        {
                            MasterInstanceName = channelLink.MasterChannel.ModelInstanceName,
                            SlaveInstanceName = channelLink.SlaveChannel.ModelInstanceName,
                            MasterVr = channelLink.MasterChannel.ValueRef,
                            SlaveVr = channelLink.SlaveChannel.ValueRef,
                            Factor = channelLink.Factor,
                            Offset = channelLink.Offset
                        }
                    }).ResponseAsync.ConfigureAwait(false);
                return response.Success;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> AddFmuAsync(string instanceName, string path)
        {
            const int CHUNK_SIZE = 64 * 1024; // 64 KB
            using (var call = _client.AddFmu())
            {
                // Init filestream
                using (var fileStream = File.OpenRead(path))
                {
                    // Write chunks of the stream
                    var buffer = new byte[CHUNK_SIZE];
                    var totalSize = fileStream.Length;

                    for (int bytesRead; (bytesRead = await fileStream.ReadAsync(buffer, 0, CHUNK_SIZE).ConfigureAwait(false)) > 0;)
                    {
                        await call.RequestStream.WriteAsync(
                            new AddFmuRequest
                            {
                                InstanceName = instanceName,
                                Chunk = ByteString.CopyFrom(buffer, 0, bytesRead),
                                TotalSize = totalSize
                            }).ConfigureAwait(false);
                    }
                }
                // Close the request & get result
                await call.RequestStream.CompleteAsync().ConfigureAwait(false);
                return (await call.ResponseAsync.ConfigureAwait(false)).Success;
            }
        }

        public async void Dispose()
        {
            // The tasks will be cancelled and the streams removed.
            // Those actions throw exeptions, but we don't care, since after disposing they won't be used anymore.
            // Gotta catch em all!
            _channel.ShutdownAsync().Wait();
            try
            {
                if (logTask != null)
                {
                    await logTask.ConfigureAwait(false);
                }
            }
            catch (RpcException) {; }
            try
            {
                if (newValuesTask != null)
                {
                    await newValuesTask.ConfigureAwait(false);
                }
            }
            catch (RpcException) {; }
        }

        public async Task PauseAsync()
        {
            await _client.PauseAsync(new PauseRequest())
                .ResponseAsync.ConfigureAwait(false);
        }

        public async Task<int> PlayAsync()
        {
            return (await _client.PlayAsync(new PlayRequest())
                .ResponseAsync.ConfigureAwait(false))
                .Status;
        }

        public async Task<int> PlayFastAsync(double timeToPlay)
        {
            return (await _client.PlayFastAsync(new PlayFastRequest { Time = timeToPlay })
                .ResponseAsync.ConfigureAwait(false))
                .Status;
        }

        public async Task<bool> RemoveChannelLinkAsync(LinearChannelLink channelLink)
        {
            var response = await _client.RemoveChannelLinkAsync(new RemoveChannelLinkRequest
            {
                ChannelLink = new ChannelLink
                {
                    MasterInstanceName = channelLink.MasterChannel.ModelInstanceName,
                    SlaveInstanceName = channelLink.SlaveChannel.ModelInstanceName,
                    MasterVr = channelLink.MasterChannel.ValueRef,
                    SlaveVr = channelLink.SlaveChannel.ValueRef,
                    Factor = channelLink.Factor,
                    Offset = channelLink.Offset
                }
            }).ResponseAsync.ConfigureAwait(false);
            return response.Success;
        }

        public async Task<bool> RemoveFmuAsync(string instanceName)
        {
            return (await _client.RemoveFmuAsync(new RemoveFmuRequest { InstanceName = instanceName })
                .ResponseAsync.ConfigureAwait(false))
                .Success;
        }

        public async Task<int> StopAsync()
        {
            return (await _client.StopAsync(new StopRequest())
                .ResponseAsync.ConfigureAwait(false))
                .Status;
        }

        public async Task<int> SetIntValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<int> values)
        {
            var intValues = new IntValues();
            intValues.Vrs.AddRange(vrs);
            intValues.Values.AddRange(values);
            return (await _client.SetIntAsync(new SetIntRequest
            {
                InstanceName = instanceName,
                Values = intValues
            }).ResponseAsync.ConfigureAwait(false)).Status;
        }

        public async Task<int> SetRealValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<double> values)
        {
            var realValues = new RealValues();
            realValues.Vrs.AddRange(vrs);
            realValues.Values.AddRange(values);
            return (await _client.SetRealAsync(new SetRealRequest
            {
                InstanceName = instanceName,
                Values = realValues
            }).ResponseAsync.ConfigureAwait(false)).Status;
        }

        public async Task<int> SetBoolValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<int> values)
        {
            var boolValues = new BoolValues();
            boolValues.Vrs.AddRange(vrs);
            boolValues.Values.AddRange(values);
            return (await _client.SetBoolAsync(new SetBoolRequest
            {
                InstanceName = instanceName,
                Values = boolValues
            }).ResponseAsync.ConfigureAwait(false)).Status;
        }

        public async Task<int> SetStringValuesAsync(string instanceName, IEnumerable<uint> vrs, IEnumerable<string> values)
        {
            var stringValues = new StringValues();
            stringValues.Vrs.AddRange(vrs);
            stringValues.Values.AddRange(values);
            return (await _client.SetStringAsync(new SetStringRequest
            {
                InstanceName = instanceName,
                Values = stringValues
            }).ResponseAsync.ConfigureAwait(false)).Status;
        }

        public async Task SetStepSizeAsync(double stepSize)
        {
            await _client.SetStepSizeAsync(new SetStepSizeRequest
            {
                StepSize = stepSize
            }).ResponseAsync.ConfigureAwait(false);
        }
    }
}
