﻿using System;

namespace ModeliChart.RemoteMode
{
    public class RemoteLogEventArgs : EventArgs
    {
        public string InstanceName { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public string Category { get; set; }
    }
}
