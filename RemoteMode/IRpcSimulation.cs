﻿using System;
using System.Threading.Tasks;

namespace ModeliChart.RemoteMode
{
    /// <summary>
    /// Implement this interface to support a remote simulation.
    /// IDisposable takes care of shutting down the connection
    /// </summary>
    public interface IRpcSimulation : IDisposable
    {
        /// <summary>
        /// The target in Uri format: "host:port" (e.g. "192.168.0.11:80").
        /// One Simulation instance is bound on construction and can not reconnect to another server.
        /// </summary>
        string TargetUri { get; }

        /// <summary>
        /// Connect to the target that the instance is bound to.
        /// </summary>
        /// <returns></returns>
        Task ConnectAsync();
    }
}
