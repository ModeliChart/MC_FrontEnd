﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModeliChart.RemoteMode
{
    public sealed class RemoteInstance : IModelInstance
    {
        // Rpc connection to the backend
        private readonly IModeliRpcClient rpcClient;
        private readonly ILookup<uint, IChannel> channelsByValueRef;

        public string Name { get; private set; }

        public IModel Model { get; private set; }

        public IEnumerable<IChannel> Channels =>
            Model.Channels.Select(c => c.WithModelInstanceName(Name));

        /// <summary>
        /// Loads the fmu and initializes the variables.
        /// </summary>
        /// <param name="instanceName"></param>
        /// <param name="location"></param>
        /// <param name="connection"></param>
        public RemoteInstance(string instanceName, IModel model, IModeliRpcClient rpcClient)
        {
            // Inject the dependencies
            Name = instanceName;
            Model = model;
            channelsByValueRef = model.Channels.ToLookup(c => c.ValueRef);
            this.rpcClient = rpcClient;
        }

        internal async Task SetValueAsync(uint valueRef, double value)
        {
            var channel = channelsByValueRef[valueRef].FirstOrDefault();
            if (channel != null && channel.Settable && !double.IsNaN(value))
            {
                Task<int> result = Task.FromResult(0);
                switch (channel.ChannelType)
                {
                    case ChannelType.Enum:
                    case ChannelType.Integer:
                        result = rpcClient.SetIntValuesAsync(Name, new uint[] { channel.ValueRef }, new int[] { Convert.ToInt32(value) });
                        break;
                    case ChannelType.Boolean:
                        result = rpcClient.SetBoolValuesAsync(Name, new uint[] { channel.ValueRef }, new int[] { Convert.ToInt32(value) });
                        break;
                    case ChannelType.Real:
                        result = rpcClient.SetRealValuesAsync(Name, new uint[] { channel.ValueRef }, new double[] { value });
                        break;
                }
                // TODO include status definition in protocol
                if (0 != await result.ConfigureAwait(false))
                {
                    Log.ConsoleLogger.WriteLine(Name, "Failed setting value for channel: " + channel.Name);
                }
            }
        }

        public void Dispose()
        {
        }
    }
}
