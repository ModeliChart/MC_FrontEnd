﻿using System;
using System.Collections.Generic;

namespace ModeliChart.RemoteMode
{
    public class RemoteValuesEventArgs : EventArgs
    {
        public double Timestamp { get; set; }
        public string InstanceName { get; set; }
        public IEnumerable<uint> IntegerValueRefs { get; set; }
        public IEnumerable<int> IntegerValues { get; set; }
        public IEnumerable<uint> RealValueRefs { get; set; }
        public IEnumerable<double> RealValues { get; set; }
        public IEnumerable<uint> BoolValueRefs { get; set; }
        public IEnumerable<int> BoolValues { get; set; }
        public IEnumerable<uint> StringValueRefs { get; set; }
        public IEnumerable<string> StringValues { get; set; }
    }
}
