﻿using ModeliChart.Basics;
using ModeliChart.Files;
using ModeliChart.LocalMode;
using ModeliChart.UI;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ModeliChart
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // High DPI support
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            // For debug
            Application.Run(LoadMainWindow(args));
        }

        private static MainWindow LoadMainWindow(string[] args)
        {
            // Create MainWindow with default configuration
            var workspace = new Workspace(Workspace.DefaultPath);
            var simulation = new LocalSimulation();
            var dataRepository = new DataRepository((IEnumerable<uint> valueRefs) => new BufferedSamplesTable(valueRefs));
            var mainWindow = new MainWindow(workspace, simulation, dataRepository);
            // Process startup commands
            if (args.Length > 0)
            {
                mainWindow.OpenFile(args[0]).Wait();
            }
            return mainWindow;
        }
    }
}
