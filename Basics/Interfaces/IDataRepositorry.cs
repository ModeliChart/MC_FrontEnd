﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Store the simulation data.
    /// The data can be accessed via a DataSourceName and a ValueRef.
    /// The design is immutable
    /// </summary>
    public interface IDataRepository : IDisposable
    {
        /// <summary>
        /// Add raw values from the current simulation step for one modelInstance.
        /// </summary>
        /// <param name="modelInstanceName"></param>
        /// <param name="time"></param>
        /// <param name="valueRefs"></param>
        /// <param name="values"></param>
        void AddValues(string modelInstanceName, double time, IList<uint> valueRefs, IList<double> values);

        /// <summary>
        /// Retrieves the values of one channel which are stored in memory.
        /// Use case is getting the data for the instruments.
        /// </summary>
        /// <param name="channel"></param>
        /// <returns>The values or an empty enumerable if no channel matches.</returns>
        IEnumerable<(double Time, double Value)> GetValues(IChannel channel);

        /// <summary>
        /// Reads the values for each timestep into an enumerable.
        /// The medium might take some time to read the values.
        /// </summary>
        /// <returns>
        /// A DataTable with the first column for the time and the other ones for the channels values.
        /// </returns>
        Task<DataTable> GetValuesAsync(IEnumerable<IChannel> channels, double startTime, double endTime);

        /// <summary>
        /// Removes all values.
        /// </summary>
        /// <returns></returns>
        Task ClearAsync();

    }
}
