﻿namespace ModeliChart.Basics
{
    /// <summary>
    /// Enables channels to be linked.
    /// The slaves value will be calculated from the master.
    /// To simplify our threading model a channel link is immutable by default.
    /// </summary>
    public abstract class ChannelLinkBase
    {
        /// <summary>
        /// If enabled execute the link.
        /// </summary>
        bool Enabled { get; }

        /// <summary>
        /// The factor for the calculation (A*x+b)
        /// </summary>
        double Factor { get; }
        /// <summary>
        /// The offset for the calcutlation (a*x + B)
        /// </summary>
        double Offset { get; }

        /// <summary>
        /// The channel from which the value is taken.
        /// </summary>
        IChannel MasterChannel { get; }
       
        /// <summary>
        /// The channel whichs value is set.
        /// </summary>
        IChannel SlaveChannel { get; }

        /// <summary>
        /// The calculated value which can be set in the slave channel.
        /// </summary>
        double CalculatedValue { get; }
    }
}
