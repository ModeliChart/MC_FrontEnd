﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// A time driven simulation.
    /// A simulation consists of several ModelInstances which are connected via ChannelLinks.
    /// The implementation must take care of thread safety, any method may be called at any time.
    /// </summary>
    public interface ISimulation : IDisposable
    {
        double CurrentTime { get; }

        /// <summary>
        /// Get the simulations step size in seconds.
        /// </summary>
        /// <returns></returns>
        double GetStepSize();

        /// <summary>
        /// Set the simulations step size in seconds.
        /// </summary>
        /// <param name="stepSize"></param>
        /// <returns></returns>
        Task SetStepSize(double stepSize);

        /// <summary>
        /// Starts the simulation and runs forever (or pause or stop is called)
        /// </summary>
        /// <returns></returns>
        Task Play();

        /// <summary>
        /// Won't simulate in real time but as fast as possible
        /// </summary>
        /// <param name="interval"></param>
        Task PlayFast(double interval);

        /// <summary>
        /// Pause the simulation and be able to continue it
        /// </summary>
        Task Pause();

        /// <summary>
        /// Stops the simulation
        /// </summary>
        Task Stop();

        /// <summary>
        /// Add a channel link to the simulation if it does not exist yet.
        /// Otherwise update the existing channel link.
        /// </summary>
        /// <param name="channelLink"></param>
        /// <returns></returns>
        Task AddChannelLink(LinearChannelLink channelLink);

        /// <summary>
        /// Remove the channel link from the simulation.
        /// </summary>
        /// <param name="channelLink"></param>
        /// <returns>True if the channel link could be removed. If it is not found, false is returned.</returns>
        Task RemoveChannelLink(LinearChannelLink channelLink);

        /// <summary>
        /// All the channel links which are used by the simulation.
        /// Modify the internal collection via AddChannelLink and RemoveChannelLink.
        /// </summary>
        IEnumerable<LinearChannelLink> ChannelLinks { get; }

        /// <summary>
        /// Add a new model instance which is based on the given model.
        /// Each instanceName is allowed to exist only once.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="instanceName"></param>
        /// <returns>The model instance that has been created or the existing instance.</returns>
        Task<IModelInstance> AddModelInstance(IModel model, string instanceName);

        /// <summary>
        /// Remove a model instance from the simulation.
        /// </summary>
        /// <returns>True if the model instance has been removed. Otherwise false.</returns>
        Task RemoveModelInstance(IModelInstance modelInstance);

        /// <summary>
        /// The instances of the simulation in a dictionary with their instance names as keys.
        /// Modify the internal dictionary via AddModelInstance and RemoveModelInstance.
        /// </summary>
        IEnumerable<IModelInstance> ModelInstances { get; }

        /// <summary>
        /// Gets called as soon as new values are available.
        /// Usually after DoStep or when a remote simulation sent new data.
        /// </summary>
        event EventHandler<NewValuesEventArgs> NewValuesAvailable;

        /// <summary>
        /// Set the value for a channel in this simulation.
        /// </summary>
        Task SetValue(IChannel channel, double value);
    }
}
