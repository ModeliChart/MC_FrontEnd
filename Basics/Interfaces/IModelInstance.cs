﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// There might be several instances of a model distinguished by an unique name.
    /// </summary>
    public interface IModelInstance : IDisposable
    {
        /// <summary>
        /// Unique name of the instance
        /// </summary>
        string Name { get; }

        // Favour composition over inheratince for cleaner implementations of the instances.
        /// <summary>
        /// The model that has bee used to create the instance.
        /// </summary>
        IModel Model { get; }

        // Proxy to the model, because this attribute is accessed frequently
        /// <summary>
        /// The Channels of the simulation.
        /// </summary>
        IEnumerable<IChannel> Channels { get; }
    }
}
