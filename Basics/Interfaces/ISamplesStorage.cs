﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Use this interface to store samples for a dataSource.
    /// </summary>
    public interface ISamplesStorage : IDisposable
    {
        /// <summary>
        /// Add samples for one simulation step.
        /// </summary>
        /// <param name="time"></param>
        /// <param name="valueRefs">Ordered IEnumerable of the channels value-references.</param>
        /// <param name="values">Ordered IEnumerable of the values that belong to the valueRefs.</param>
        void AddSamples(double time, IList<uint> valueRefs, IList<double> values);

        /// <summary>
        /// Get the buffered samples or one specific channel.
        /// Must be quick.
        /// </summary>
        /// <param name="valueRef"></param>
        /// <returns></returns>
        IEnumerable<(double Time, double Value)> GetValues(uint valueRef);

        /// <summary>
        /// Get for a specified enumerable of channels and a given start and end time all the values.
        /// Handy for exporting multiple channels.
        /// </summary>
        /// <returns></returns>
        Task<DataTable> GetValuesAsync(IEnumerable<IChannel> channels, double startTime, double endTime);

        /// <summary>
        /// Delete the data history. The last values must be kept.
        /// </summary>
        Task ClearAsync();
    }
}
