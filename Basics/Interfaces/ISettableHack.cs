﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// This interface contains all the bad things a channel must currently do.
    /// A channel should not do this kind of stuff.
    /// </summary>
    public interface ISettableHack
    {
        /// <summary>
        /// Sets a value without checking if the channel is settable.
        /// </summary>
        Task SetValueUnsafe(IEnumerable<IModelInstance> modelInstances, double value);
    }
}
