﻿using System.Collections.Generic;

namespace ModeliChart.Basics
{
    public interface IEnumerationChannel: IChannel
    {
        IDictionary<int, string> Entries
        {
            get;
        }
    }
}
