﻿using System.Collections.Generic;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Represents an arbitrary model as part of the Simulation.  
    /// </summary>
    public interface IModel
    {
        /// <summary>
        /// The model name from the xml.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The model identifier from the xml. Used to locate the binary model.
        /// </summary>
        string ModelIdentifier { get; }

        /// <summary>
        /// The guid from the xml that can be used for instantiation.
        /// Can be used to distinguish models with the same name.
        /// </summary>
        string Guid { get; }

        /// <summary>
        /// The location of the EXTRACTED model which is ready to use.
        /// It is always possible to zip the model again.
        /// </summary>
        string Location { get; }

        /// <summary>
        /// Channels which from the model description.
        /// </summary>
        IEnumerable<IChannel> Channels { get; }
    }
}
