﻿namespace ModeliChart.Basics
{
    /// <summary>
    /// The different types of channels the fmu standard allows.
    /// </summary>
    public enum ChannelType
    {
        Integer,
        Real,
        Enum,
        Boolean,
        None
    }

    /// <summary>
    /// A Channel bundles the information to access values from a data repository and set values in a modelinstance. 
    /// </summary>
    public interface IChannel
    {
        // Properties
        /// <summary>
        /// Must be unique: FMI 2.0 standard
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Some notes for the enduser.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// The unit of the channels
        /// </summary>
        string DisplayedUnit { get; }

        /// <summary>
        /// Be able to identify if this Channel is Int, Double, or Bool
        /// </summary>
        ChannelType ChannelType { get; }

        /// <summary>
        /// Get the unique name of the model instance.
        /// </summary>
        string ModelInstanceName { get; }

        /// <summary>
        /// Create a copy of this channel with a new model instance name.
        /// </summary>
        /// <param name="modelInstanceName"></param>
        /// <returns></returns>
        IChannel WithModelInstanceName(string modelInstanceName);

        /// <summary>
        /// Reference which will be used to get the values from a DataSource
        /// Different Channels might share the same ValueRef, FMI 2.0 Standard
        /// </summary>
        uint ValueRef { get; }

        /// <summary>
        /// True: Channel accepts new value; False -> not
        /// </summary>
        bool Enabled { get; set; }

        /// <summary>
        /// Wheter the channels value can be changed.
        /// </summary>
        bool Settable { get; }
    }
}
