﻿using System;
using System.Collections.Generic;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Uses a linear function to calculate the value for the slave channel.
    /// </summary>
    public class LinearChannelLink : IEquatable<LinearChannelLink>
    {
        public LinearChannelLink(IChannel masterChannel, IChannel slaveChannel, double factor, double offset, bool enabled)
        {
            MasterChannel = masterChannel;
            SlaveChannel = slaveChannel;
            Factor = factor;
            Offset = offset;
            Enabled = enabled;
        }

        /// <summary>
        /// Equality if the channels match.
        /// </summary>
        /// <param name="channelLink"></param>
        /// <returns>True if master and slave channels are the same.</returns>
        public bool Equals(LinearChannelLink channelLink) => channelLink.MasterChannel == MasterChannel && channelLink.SlaveChannel == SlaveChannel;

        public override bool Equals(object obj)
        {
            if (obj is LinearChannelLink link)
            {
                return Equals(obj as LinearChannelLink);
            }
            return false;
        }

        /// <summary>
        /// If enabled execute the link.
        /// </summary>
        public bool Enabled { get; }

        /// <summary>
        /// The channel from which the value is taken.
        /// </summary>
        public IChannel MasterChannel { get; }

        /// <summary>
        /// The channel whichs value is set.
        /// </summary>
        public IChannel SlaveChannel { get; }


        public double Factor { get; }
        public double Offset { get; }

        // Generatedby VS for IEquatable
        public override int GetHashCode()
        {
            var hashCode = -646954941;
            hashCode = hashCode * -1521134295 + Enabled.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IChannel>.Default.GetHashCode(MasterChannel);
            hashCode = hashCode * -1521134295 + EqualityComparer<IChannel>.Default.GetHashCode(SlaveChannel);
            hashCode = hashCode * -1521134295 + Factor.GetHashCode();
            hashCode = hashCode * -1521134295 + Offset.GetHashCode();
            return hashCode;
        }
    }
}
