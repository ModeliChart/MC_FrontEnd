﻿using System.Collections.Generic;

namespace ModeliChart.Basics
{
    public class EnumerationChannel : Channel, IEnumerationChannel
    {
        public IDictionary<int, string> Entries { get; }

        public new ChannelType ChannelType => ChannelType.Enum;

        public EnumerationChannel(string name, uint valueRef, string description, bool settable, IDictionary<int, string> entries)
            : base(name, valueRef, description, settable)
        {
            Entries = entries;
        }

        public EnumerationChannel(IChannel channel, IDictionary<int, string> entries)
          : base(channel)
        {
            Entries = entries;
        }


    }

}
