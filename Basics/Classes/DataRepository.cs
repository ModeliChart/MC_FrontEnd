﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// A default implementation of <see cref="IDataRepository"/>
    /// using a Dictionary of DataSource names and ISamplesStorages.
    /// </summary>
    public sealed class DataRepository : IDataRepository
    {
        // Factory function for storage
        private readonly Func<IEnumerable<uint>, ISamplesStorage> createStorage;
        private readonly Dictionary<string, ISamplesStorage> storages = new Dictionary<string, ISamplesStorage>();

        public DataRepository(Func<IEnumerable<uint>, ISamplesStorage> createStorage)
        {
            this.createStorage = createStorage;
        }

        public void AddValues(string modelInstanceName, double time, IList<uint> valueRefs, IList<double> values)
        {
            // Create new storage if needed
            if (!storages.ContainsKey(modelInstanceName))
            {
                storages.Add(modelInstanceName, createStorage(valueRefs));
            }
            storages[modelInstanceName].AddSamples(time, valueRefs, values);
        }

        public IEnumerable<(double Time, double Value)> GetValues(IChannel channel)
        {
            if (storages.ContainsKey(channel.ModelInstanceName))
            {
                return storages[channel.ModelInstanceName].GetValues(channel.ValueRef);
            }
            else
            {
                return Enumerable.Empty<(double Time, double Value)>();
            }
        }

        public async Task<DataTable> GetValuesAsync(IEnumerable<IChannel> channels, double startTime, double endTime)
        {
            // Only use channels with data
            var groups = channels
                .Where(c => storages.ContainsKey(c.ModelInstanceName))
                .GroupBy(c => c.ModelInstanceName);
            // Get the tables
            var tables = await Task.WhenAll(groups
                .Select(g => storages[g.Key].GetValuesAsync(g, startTime, endTime)))
                .ConfigureAwait(false);
            // Merge into the first table
            for (int i = 1; i < tables.Length; i++)
            {
                tables[0].Merge(tables[i]);
            }
            return tables.FirstOrDefault();
        }

        public void Dispose()
        {
            foreach (var storage in storages.Values)
            {
                storage.Dispose();
            }
        }

        public Task ClearAsync() => Task.WhenAll(storages.Values.Select(s => s.ClearAsync()));
    }
}
