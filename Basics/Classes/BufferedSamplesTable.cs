﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Stores the samples in a buffer and additionally in a persistent DB.
    /// Data has to be added for a timestep and retrieved for a channel.
    /// </summary>
    public class BufferedSamplesTable : ISamplesStorage
    {
        // Use specialized storage classes
        private CyclicSamplesTable buffer;
        private FileSamplesTable fileStorage;


        public BufferedSamplesTable(IEnumerable<uint> valueRefs, int bufferSize = 60000)
        {
            buffer = new CyclicSamplesTable(valueRefs, bufferSize);
            fileStorage = new FileSamplesTable(valueRefs);
        }

        public void AddSamples(double time, IList<uint> valueRefs, IList<double> values)
        {
            buffer.AddSamples(time, valueRefs, values);
            fileStorage.AddSamples(time, valueRefs, values);
        }

        public IEnumerable<(double Time, double Value)> GetValues(uint valueRef)
        {
            return buffer.GetSamples(valueRef);
        }

        /// <summary>
        /// Get for a specified enumerable of channels and a given start and end time all the values.
        /// Handy for exporting multiple channels.
        /// </summary>
        /// <param name="modelInstanceName"></param>
        /// <param name="valueRefs"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public Task<DataTable> GetValuesAsync(IEnumerable<IChannel> channels, double startTime, double endTime)
        {
            return fileStorage.GetValuesAsync(channels, startTime, endTime);
        }

        /// <summary>
        /// Clears the DB and buffer.
        /// </summary>
        public async Task ClearAsync()
        {
            buffer.Clear();
            await fileStorage.Clear().ConfigureAwait(false);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    fileStorage.Dispose();
                }
                disposedValue = true;
            }
        }

        // ~BufferedSamplesTable() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // GC.SuppressFinalize(this);
        }

        public Task<IEnumerable<IEnumerable<(double time, double value)>>> GetValuesAsync(string modelInstanceName, IEnumerable<uint> valueRefs, double startTime, double endTime)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
