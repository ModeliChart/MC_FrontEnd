﻿using System.Collections.Generic;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Observers the ISimulations NewValuesAvailable event and forwards
    /// the values to a IDataRepository.
    /// </summary>
    public class SimulationDataStorer
    {
        private readonly IDataRepository dataRepository;

        public SimulationDataStorer(ISimulation simulation, IDataRepository dataRepository)
        {
            this.dataRepository = dataRepository;
            simulation.NewValuesAvailable += Simulation_NewValuesAvailable;
        }

        private void Simulation_NewValuesAvailable(object sender, NewValuesEventArgs e)
        {
            dataRepository.AddValues(e.ModelInstanceName, e.Time, e.ValueRefs, e.Values);
        }
    }
}
