﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ModeliChart.Basics
{
    /// <summary>
    /// Writes samples to a file on the disk so it can take much more memory than the 
    /// </summary>
    public sealed class FileSamplesTable : IDisposable
    {
        // Uses a custom dense binary serialization: [double[] values, double time]
        // Appens newest value to the end.

        // Save the storage to disk
        private FileStream stream;
        // Write in another Task
        private Task writeTask;
        // Cache for writing, blocks until content is inside
        private BlockingCollection<double[]> writeCache;
        // Efficient access via dictionaries
        private readonly Dictionary<uint, int> posByValueRef;
        private readonly int bufferSize;
        double time_lastWrite;

        /// <summary>
        /// The valueRefs must be known in advance, since calculations depend on a fixed number of channels.
        /// </summary>
        /// <param name="valueRefs"></param>
        public FileSamplesTable(IEnumerable<uint> valueRefs)
        {
            // Init variables   
            writeCache = new BlockingCollection<double[]>();
            posByValueRef = new Dictionary<uint, int>();
            foreach (uint vr in valueRefs)
            {
                if (!posByValueRef.ContainsKey(vr))
                {
                    posByValueRef.Add(vr, posByValueRef.Count);
                }
            }
            bufferSize = posByValueRef.Count + 1;
            CreateTempFile();
            // Run the writing in a parallel Task
            StartWriteTask();
        }

        // Definition of the buffer access.
        private double GetBufferValue(double[] buffer, uint valueRef) =>
          buffer[posByValueRef[valueRef]];

        private void SetBufferValue(double[] buffer, uint valueRef, double value) =>
            buffer[posByValueRef[valueRef]] = value;

        private double GetBufferTime(double[] buffer) => buffer[buffer.Length - 1];

        private void SetBufferTIme(double[] buffer, double time) => buffer[buffer.Length - 1] = time;

        /// <summary>
        /// Creates a new temp file. The old one will be closed and deleted.
        /// </summary>
        private void CreateTempFile()
        {
            // Free the old file
            if (stream != null)
            {
                stream.Dispose();
            }
            // Create the temp with DeleteOnClose flag.
            stream = new FileStream(Path.GetTempFileName(), FileMode.Create, FileAccess.ReadWrite, FileShare.None, 4096, FileOptions.DeleteOnClose);
        }

        private void StartWriteTask()
        {
            // Create new write cache if it has been completed.
            if (writeCache.IsCompleted)
            {
                writeCache = new BlockingCollection<double[]>();
            }
            // Create longrunning! task
            writeTask = new Task(() =>
            {
                try
                {
                    while (!writeCache.IsCompleted)
                    {
                        // Use less resources by waiting for new values
                        var valueBuffer = writeCache.Take();
                        // Convert to byte array and write it
                        byte[] byteBuffer = new byte[valueBuffer.Length * sizeof(double)];
                        Buffer.BlockCopy(valueBuffer, 0, byteBuffer, 0, byteBuffer.Length);
                        stream.Write(byteBuffer, 0, byteBuffer.Length);
                    }
                }
                catch (InvalidOperationException)
                {
                    // The collection has been completed while taking.
                }
            }, TaskCreationOptions.LongRunning);
            writeTask.Start();
        }

        private async Task StopWriteTaskAsync()
        {
            // Stop adding data to the buffer
            writeCache.CompleteAdding();
            await writeTask.ConfigureAwait(false);
        }

        /// <summary>
        /// Clears all the data: Cache and disk.
        /// New samples are ignored While this method executes.
        /// </summary>
        public async Task Clear()
        {
            // Finish wrting
            await StopWriteTaskAsync().ConfigureAwait(false);
            // Use a new cache
            CreateTempFile();
            // Restart writing
            StartWriteTask();
        }

        /// <summary>
        /// The fast method for adding values from the current simulation step.
        /// </summary>
        /// <param name="time"></param>
        /// <param name="valueRefs"></param>
        /// <param name="values"></param>
        public void AddSamples(double time, IList<uint> valueRefs, IList<double> values)
        {
            // Buffer = Timestamp + values
            var buffer = new double[bufferSize];
            var zipped = from valueRef in valueRefs
                         join value in values on valueRefs.IndexOf(valueRef) equals values.IndexOf(value)
                         select (valueRef, value);
           
            foreach (var (valueRef, value) in zipped)
            {
                SetBufferValue(buffer, valueRef, value);
            }
            time_lastWrite = time;
            SetBufferTIme(buffer, time);
            // Append to the writer buffer
            writeCache.Add(buffer);
        }

        /// <summary>
        /// Get for a specified enumerable of channels and a given start and end time all the values.
        /// Handy for exporting multiple channels.
        /// </summary>
        /// <returns>DataTable: Time; valuerefs</returns>
        public async Task<DataTable> GetValuesAsync(IEnumerable<IChannel> channels, double startTime, double endTime)
        {
            // Only use available channels
            channels = channels.Where(c => posByValueRef.ContainsKey(c.ValueRef));
            // Init res DataTable
            var table = new DataTable();
            var timeColumn = new DataColumn("Time", typeof(double));
            table.Columns.Add(timeColumn);
            table.Columns.AddRange(channels
                .Select(c => new DataColumn(c.Name, typeof(double)))
                .ToArray());
            // Get access to the file, stop writing
            await StopWriteTaskAsync().ConfigureAwait(false);
            // Read the file from the begin
            stream.Seek(0, SeekOrigin.Begin);
            // larger chunks for better performance, + 1 for the time
            var buffer = new byte[bufferSize * sizeof(double)];
            var doubleBuffer = new double[bufferSize];
            // Read the whole file
            while (stream.CanRead)
            {
                // Read samples for one timepoint into buffer (larger chunks for performance)
                int bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
                if (bytesRead == 0)
                {
                    // Reached the end of the file
                    break;
                }
                // Extract the values in the same order as the valueRefs
                Buffer.BlockCopy(buffer, 0, doubleBuffer, 0, buffer.Length);
                var time = GetBufferTime(doubleBuffer);
                // Only add data that is requested
                if (endTime==0)
                {
                    endTime = time_lastWrite;
                }
                if (time >= startTime && time <= endTime)
                {
                    // Add new row to table
                    var row = table.NewRow();
                    row[timeColumn] = time;
                    foreach (var channel in channels)
                    {
                        double val =  GetBufferValue(doubleBuffer, channel.ValueRef);
                        row[channel.Name] = val;
                    }
                    table.Rows.Add(row);
                }
            }
            // Restart the writing Task
            StartWriteTask();
            return table;
        }


        public void Dispose()
        {
            // Dispose the managed resources
            StopWriteTaskAsync().Wait();
            writeTask.Dispose();
            writeCache.Dispose();
            // File will be deleted because of the DeleteOnClose flag.
            stream.Dispose();
        }
    }
}
