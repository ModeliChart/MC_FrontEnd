﻿namespace ModeliChart.Basics
{
    public class Channel : IChannel
    {
        public string Name { get; }

        public string Description { get; }

        public string DisplayedUnit { get; set; }

        public ChannelType ChannelType { get; set; }

        public string ModelInstanceName { get; set; }

        public uint ValueRef { get; }

        public bool Enabled { get; set; }

        public bool Settable { get; }

        public Channel(string name, uint valueRef, string description, bool settable)
        {
            Name = name;
            ValueRef = valueRef;
            Description = description;
            Settable = settable;
            Enabled = true;
        }

        /// <summary>
        /// Create a deep copy of the channel.
        /// </summary>
        /// <param name="channel"></param>
        public Channel(IChannel channel)
        {
            Name = channel.Name;
            ValueRef = channel.ValueRef;
            ChannelType = channel.ChannelType;
            Settable = channel.Settable;
            Description = channel.Description;
            DisplayedUnit = channel.DisplayedUnit;
            ModelInstanceName = channel.ModelInstanceName;
            Enabled = channel.Enabled;
        }

        public IChannel WithModelInstanceName(string modelInstanceName) => 
            new Channel(this) { ModelInstanceName = modelInstanceName };
    }
}