﻿using System;
using System.Collections.Generic;

namespace ModeliChart.Basics
{
    public class NewValuesEventArgs : EventArgs
    {
        public string ModelInstanceName { get; set; }
        public double Time { get; set; }
        public IList<uint> ValueRefs { get; set; }
        public IList<double> Values { get; set; }
    }
}
