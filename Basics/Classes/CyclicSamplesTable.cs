﻿using System.Collections.Generic;
using System.Linq;
using CircularBuffer;

namespace ModeliChart.Basics
{
    // Using the Beer-Ware (hooray!) licensed circular buffer implementation by Joao Portela
    // to prevent the gc from collecting lots of samples.
    public class CyclicSamplesTable
    {
        private readonly CircularBuffer<double> timeQueue;
        private readonly IDictionary<uint, (CircularBuffer<double> Queue, double LastValue)> data;
        private readonly object dataLock = new object();
        // Limit the memory usage
        private readonly int capacity;

        public CyclicSamplesTable(IEnumerable<uint> valueRefs, int capacity = 60000)
        {
            this.capacity = capacity;
            timeQueue = new CircularBuffer<double>(capacity);
            data = valueRefs
                .ToDictionary(vr => vr,
                vr => (new CircularBuffer<double>(capacity), 0.0));
        }

        public void AddSamples(double time, IEnumerable<uint> valueRefs, IEnumerable<double> values)
        {
            var zipped = valueRefs.Zip(values, (vr, value) => (vr, value));
            lock (dataLock)
            {
                timeQueue.PushBack(time);
                foreach (var (vr, value) in zipped)
                {
                    var (queue, lastValue) = data[vr];
                    queue.PushBack(value);
                    lastValue = value;
                }
            }
        }

        public void Clear()
        {
            lock (dataLock)
            {
                while (!timeQueue.IsEmpty)
                {
                    timeQueue.PopFront();
                }
                // Use key because we cannot modify the iterated variables... I know good Hackerboi :D
                foreach (var key in data.Keys)
                {
                    var (queue, lastValue) = data[key];
                    while (!queue.IsEmpty)
                    {
                        queue.PopFront();
                    }
                    lastValue = 0;
                }
            }
        }

        public double GetLast(uint valueRef)
        {
            lock (dataLock)
            {
                return data[valueRef].LastValue;
            }
        }

        public IEnumerable<(double Time, double Value)> GetSamples(uint valueRef)
        {
            lock (dataLock)
            {
                if (timeQueue.IsEmpty)
                {
                    // Empty queue fails to execute ToArray
                    return Enumerable.Empty<(double Time, double Value)>();
                }
                else
                {
                    // It is important to copy the data, because the linq query will be executed delayed
                    return timeQueue.ToArray()
                        .Zip(data[valueRef].Queue.ToArray(),
                        (time, value) => (time, value));
                }
            }
        }

    }
}
