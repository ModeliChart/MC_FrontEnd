﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace ModeliChart.LocalMode
{
    class TaskQueue : IDisposable
    {
        private readonly BlockingCollection<Task> taskQueue = new BlockingCollection<Task>();

        public Task Add(Action item)
        {
            var task = new Task(item);
            taskQueue.Add(task);
            return task;
        }

        public Task<T> Add<T>(Func<T> item)
        {
            var task = new Task<T>(item);
            taskQueue.Add(task);
            return task;
        }

        /// <summary>
        /// Executes the task as soon as it becomes available
        /// </summary>
        /// <returns></returns>
        public void ExecuteOne()
        {
            try
            {
                var task = taskQueue.Take();
                task.RunSynchronously();
            }
            catch (ObjectDisposedException)
            {
                ;   // Occurs when
            }
            catch (InvalidOperationException)
            {
                ;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    taskQueue.Dispose();
                }
                disposedValue = true;
            }
        }

        // ~TaskQueue() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
