﻿using ModeliChart.Basics;
using ModeliChart.FmuTools;
using ModeliChart.Log;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ModeliChart.LocalMode
{

    /// <summary>
    /// ModeliChart fmu instance is sink and source.
    /// Warning this class is not designed to be threadsafe.
    /// The caller must make sure to only call one method at a time.
    /// </summary>
    public class FmuInstance : IModelInstance
    {
        private FmiWrapper_Net.FmuInstance fmu;
        private FmuState currentState;
        // Avoid accessing and modifying the fmu values at the same time
        private readonly uint[] intVrs;
        private readonly uint[] realVrs;
        private readonly uint[] boolVrs;
        // Save the values for the current simulation step
        private readonly int[] intValues;
        private readonly double[] realValues;
        private readonly bool[] boolValues;
        // Save the index of each ValueRefs
        private readonly Dictionary<uint, int> intVrPos = new Dictionary<uint, int>();
        private readonly Dictionary<uint, int> realVrPos = new Dictionary<uint, int>();
        private readonly Dictionary<uint, int> boolVrPos = new Dictionary<uint, int>();
        // Order: Int-Real-Bool !!!
        private readonly IList<uint> concatenatedVrs;

        public string Name { get; private set; }

        public IModel Model { get; }

        public IEnumerable<IChannel> Channels =>
            Model.Channels.Select(c => c.WithModelInstanceName(Name));

        public FmuInstance(string instanceName, IModel model)
        {
            Name = instanceName;
            Model = model;
            // Create the wrapper instance
            fmu = new FmiWrapper_Net.FmuInstance(FmuLoader.GetBinaryPath(Model));
            fmu.Log += FmuInstance_Log;
            // Store each valueref once
            intVrs = FmuChannels.GetIntVrs(model.Channels).Distinct().ToArray();
            realVrs = FmuChannels.GetRealVrs(model.Channels).Distinct().ToArray();
            boolVrs = FmuChannels.GetBoolVrs(model.Channels).Distinct().ToArray();
            concatenatedVrs = intVrs.Concat(realVrs).Concat(boolVrs).ToList();
            intValues = new int[intVrs.Length];
            realValues = new double[realVrs.Length];
            boolValues = new bool[boolVrs.Length];
            // Remember the position in the arrays
            intVrPos = intVrs
                .Select((vr, index) => (vr, index))
                .ToDictionary(pair => pair.vr, pair => pair.index);
            realVrPos = realVrs
                .Select((vr, index) => (vr, index))
                .ToDictionary(pair => pair.vr, pair => pair.index);
            boolVrPos = boolVrs
                .Select((vr, index) => (vr, index))
                .ToDictionary(pair => pair.vr, pair => pair.index);
            // Create the instance
            RecreateFmu();
        }

        /// <summary>
        /// Recreates the fmu instance and leaves it in initialization mode.
        /// </summary>
        private void RecreateFmu()
        {
            // Create resource URI
            DirectoryInfo path = new DirectoryInfo(Model.Location);
            string resourceUri = (new Uri(path.FullName)).AbsoluteUri + "/resources";
            // Free old instance
            ConsoleLogger.WriteLine(Name, "Free Instance");
            fmu.FreeInstance();
            try
            {
                // Instantiate throws exceptions on fail
                ConsoleLogger.WriteLine(Name, "Instantiating");
                fmu.Instantiate(Name, FmiWrapper_Net.Fmi2Type.fmi2CoSimulation, Model.Guid, resourceUri, false, false);
                currentState = FmuState.Instantiated;
                ConsoleLogger.WriteLine(Name, "Instantiated");
                fmu.SetupExperiment(false, 0.0, 0.0, false, 0.0);
                fmu.EnterInitializationMode();
                currentState = FmuState.InitializationMode;
                ConsoleLogger.WriteLine(Name, "Initialization mode");
            }
            catch (Exception e)
            {
                ConsoleLogger.WriteLine(Name, e.Message);
            }
        }

        private void FmuInstance_Log(string instanceName, FmiWrapper_Net.Fmi2Status status, string category, string message)
        {
            if (!message.EndsWith("The solver will continue anyway.") && !message.StartsWith("fmiDoStep: currentCommunicationPoint"))
            {
                ConsoleLogger.WriteLine(Name, "Fmu: " + instanceName
                    + ", category: " + category
                    + ", status: " + status.ToString("g")
                    + ", message: " + message);
            }
        }

        internal void DoStep(double currentTime_sec, double stepSize)
        {
            // Exit initialization on first step
            if (currentState == FmuState.InitializationMode)
            {
                fmu.ExitInitializationMode();
                currentState = FmuState.SlaveInitialized;
                ConsoleLogger.WriteLine(Name, "Slave initialized");
            }
            if (currentState == FmuState.SlaveInitialized)
            {
                // Execute step
                fmu.DoStep(currentTime_sec, stepSize, false);
            }
        }

        internal void Reset()
        {
            // Reset the fmu
            if (currentState == FmuState.SlaveInitialized || currentState == FmuState.Instantiated || currentState == FmuState.InitializationMode)
            {
                fmu?.Terminate();
                currentState = FmuState.Terminated;
                RecreateFmu();
            }
        }

        private void SetIntegerValue(uint vr, int value) =>
            fmu.SetInteger(new uint[] { vr }, new int[] { value });

        private void SetRealValue(uint vr, double value) =>
            fmu.SetReal(new uint[] { vr }, new double[] { value });

        private void SetBooleanValue(uint vr, bool value) =>
            fmu.SetBoolean(new uint[] { vr }, new bool[] { value });

        internal void SetValue(IChannel channel, double value)
        {
            if (channel != null && channel.Settable && !double.IsNaN(value))
            {
                switch (channel.ChannelType)
                {
                    case ChannelType.Integer:
                    case ChannelType.Enum:
                        SetIntegerValue(channel.ValueRef, Convert.ToInt32(value));
                        break;
                    case ChannelType.Real:
                        SetRealValue(channel.ValueRef, value);
                        break;
                    case ChannelType.Boolean:
                        SetBooleanValue(channel.ValueRef, Convert.ToBoolean(value));
                        break;
                }
            }
        }

        /// <summary>
        /// Gets the current value from the buffer.
        /// Warning: Not thread safe. Do not execute while SetValue or DoStep!
        /// </summary>
        private int GetIntegerValue(IChannel channel) => intValues[intVrPos[channel.ValueRef]];

        /// <summary>
        /// Gets the current value from the buffer.
        /// Warning: Not thread safe. Do not execute while SetValue or DoStep!
        /// </summary>
        private double GetRealValue(IChannel channel) => realValues[realVrPos[channel.ValueRef]];

        /// <summary>
        /// Gets the current value from the buffer.
        /// Warning: Not thread safe. Do not execute while SetValue or DoStep!
        /// </summary>
        private bool GetBoolValue(IChannel channel) => boolValues[boolVrPos[channel.ValueRef]];

        /// <summary>
        /// Gets the current value from the buffer and converts it to a double value.
        /// Warning: Not thread safe. Do not execute while SetValue or DoStep!
        /// </summary>
        internal double GetValueAsDouble(IChannel channel)
        {
            switch (channel.ChannelType)
            {
                case ChannelType.Integer:
                case ChannelType.Enum:
                    return GetIntegerValue(channel);
                case ChannelType.Real:
                    return GetRealValue(channel);
                case ChannelType.Boolean:
                    return Convert.ToDouble(GetBoolValue(channel));
                default:
                    return 0;
            }
        }

        internal (IList<uint> ValueRefs, IList<double> Values) GetCurrentValues()
        {
            // Update local values from fmu
            fmu.GetInteger(intVrs, intValues);
            fmu.GetReal(realVrs, realValues);
            fmu.GetBoolean(boolVrs, boolValues);
            // Prepare result
            var values = intValues
                   .Select(i => Convert.ToDouble(i))
                   .Concat(realValues)
                   .Concat(boolValues.Select(b => Convert.ToDouble(b)));
            return (concatenatedVrs, values.ToList());
        } 

        public void Dispose()
        {
            fmu.Dispose();
        }
    }

    public enum FmuState
    {
        Instantiated,
        InitializationMode,
        SlaveInitialized,
        Terminated,
        Undefined
    }
}


