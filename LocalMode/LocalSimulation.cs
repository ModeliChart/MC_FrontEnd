﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace ModeliChart.LocalMode
{
    /// <summary>
    /// Local simulation is based on the execution of FmuInstances.
    /// Uses a Diagnostics stopwatch to keep track of reatlime and waits for the time while playing.
    /// This class uses the TaskQueue class to synchronize the access and take full control of dispatching the tasks.
    /// </summary>
    public class LocalSimulation : ISimulation
    {
        // Collections
        private ImmutableDictionary<string, FmuInstance> fmuInstances = ImmutableDictionary.Create<string, FmuInstance>();
        private ImmutableList<LinearChannelLink> channelLinks = ImmutableList.Create<LinearChannelLink>();

        // State of the simulation
        // Current time in seconds, which has been simulated by DoStep()
        private double currentTime = 0;
        private double stepSize = 0.01;

        // Synchronize via message loop
        private readonly Task messageLoopTask;
        private readonly CancellationTokenSource messageLoopCanceller = new CancellationTokenSource();
        private readonly TaskQueue taskQueue = new TaskQueue();
        // Simulation Task
        private Task simulationTask = Task.CompletedTask;
        private CancellationTokenSource simulationCanceller = new CancellationTokenSource();

        public event EventHandler<NewValuesEventArgs> NewValuesAvailable;

        /// <summary>
        /// Creates a new SimulationController by injecting the dataSources and channelLinks
        /// </summary>
        /// <param name="dataSources"></param>
        /// <param name="channelLinks"></param>
        public LocalSimulation()
        {
            // Poll events in separate task.
            var token = messageLoopCanceller.Token;
            messageLoopTask = new Task(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    taskQueue.ExecuteOne();
                }
            }, TaskCreationOptions.LongRunning);
            messageLoopTask.Start();
        }

        /// <summary>
        /// Keeps adding DoStep events to the task queue in realtime.
        /// </summary>
        /// <returns></returns>
        private async Task RunSimulation(CancellationToken token)
        {
            var startTime = TimeSpan.FromSeconds(currentTime);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            while (!token.IsCancellationRequested)
            {
                if (stopwatch.Elapsed + startTime >= TimeSpan.FromSeconds(currentTime))
                {
                    await DoStep().ConfigureAwait(false);
                }
                else
                {
                    Thread.Sleep(1);
                }
            }
        }

        private Task DoStep()
        {
            return taskQueue.Add(() =>
{
    // Execute channellinks BEFORE DoStep (initial values are appllied)
    foreach (var link in channelLinks)
    {
        if (link.SlaveChannel.Settable
        && fmuInstances.TryGetValue(link.MasterChannel.ModelInstanceName, out var masterInstance)
        && fmuInstances.TryGetValue(link.SlaveChannel.ModelInstanceName, out var slaveInstance))
        {
            var value = link.Factor * slaveInstance.GetValueAsDouble(link.SlaveChannel) + link.Offset;
            masterInstance.SetValue(link.MasterChannel, value);
        }
    }
    // Simulate all the DataSources
    foreach (var instance in fmuInstances.Values)
    {
        instance.DoStep(currentTime, stepSize);
        var (ValueRefs, Values) = instance.GetCurrentValues();
        NewValuesAvailable?.Invoke(this,
          new NewValuesEventArgs
          {
              ModelInstanceName = instance.Name,
              Time = currentTime,
              ValueRefs = ValueRefs,
              Values = Values
          });
    }
    // New simulation time
    Interlocked.Exchange(ref currentTime, currentTime + stepSize);
});
        }

        public async Task Play()
        {
            await Pause().ConfigureAwait(false);
            await taskQueue.Add(() =>
            {
                if (simulationTask.IsCompleted)
                {
                    simulationTask = RunSimulation(simulationCanceller.Token);
                }
            }
            );
        }

        public async Task PlayFast(double interval)
        {
            await Pause().ConfigureAwait(false);
            double finishTime = currentTime + interval;
            var token = simulationCanceller.Token;
            // Keep executing DoSteps without waiting. 
            while (currentTime < finishTime && !token.IsCancellationRequested)
            {
                await DoStep().ConfigureAwait(false);
            }
        }

        public Task Pause()
        {
            return taskQueue.Add(() =>
{
    simulationCanceller.Cancel();
    simulationCanceller = new CancellationTokenSource();
});
        }

        public async Task Stop()
        {
            await Pause().ConfigureAwait(false);
            // Wait for the simulation to finish before resetting the fmus.
            // Otherwise the state gets corrupted.
            // Waiting instide the taskQueue causes a deadlock if the
            // Simulation task is not finished.
            await simulationTask.ConfigureAwait(false);
            // Reset the data
            Interlocked.Exchange(ref currentTime, 0);
            await taskQueue.Add(() =>
            {
                foreach (var fmuInstance in fmuInstances.Values)
                {
                    fmuInstance.Reset();
                }
            }).ConfigureAwait(false);
        }

        public Task AddChannelLink(LinearChannelLink channelLink)
        {
            // Only add channel links which are complete
            if (channelLink.MasterChannel == null || channelLink.SlaveChannel == null)
            {
                return Task.CompletedTask;
            }
            return taskQueue.Add(() => channelLinks = channelLinks.Add(channelLink));
        }

        public Task RemoveChannelLink(LinearChannelLink channelLink)
        {
            return taskQueue.Add(() => channelLinks = channelLinks.Remove(channelLink));
        }

        public Task<IModelInstance> AddModelInstance(IModel model, string instanceName)
        {
            // Run time consumesive creation before adding the task to the queue
            var instance = new FmuInstance(instanceName, model);
            // The add action is short enough to be added to the queue
            return taskQueue.Add(() =>
            {
                if (fmuInstances.ContainsKey(instanceName))
                {
                    return fmuInstances[instanceName] as IModelInstance;
                }
                fmuInstances = fmuInstances.Add(instanceName, instance);
                return instance as IModelInstance;
            });
        }

        public async Task RemoveModelInstance(IModelInstance modelInstance)
        {
            // Remove from simulation
            await taskQueue.Add(() => fmuInstances = fmuInstances.Remove(modelInstance.Name))
                .ConfigureAwait(false);
            modelInstance.Dispose();
        }

        public double GetStepSize()
        {
            return stepSize;
        }

        public Task SetStepSize(double stepSize)
        {
            if (stepSize > 0)
            {
                Interlocked.Exchange(ref this.stepSize, stepSize);
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Modifying the elements directly is not threadsafe!
        /// Only adding and removing the is!
        /// </summary>
        public IEnumerable<IModelInstance> ModelInstances => fmuInstances.Values;

        public IEnumerable<LinearChannelLink> ChannelLinks => channelLinks;

        public Task SetValue(IChannel channel, double value)
        {
            return taskQueue.Add(() => fmuInstances[channel.ModelInstanceName].SetValue(channel, value));
        }

        public double CurrentTime => currentTime;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // dipsose managed resources
                    Stop().Wait();
                    messageLoopCanceller.Cancel();
                    taskQueue.Dispose();
                    foreach (var instance in fmuInstances.Values)
                    {
                        instance.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
