﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModeliChart.Log
{
    public static class StatusLogger
    {
        private static ModeliStatusStrip _statusStrip = new ModeliStatusStrip();

        public static StatusStrip StatusStrip
        {
            get { return _statusStrip.StatusStrip; }
        }

        public static string Status
        {
            get { return _statusStrip.Status; }
            set
            {
                if (_statusStrip.InvokeRequired)
                {
                    _statusStrip.BeginInvoke(new Action(() => Status = value));
                }
                _statusStrip.Status = value;
            }
        }

        public static void Reset()
        {
            Marquee = false;
            Progress = 0;
            Status = "Ready";
        }

        /// <summary>
        /// From 0 to 100
        /// </summary>
        public static int Progress
        {
            get { return _statusStrip.ProgressBar.Value; }
            set
            {
                if (_statusStrip.InvokeRequired)
                {
                    _statusStrip.BeginInvoke(new Action(() => Progress = value));
                }
                _statusStrip.ProgressBar.Minimum = 0;
                _statusStrip.ProgressBar.Maximum = 100;
                if ((_statusStrip.ProgressBar.Minimum <= value)
                    && (value <= _statusStrip.ProgressBar.Maximum))
                {
                    _statusStrip.ProgressBar.Value = value;
                }
            }
        }

        public static bool Marquee
        {
            get { return _statusStrip.ProgressBar.MarqueeAnimationSpeed > 0; }
            set
            {
                if (_statusStrip.InvokeRequired)
                {
                    _statusStrip.BeginInvoke(new Action(() => Marquee = value));
                }
                if (value == true)
                {
                    _statusStrip.ProgressBar.MarqueeAnimationSpeed = 30;
                    _statusStrip.ProgressBar.Style = ProgressBarStyle.Marquee;
                }
                else
                {
                    _statusStrip.ProgressBar.MarqueeAnimationSpeed = 0;
                    _statusStrip.ProgressBar.Style = ProgressBarStyle.Continuous;
                }
            }
        }
    }
}
