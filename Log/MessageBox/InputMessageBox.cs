﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeliChart.Log
{
    public class InputMessageBox : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Button btnOK;

        private static InputMessageBox _msgBox;

        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 9);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(50, 13);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "Message";
            // 
            // txtInput
            // 
            this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInput.Location = new System.Drawing.Point(12, 74);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(288, 20);
            this.txtInput.TabIndex = 1;
            this.txtInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInput_KeyDown);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(225, 100);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // InputMessageBox
            // 
            this.ClientSize = new System.Drawing.Size(312, 135);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.lblMessage);
            this.MinimumSize = new System.Drawing.Size(290, 150);
            this.Name = "InputMessageBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private InputMessageBox()
        {
            InitializeComponent();
            btnOK.Click += btnOK_Click;
        }

        public static string Show(string message, string title, string defaultText)
        {
            _msgBox = new InputMessageBox();
            _msgBox.lblMessage.Text = message;
            _msgBox.txtInput.Text = defaultText;
            _msgBox.ShowDialog();
            return _msgBox.txtInput.Text;
        }

        public static string Show(string message, string title)
        {
            _msgBox = new InputMessageBox();
            _msgBox.lblMessage.Text = message;
            _msgBox.Text = title;
            _msgBox.ShowDialog();
            return _msgBox.txtInput.Text;
        }

        private static void btnOK_Click(object sender, EventArgs e)
        {
            _msgBox.Close();
        }

        private void txtInput_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter)
            {
                btnOK_Click(sender, e);
            }
        }
    }
}
