﻿using System;

namespace ModeliChart.Log
{
    public static class ConsoleLogger
    {
        // Only one console exists so static is just fine
        private static ConsoleOutput _console = new ConsoleOutput();

        public static ConsoleOutput Console
        {
            get
            {
                return _console;
            }
            set
            {
                _console = value;
            }
        }

        /// <summary>
        /// Adds a line to the console. Uses invocation so it is thread safe.
        /// </summary>
        /// <param name="senderName"></param>
        /// <param name="msg"></param>
        public static void WriteLine(string senderName, string msg)
        {
            if (_console.InvokeRequired)
            {
                // Invoke for threadsafety
                // BeginInvoke because we don't need to wait for the completion
                _console.BeginInvoke(new Action(() => WriteLine (senderName, msg)));
            }
            else
            {
                _console.WriteLine(senderName + ": " + msg);
                // Don't let the console make the UI unresponsive
                System.Threading.Thread.Sleep(0);
            }
        }
    }
}
