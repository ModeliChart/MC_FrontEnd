﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModeliChart.Log
{
    public partial class ModeliStatusStrip : UserControl
    {
        public ModeliStatusStrip()
        {
            InitializeComponent();
        }

        public StatusStrip StatusStrip
        {
            get { return statusStrip; }
        }

        internal ToolStripProgressBar ProgressBar
        {
            get { return statusProgress; }
        }

        private string _status;
        internal string Status
        {
            get { return _status; }
            set
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new Action(() => Status = value));
                }
                _status = value;
                statusLabel.Text = "Status: " + value;
            }
        }
    }
}
