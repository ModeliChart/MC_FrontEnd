﻿using System;
using WeifenLuo.WinFormsUI.Docking;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ModeliChart.Log
{
    public partial class ConsoleOutput : DockContent
    {
        const int MAX_LOG_LINES = 100;
        // Producer consumer principle so only one thread can access the console.
        private readonly BlockingCollection<string> logBuffer = new BlockingCollection<string>();

        public ConsoleOutput()
        {
            InitializeComponent();
            // Run the consumer task
            Task.Run(() => ConsumeMessages());
            WriteLine("Console started.");

        }


        /// <summary>
        /// Consume the messages.
        /// </summary>
        private void ConsumeMessages()
        {
            while (!logBuffer.IsCompleted)
            {
                WriteToConsole(logBuffer.Take());
            }
        }

        /// <summary>
        /// Actually adds the message to the console
        /// </summary>
        /// <param name="message"></param>
        private void WriteToConsole(string message)
        {
            if (logListBox.InvokeRequired)
            {
                logListBox.BeginInvoke(new Action(() => WriteToConsole(message)));
            }
            else
            {
                logListBox.Items.Add(GetPrefix() + message);
                // Prevent too many messages
                if (logListBox.Items.Count > MAX_LOG_LINES)
                {
                    logListBox.Items.RemoveAt(0);
                }
                // Scroll down
                logListBox.SelectedIndex = logListBox.Items.Count - 1;
                logListBox.ClearSelected();
            }
        }

        private string GetPrefix()
        {
            return "[" + DateTime.Now.ToLongTimeString() + "]: ";
        }

        /// <summary>
        /// Writes the message to the console.
        /// </summary>
        /// <param name="message"></param>
        public void WriteLine(string message)
        {
            // Only enqueue the message
            logBuffer.Add(message);
        }
        
    }
}
