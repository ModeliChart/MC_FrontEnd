﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModeliChart.Basics;
using ModeliChart.LocalMode;
using ModeliChart.RemoteMode;
using System.IO;
using Google.Protobuf;
using ModeliChart.Log;

namespace ModeliChart.Files
{
    class ProtoSimulationConfig : ISimulationConfig
    {
        /// <summary>
        /// Where the proto file is stored.
        /// </summary>
        private readonly string path;

        /// <summary>
        /// The simulation will be stored in a proto file (path).
        /// </summary>
        /// <param name="path"></param>
        public ProtoSimulationConfig(string path)
        {
            this.path = path;
        }

        public async Task<ISimulation> LoadSimulationAsync(IModelRepository modelRepo)
        {
            var simulationSettings = LoadSimulationSettings();
            var simulation = await CreateSimulation(simulationSettings);
            // modelInstances
            await BuildModelInstances(simulation, simulationSettings, modelRepo).ConfigureAwait(false);
            await BuildChannelLinks(simulationSettings, simulation).ConfigureAwait(false);
            return simulation;
        }

        public Task SaveSimulationAsync(ISimulation simulation)
        {
            // Convert
            var settings = new ProtoSimulationSettings
            {
                StepSize = simulation.GetStepSize(),
                RemoteSettings = ProtoConverter.ToProtoRemoteSettings(simulation)
            };
            settings.ChannelLinks.AddRange(simulation.ChannelLinks
                .Select(ProtoConverter.ToProtoChannelLink));
            settings.FmuInstances.AddRange(simulation.ModelInstances
                .Select(ProtoConverter.ToProtoFmuInstance));
            // Save changes
            using (var fs = File.Create(path))
            {
                settings.WriteTo(fs);
            }
            return Task.CompletedTask;
        }


        /// <summary>
        /// Loads the proto from the file.
        /// If it does not exist yet, a new ProtoSimulationSettings instance is created.
        /// </summary>
        /// <returns></returns>
        private ProtoSimulationSettings LoadSimulationSettings()
        {
            // The file might not exist yet
            try
            {
                using (var fs = File.OpenRead(path))
                {
                    return ProtoSimulationSettings.Parser.ParseFrom(fs);
                }
            }
            catch (FileNotFoundException)
            {
                ConsoleLogger.WriteLine("Simulation Config", "Could not load the configuration, the file has not been found.");
                return new ProtoSimulationSettings();
            }
            catch (InvalidProtocolBufferException)
            {
                ConsoleLogger.WriteLine("Simulation Config", "Could not load the configuration, invalid proto file.");
                return new ProtoSimulationSettings();
            }
        }

        // Helper functions for loading the simulation from a proto file.
        private static async Task<ISimulation> CreateSimulation(ProtoSimulationSettings simulationSettings)
        {
            ISimulation result;
            if (simulationSettings.RemoteSettings != null && simulationSettings.RemoteSettings.Connected)
            {
                result = new RemoteSimulation(simulationSettings.RemoteSettings.TargetUri);
            }
            else
            {
                result = new LocalSimulation();
            }
            if (simulationSettings.StepSize > 0)
            {
                await result.SetStepSize(simulationSettings.StepSize).ConfigureAwait(false);
            }
            return result;
        }

        private static async Task BuildModelInstances(ISimulation simulation, ProtoSimulationSettings simulationSettings, IModelRepository modelRepo)
        {
            // Match instances with thei model
            var modelsForInstances = from inst in simulationSettings.FmuInstances
                                     join model in modelRepo.Models on inst.Guid equals model.Guid
                                     select (FmuInstance: inst, Model: model);
            // Create the modelInstances
            foreach (var (instance, model) in modelsForInstances)
            {
                var modelInstance = await simulation.AddModelInstance(model, instance.InstanceName).ConfigureAwait(false);
                if (modelInstance != null)
                {
                    // Update the channel settings
                    var channelTuples = from protoChannel in instance.Channels
                                        join sourceChannel in modelInstance.Channels on protoChannel.Name equals sourceChannel.Name
                                        select (protoChannel, sourceChannel);
                    foreach (var (protoChannel, sourceChannel) in channelTuples)
                    {
                        sourceChannel.Enabled = protoChannel.Enabled;
                    }
                }
            }
        }
        private static async Task BuildChannelLinks(ProtoSimulationSettings simulationSettings, ISimulation simulation)
        {
            // I don't know how to query two channels in an elegant way
            foreach (var link in simulationSettings.ChannelLinks)
            {
                // This local function will return the channel from the modelInstances or null if not found
                IChannel FindChannel(string channelName, string modelInstanceName, IEnumerable<IModelInstance> modelInstances) =>
                    modelInstances.FirstOrDefault(d => d.Name == modelInstanceName)
                    ?.Channels.FirstOrDefault(c => c.Name == channelName);
                var masterChannel = FindChannel(link.MasterChannel.Name, link.MasterChannel.FmuInstanceName, simulation.ModelInstances);
                var slaveChannel = FindChannel(link.SlaveChannel.Name, link.SlaveChannel.FmuInstanceName, simulation.ModelInstances);
                if (masterChannel != null && slaveChannel != null)
                {
                    await simulation.AddChannelLink(new LinearChannelLink(masterChannel, slaveChannel, link.Factor, link.Offset, link.Enabled))
                        .ConfigureAwait(false);
                }
            }
        }
    }
}
