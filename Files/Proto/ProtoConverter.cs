﻿using ModeliChart.Basics;
using ModeliChart.RemoteMode;
using ModeliChart.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeliChart.Files
{
    /// <summary>
    /// Conversion between the generated classes and domain model classes.
    /// </summary>
    static class ProtoConverter
    {
        public static ModeliChart.UserControls.InstrumentType ToModeliInstrumentType(ProtoInstrument.Types.ProtoInstrumentType type)
        {
            switch (type)
            {
                case ProtoInstrument.Types.ProtoInstrumentType.OneChannel:
                    return InstrumentType.OneChannelInstrument;
                case ProtoInstrument.Types.ProtoInstrumentType.TwoChannel:
                    return InstrumentType.TwoChannelInstrument;
                case ProtoInstrument.Types.ProtoInstrumentType.MultiChannel:
                    return InstrumentType.MultiChannelInstrument;
                default:
                    return InstrumentType.MultiChannelInstrument;
            }
        }

        public static Basics.ChannelType ToModeliChannelType(ChannelType type)
        {
            switch (type)
            {
                case ChannelType.Boolean:
                    return Basics.ChannelType.Boolean;
                case ChannelType.Enum:
                    return Basics.ChannelType.Enum;
                case ChannelType.Integer:
                    return Basics.ChannelType.Integer;
                case ChannelType.None:
                    return Basics.ChannelType.None;
                case ChannelType.Real:
                    return Basics.ChannelType.Real;
                default:
                    return Basics.ChannelType.None;
            }
        }

        public static ChannelType ToProtoChannelType(Basics.ChannelType type)
        {
            switch (type)
            {
                case Basics.ChannelType.Boolean:
                    return ChannelType.Boolean;
                case Basics.ChannelType.Enum:
                    return ChannelType.Enum;
                case Basics.ChannelType.Integer:
                    return ChannelType.Integer;
                case Basics.ChannelType.None:
                    return ChannelType.None;
                case Basics.ChannelType.Real:
                    return ChannelType.Real;
                default:
                    return ChannelType.None;
            }
        }

        /// <summary>
        /// Conversion from domain channel to proto channel.
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        public static ProtoChannel ToProtoChannel(IChannel channel) => new ProtoChannel
        {
            Name = channel.Name,
            FmuInstanceName = channel.ModelInstanceName,
            ValueRef = channel.ValueRef,
            Enabled = channel.Enabled,
            ChannelType = ToProtoChannelType(channel.ChannelType),
            Description = channel.Description ?? "",
            DisplayedUnit = channel.DisplayedUnit ?? "",
            Settable = channel.Settable
        };

        public static Channel ToModeliChannel(ProtoChannel channel) => new Basics.Channel(channel.Name, channel.ValueRef, channel.Description, channel.Settable)
        {
            ChannelType = ToModeliChannelType(channel.ChannelType),
            DisplayedUnit = channel.DisplayedUnit,
            Enabled = channel.Enabled,
            ModelInstanceName = channel.FmuInstanceName
        };

        /// <summary>
        /// Conversion from domain channel link to proto channel link.
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        public static ProtoChannelLink ToProtoChannelLink(LinearChannelLink link) => new ProtoChannelLink()
        {
            MasterChannel = ToProtoChannel(link.MasterChannel),
            SlaveChannel = ToProtoChannel(link.SlaveChannel),
            Factor = link.Factor,
            Offset = link.Offset,
            Enabled = link.Enabled
        };

        /// <summary>
        /// Extracts the proto remote settings from the simulation.
        /// </summary>
        /// <param name="simulation"></param>
        /// <returns></returns>
        public static ProtoRemoteSettings ToProtoRemoteSettings(ISimulation simulation)
        {
            if (simulation is IRpcSimulation sim)
            {
                return new ProtoRemoteSettings()
                {
                    Connected = true,
                    TargetUri = sim.TargetUri
                };
            }
            else
            {
                return new ProtoRemoteSettings()
                {
                    Connected = false
                };
            }
        }

        public static (string AreaTitle, IEnumerable<UniversalOxyInstrument> Instruments) ToModeliInstrumentArea(
            ProtoInstrumentArea area, ISimulation simulation, IDataRepository dataRepository)
        {
            var instruments = new List<UniversalOxyInstrument>();
            foreach (var protoInstrument in area.Instruments)
            {
                // Create instrument
                var instrument = new UniversalOxyInstrument(simulation, dataRepository)
                {
                    DisplayedInterval = protoInstrument.IntervalSec,
                    InstrumentType = ProtoConverter.ToModeliInstrumentType(protoInstrument.InstrumentType),
                    SweepMode = protoInstrument.SweepMode
                };
                // Add channels
                var channels = protoInstrument.Channels
                    .Select(ProtoConverter.ToModeliChannel);
                foreach (var channel in channels)
                {
                    instrument.AddChannel(channel);
                }
                // Add instrument
                instruments.Add(instrument);
            }
            return (area.Name, instruments);
        }

        /// <summary>
        /// Converts the instrument area setup to a proto instrument area.
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        public static ProtoInstrumentArea ToProtoInstrumentArea((string Title, IEnumerable<UniversalOxyInstrument> Instruments) area)
        {
            var protoArea = new ProtoInstrumentArea()
            {
                Name = area.Title,
            };
            protoArea.Instruments.AddRange(
               area.Instruments
               .Select(ToProtoInstrument)
                );
            return protoArea;
            // Local functions
            ProtoInstrument ToProtoInstrument(UniversalOxyInstrument instrument)
            {
                var result = new ProtoInstrument()
                {
                    InstrumentType = ToProtoInstrumentType(instrument.InstrumentType),
                    IntervalSec = instrument.DisplayedInterval,
                    SweepMode = instrument.SweepMode
                };
                result.Channels.AddRange(
                    instrument.Channels
                    .Where(c => c != null)
                    .Select(ToProtoChannel)
                    );
                return result;
            }
        }

        public static ProtoInstrument.Types.ProtoInstrumentType ToProtoInstrumentType(UserControls.InstrumentType type)
        {
            switch (type)
            {
                case InstrumentType.OneChannelInstrument:
                    return ProtoInstrument.Types.ProtoInstrumentType.OneChannel;
                case InstrumentType.TwoChannelInstrument:
                    return ProtoInstrument.Types.ProtoInstrumentType.TwoChannel;
                case InstrumentType.MultiChannelInstrument:
                default:
                    // Most general
                    return ProtoInstrument.Types.ProtoInstrumentType.MultiChannel;
            }
        }

        /// <summary>
        /// Converts the data source to a proto fmuinstance.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static ProtoFmuInstance ToProtoFmuInstance(IModelInstance instance)
        {
            var fmuInstance = new ProtoFmuInstance()
            {
                InstanceName = instance.Name,
                Guid = instance.Model.Guid
            };
            fmuInstance.Channels.AddRange(instance.Channels.Select(ToProtoChannel));
            return fmuInstance;
        }

    }
}
