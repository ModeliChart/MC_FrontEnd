﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModeliChart.Basics;

namespace ModeliChart.Files
{
    /// <summary>
    /// This repository contains the parameter configuration for channels.
    /// </summary>
    public interface IParameterRepository
    {
        /// <summary>
        /// Load the parameter configuration by setting the channels values.
        /// </summary>
        /// <param name="channels"></param>
        /// <returns></returns>        
        Task LoadAsync(ISimulation simulation, IEnumerable<IChannel> channels);

        /// <summary>
        /// Save the channels parameter configuration by retrieving their last value.
        /// </summary>
        /// <param name="channels"></param>
        /// <returns></returns>
        Task SaveAsync(IDataRepository dataRepository, IEnumerable<IChannel> channels);

    }
}
