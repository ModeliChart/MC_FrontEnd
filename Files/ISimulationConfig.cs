﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeliChart.Files
{
    /// <summary>
    /// Unifies the access to the simulation.
    /// </summary>
    public interface ISimulationConfig
    {
        /// <summary>
        /// Load the simulation from the configuration.
        /// </summary>
        /// <param name="models">The models needed for creating the datasources.</param>
        /// <returns></returns>
        Task<ISimulation> LoadSimulationAsync(IModelRepository modelRepo);

        /// <summary>
        /// Save the simulation to the repository.
        /// </summary>
        /// <param name="simulation"></param>
        Task SaveSimulationAsync(ISimulation simulation);
    }
}
