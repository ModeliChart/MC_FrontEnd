﻿namespace ModeliChart.Files
{
    /// <summary>
    /// Stores the string constants of the filenames inside a modeli file
    /// </summary>
    static class ModeliFileNames
    {
        // FMUDataSources: (IDataSource.Name).fmu
        // DataSourceInfo: (IDataSource.Name).xml contains the ChannelInfos as ChannelBase
        public const string PROTO_FILENAME = "Simulation.dat";
        public const string SETTABLES_FILENAME = "Settables.txt";
        public const string MODELI_EXTENSION = ".ModeliProto";
    }
}
