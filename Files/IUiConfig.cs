﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModeliChart.UserControls;

namespace ModeliChart.Files
{
    /// <summary>
    /// Load and save instruments in a repository.
    /// </summary>
    public interface IUiConfig
    {
        /// <summary>
        /// The update frequency of the UI.
        /// </summary>
        double RefreshRate_Hz { get; set; }

        IEnumerable<(string AreaTitle, IEnumerable<UniversalOxyInstrument> Instruments)> InstrumentAreas { get; set; }

        /// <summary>
        /// Saves the properties that have been set.
        /// </summary>
        void SaveConfig();
        
        /// <summary>
        /// Loads the properties.
        /// </summary>
        void LoadConfig(ISimulation simulation, IDataRepository dataRepository);
    }
}
