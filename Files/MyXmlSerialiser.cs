﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ModeliChart.Files
{
    public static class MyXmlSerializer
    {

        // Note: Theoretically all types of Objects can be serialized
        // Generic Object typeof T, filename

        /// <summary>
        /// Writes a new xml
        /// </summary>
        /// <param name="content">The object to be Serialized</param>
        /// <param name="path">The path where it has to be serialized</param>
        public static void Serialize<T>(T content, string path)
        {
            // Create Settings
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Indent = true;
            // Open the file to be written in
            using (FileStream stream = File.OpenWrite(path))
            {
                // serialize, write in stram
                using (XmlWriter writer = XmlWriter.Create(stream, writerSettings))
                {
                    Serialize(content, writer);
                }
            }
        }
        public static void Serialize<T>(T content, XmlWriter writer)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T), "");
            // serialize, write in stram
            serializer.Serialize(writer, content);
        }

        /// <summary>
        /// Returns the object from a xml
        /// </summary>
        /// <param name="path">Path of the xml</param>
        /// <returns></returns>
        public static T Deserialize<T>(string path)
        {
            // Read file
            using (FileStream stream = File.OpenRead(path))
            {
                return Deserialize<T>(stream);
            }
        }

        public static T Deserialize<T>(Stream input)
        {
            using (var reader = XmlReader.Create(input))
            {
                return Deserialize<T>(reader);
            }
        }

        public static T Deserialize<T>(XmlReader reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T),"");
            // Deserialize
            return (T)serializer.Deserialize(reader);
        }
       
    }
}
