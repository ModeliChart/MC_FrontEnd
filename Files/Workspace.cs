﻿using Google.Protobuf;
using ModeliChart.Basics;
using ModeliChart.FmuTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace ModeliChart.Files
{
    /// <summary>
    /// This class provides a default persistence layer access.
    /// It uses a proto file to store the simulation and instruments settings.
    /// The models are stored in a directory.
    /// </summary>
    public class Workspace
    {
        /// <summary>
        /// Where the workspace is located.
        /// </summary>
        public string Path { get; private set; }
        // Path to the proto file
        internal string SimulationProtoPath => System.IO.Path.Combine(Path, ".SimulationProto");
        internal string UiProtoPath => System.IO.Path.Combine(Path, ".UiProto");
        public string SettablesTxtPath => System.IO.Path.Combine(Path, "Settables.txt");

        /// <summary>
        /// Creates
        /// </summary>
        /// <param name="dir"></param>
        public Workspace(string dir)
        {
            this.Path = dir;
            // Create needed files if non existant
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            // Initialize members
            ModelRepository = new DirectoryModelRepository(dir);
            SimulationConfig = new ProtoSimulationConfig(SimulationProtoPath);
            UiConfig = new ProtoUiConfig(UiProtoPath);
            ParameterRepository = new TxtParameterRepository(SettablesTxtPath);
        }

        /// <summary>
        /// This config can be used to save and load a simulation.
        /// </summary>
        public ISimulationConfig SimulationConfig { get; }

        /// <summary>
        /// This repository can be used to manage fmu models.
        /// </summary>
        public IModelRepository ModelRepository { get; }

        /// <summary>
        /// This config can be used to save and load the settings of the instruments.
        /// </summary>
        public IUiConfig UiConfig { get; }

        /// <summary>
        /// Use this repository to store the parameter settings of channels in this workspace.
        /// </summary>
        public IParameterRepository ParameterRepository { get; private set; }

        /// <summary>
        /// The default path to the workspace is located in AppData/Local/Modelichart .
        /// </summary>
        public static string DefaultPath => System.IO.Path.Combine(MyDocumentsPath, ".ModeliChart");
        private static string MyDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

        /// <summary>
        /// Returns true if the directory contains the proto files.
        /// </summary>
        /// <returns></returns>
        public bool HasProtoFiles() => File.Exists(SimulationProtoPath) && File.Exists(UiProtoPath);
    }
}
