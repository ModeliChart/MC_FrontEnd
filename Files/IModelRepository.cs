﻿using System.Collections.Generic;
using ModeliChart.Basics;

namespace ModeliChart.Files
{
    /// <summary>
    /// Manages the model entities. Models can be added or removed from the repository.
    /// </summary>
    public interface IModelRepository
    {
        /// <summary>
        /// Get all models from the repository.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IModel> Models { get; }

        /// <summary>
        /// Retrieves the model if it allready exists.
        /// If it does not exist yet, the model is added to the repository and returned.
        /// </summary>
        /// <param name="model"></param>
        IModel AddOrGetModel(string path);

        /// <summary>
        /// Removes the model if it exists. Otherwise nothing is done.
        /// </summary>
        /// <param name="model"></param>
        bool RemoveModel(IModel model);
    }
}
