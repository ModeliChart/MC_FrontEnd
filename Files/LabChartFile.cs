﻿using ADIDatCli;
using ModeliChart.Basics;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModeliChart.Files
{
    public static class LabChartFile
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="channels"></param>
        /// <param name="dataTable"></param>
        /// <param name="simulationStepSize"></param>
        /// <returns></returns>
        public static async Task WriteRecordAsync(string filename, IEnumerable<IChannel> channels, IDataRepository dataRepository, double simulationStepSize, double startTime, double endTime)
        {
            if (File.Exists(filename))
            {
                // SaveFileDialog already asked if the file is to be replaced the answer must have been yes :)
                File.Delete(filename);
            }

            // Do not save more than the maximum
            if (channels.Count() > 32)
            {
                MessageBox.Show("Error: A maximum of 32 channels can be exported in LabChrt(.adidat) format.");
                return;
            }

            // Fetch the data
            var dataTable = await dataRepository.GetValuesAsync(channels, startTime, endTime)
                .ConfigureAwait(false);

            // Start writing
            var channelNames = from channel in channels select channel.Name;
            var channelUnits = from channel in channels select channel.DisplayedUnit;
            using (AdiDatWriter writer = new AdiDatWriter(filename, channelNames, channelUnits, simulationStepSize))
            {
                // Query the data to write to the record
                List<IEnumerable<float>> data = new List<IEnumerable<float>>();
                foreach (DataColumn column in dataTable.Columns)
                {
                    if (column.ColumnName == "Time")
                    {
                        continue;
                    }
                    var channelData = new List<float>(dataTable.Rows.Count);
                    foreach (DataRow row in dataTable.Rows)
                    {
                        channelData.Add((float)row[column]);
                    }
                    // Add the data from the memory
                    data.Add(channelData);
                }
                // Write it!
                writer.AddRecord(data);
            }
        }
    }
}
