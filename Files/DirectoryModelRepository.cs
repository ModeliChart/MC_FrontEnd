﻿using System.Collections.Generic;
using System.Linq;
using ModeliChart.Basics;
using ModeliChart.FmuTools;
using System.IO;
using System.IO.Compression;

namespace ModeliChart.Files
{
    /// <summary>
    /// A repository that stores all models in a directory on the filesystem.
    /// The models are extracted to directories which are named after their GUIDs.
    /// </summary>
    class DirectoryModelRepository : IModelRepository
    {
        // The dictionary this repository is working in.
        private string directory;
        // The cached models, each model shall only exist once.
        private Dictionary<string, FmuModel> modelsByGuid = new Dictionary<string, FmuModel>();

        /// <summary>
        /// Creates a repository that is located in the directoryPath.
        /// Loads the exisiting models into the cache.
        /// </summary>
        /// <param name="directoryPath"></param>
        public DirectoryModelRepository(string directory)
        {
            this.directory = directory;
            // Load exisiting models from the directories
            ScanDirectory();
        }

        /// <summary>
        /// Scans the directory for fmus which have not been added yet.
        /// Adds them to the modelsByGuid dictionary.
        /// </summary>
        private void ScanDirectory()
        {
            var guids = from subDir in Directory.GetDirectories(directory)
                        where FmuLoader.DirectoryIsFmu(subDir)
                        select Path.GetFileName(subDir);
            foreach (var guid in guids)
            {
                if (!modelsByGuid.ContainsKey(guid))
                {
                    modelsByGuid.Add(guid, new FmuModel(GetModelDir(guid)));
                }
            }
        }

        /// <summary>
        /// Add the model or return the model if it has already been added to the repository.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IModel AddOrGetModel(string path)
        {
            var guid = FmuLoader.LoadModelDescriptionFromArchive(path).guid;
            if (modelsByGuid.ContainsKey(guid))
            {
                // Model already loaded
                return modelsByGuid[guid];
            }
            else
            {
                // Extract new model
                var modelDir = GetModelDir(guid);
                if (!Directory.Exists(modelDir))
                {
                    Directory.CreateDirectory(modelDir);
                    ZipFile.ExtractToDirectory(path, modelDir);
                }
                // Load model
                var model = new FmuModel(modelDir);
                modelsByGuid.Add(guid, model);
                return model;
            }
        }

        /// <summary>
        /// A collection of all models that have been loaded.
        /// </summary>
        public IEnumerable<IModel> Models
        {
            get
            {
                ScanDirectory();
                return modelsByGuid.Values;
            }
        }

        /// <summary>
        /// Removes the model from the directory and the cache.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool RemoveModel(IModel model)
        {
            var modelDir = GetModelDir(model.Guid);
            if (Directory.Exists(modelDir))
            {
                Directory.Delete(modelDir, true);
            }
            return modelsByGuid.Remove(model.Guid);
        }

        /// <summary>
        /// Creates the full path string to the model folder.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private string GetModelDir(string guid) => Path.Combine(directory, guid);
    }
}
