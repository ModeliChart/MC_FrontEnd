﻿using System;
using System.IO;
using System.IO.Compression;

namespace ModeliChart.Files
{
    /// <summary>
    /// Provides methods to store/load a workspace in/from a .modeli file.
    /// </summary>
    public static class ModeliFile
    {
        /// <summary>
        /// Saves the workspace to a modeli file. Make sure to save any pending changes to the workspace.
        /// Overwrites the exisiting file.
        /// Basically the workspace gets zipped.
        /// </summary>
        /// <param name="workspace">The workspace that shall be saved as modeli.</param>
        /// <param name="filename">Where the modeli is saved.</param>
        /// <returns></returns>
        public static void SaveToModeli(Workspace workspace, string filename)
        {
            // Override existing file
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            ZipFile.CreateFromDirectory(workspace.Path, filename);
        }

        /// <summary>
        /// Helps deleting the dierectories recursively while handling the exceptions
        /// </summary>
        /// <param name="path"></param>
        private static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }
            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        /// <summary>
        /// Loads the modeli file into the workspace.
        /// <param name="filename">The full path to the modeli file.</param>
        /// <param name="workspace">The workspace in which the modeli file will be loaded.</param>
        /// <returns></returns>
        public static void LoadModeli(string filename, Workspace workspace)
        {
            // Clear the workspace
            DeleteDirectory(workspace.Path);
            // Extract the modeli file
            ZipFile.ExtractToDirectory(filename, workspace.Path);
            // Load old modeli format
            if (!workspace.HasProtoFiles())
            {
                XmlModeliLoader.ConvertWorkspace(workspace);
            }
        }
    }
}
