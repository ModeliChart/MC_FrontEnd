﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    /// <summary>
    /// Wrapper to correctly serialize and deserialize the ChannelLinks
    /// </summary>
    [XmlRoot("ChannelLinks")]
    public class SerializableChannelLinkList
    {
        private List<SerializableChannelLink> _channelLinks;

        [XmlElement("ChannelLink")] // Inside the ChannelLinks XmlArray the Elments are named ChannelLink
        public List<SerializableChannelLink> ChannelLinks { get => _channelLinks; set => _channelLinks = value; }

        public SerializableChannelLinkList()
        {
            ChannelLinks = new List<SerializableChannelLink>();
        }
    }
}
