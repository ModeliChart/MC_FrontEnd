﻿using System;
using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    /// <summary>
    /// This class is used to save the connection settings.
    /// We are seperating the concern of serialization from the connection class.
    /// </summary>
    public class SerializableRemoteConnection
    {
        [XmlElement("RemoteAdress")]
        public string RemoteAdress { get; set; }
        [XmlElement("Port")]
        public int Port { get; set; }
        [XmlElement("Connected")]
        public bool Connected { get; set; }
    }
}
