﻿using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    public class SerializableChannelLink
    {
        private SerializableChannel _masterChannel;
        private SerializableChannel _slaveChannel;
        private bool _enabled;
        private double _factor;
        private double _offset;

        [XmlElement("MasterChannel")]
        public SerializableChannel MasterChannel { get => _masterChannel; set => _masterChannel = value; }
        [XmlElement("SlaveChannel")]
        public SerializableChannel SlaveChannel { get => _slaveChannel; set => _slaveChannel = value; }
        [XmlIgnore]
        public bool Enabled { get => _enabled; set => _enabled = value; }
        // Fix for deserializing True and False
        [XmlElement("Linked")]
        public string EnabledString
        {
            get => Enabled.ToString();
            set
            {
                if(bool.TryParse(value, out bool enabled))
                {
                    Enabled = enabled;
                }
            }
        }
        [XmlElement("ScaleFactor")]
        public double Factor { get => _factor; set => _factor = value; }
        [XmlElement("Offset")]
        public double Offset { get => _offset; set => _offset = value; }
    }
}
