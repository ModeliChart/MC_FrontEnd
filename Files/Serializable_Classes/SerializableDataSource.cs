﻿using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    public class SerializableDataSource
    {
        private string _name;
        [XmlElement("InstanceName")]
        public string Name { get => _name; set => _name = value; }

        private string _location;
        [XmlElement("Location")]
        public string Location { get => _location; set => _location = value; }
    }
}
