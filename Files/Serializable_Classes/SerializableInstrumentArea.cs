﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    [XmlType("InstrumentAreaSetup")]
    public class SerializableInstrumentArea
    {
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlArray("Instruments")]
        [XmlArrayItem("UniversalOxyInstrument")]
        public List<SerializableInstrument> Instruments { get; set; }
    }
}
