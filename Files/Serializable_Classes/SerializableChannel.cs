﻿using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    public class SerializableChannel
    {
        private string _name;
        private string _dataSourceName;
        private uint _valueRef;
        private bool _settable = false;
        private bool _enabled = true;

        [XmlElement("Name")]
        public string Name { get => _name; set => _name = value; }
        [XmlElement("ValueRef")]
        public uint ValueRef { get => _valueRef; set => _valueRef = value; }
        [XmlElement("DataSourceName")]
        public string DataSourceName { get => _dataSourceName; set => _dataSourceName = value; }
        [XmlIgnore]
        public bool Settable { get => _settable; set => _settable = value; }
        [XmlIgnore]
        public bool Enabled { get => _enabled; set => _enabled = value; }

        // Old manual method used boolean.ToString => True & False not true and false :(
        // Workaround: String surrogate
        [XmlElement("Settable")]
        public string SettableString
        {
            get => Settable.ToString();
            set
            {
                if(bool.TryParse(value, out bool settable))
                {
                    Settable = settable;
                }
            }
        }
        [XmlElement("Enabled")]
        public string EnabledString
        {
            get => Enabled.ToString();
            set
            {
                if(bool.TryParse(value, out bool enabled))
                {
                    Enabled = enabled;
                }
            }
        }
    }
}
