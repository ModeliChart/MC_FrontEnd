﻿using ModeliChart.UserControls;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    public class SerializableInstrument
    {
        private SerializableInstrumentSettings _settings;
        private List<SerializableChannel> _channels;
        [XmlElement("Settings")]
        public SerializableInstrumentSettings Settings { get => _settings; set => _settings = value; }

        [XmlArray("Channels")]
        [XmlArrayItem("Channel")]
        public List<SerializableChannel> Channels { get => _channels; set => _channels = value; }
    }

    /// <summary>
    /// I was stupid in the past and serialized some properties as XmlAttribute
    /// </summary>
    public class SerializableInstrumentSettings
    {
        private double _timeInterval;
        private InstrumentType _instrumentType;
        private bool _SweepMode;

        [XmlAttribute("InstrumentType")]
        public string InstrumentTypeString
        {
            get => _instrumentType.ToString();
            set
            {
                if (Enum.TryParse<InstrumentType>(value, out var instrumentType))
                {
                    _instrumentType = instrumentType;
                }
            }
        }

        [XmlIgnore]
        public InstrumentType InstrumentTypeEnum
        {
            get => _instrumentType;
            set => _instrumentType = value;
        }
        [XmlAttribute("TimeInterval")]
        public double TimeInterval { get => _timeInterval; set => _timeInterval = value; }
        [XmlIgnore]
        public bool SweepMode { get => _SweepMode; set => _SweepMode = value; }
        // Workaround for deserializing True and False
        [XmlAttribute("SweepMode")]
        public string SweepModeString
        {
            get => SweepMode.ToString();
            set
            {
                if(bool.TryParse(value, out bool sweepMode))
                {
                    SweepMode = sweepMode;
                }
            }
        }
    }
}
