﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    [XmlType("Channels")]   // The root of the XmlFile
    public class SerializableChannelList
    {
        private List<SerializableChannel> _channels;
        [XmlElement("Channel")] // The Elements inside "Channels
        public List<SerializableChannel> Channels { get => _channels; set => _channels = value; }
    }
}
