﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ModeliChart.Files.Serializable_Classes
{
    /// <summary>
    /// Remembers where the files are beeing saved
    /// </summary>
    [XmlRoot(ElementName = "Description")]
    public class ModeliIndex
    {
        public ModeliIndex()
        {
            FmuFiles = new List<string>();
            DataSources = new List<SerializableDataSource>();
        }

        /// <summary>
        /// A List of all FmuFiles
        /// FileNames: "SampleFMU.fmu",....
        /// </summary>
        [XmlElement("FmuFiles")]
        public List<string> FmuFiles
        {
            get;
            set;
        }

        [XmlArray("DataSources")]
        [XmlArrayItem("DataSourceDescription")]
        public List<SerializableDataSource> DataSources
        {
            get;
            set;
        }
    }
}
