﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace ModeliChart.Files
{
    public static class CSVExport
    {
        /// <summary>
        /// Exports the given Channels to a CSV file (Time, Channel[0], ..., Channel[n]), Delimeter 'c'
        /// </summary>
        /// <param name="filename">The absolute path where the csv file will be written</param>
        /// <param name="data">A datatable with a "Time" column and channel columns as double</param>
        public static void Write(string filename, DataTable data)
        {
            // Format & buffer settings
            const string DelimiterChar = ";";
            // Create the strings to write
            StringBuilder stringBuilder = new StringBuilder();
            // Description
            IEnumerable<string> columnNames = data.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName
                                              .Replace(".", "_")
                                              .Replace(")", "")
                                              .Replace("(", "_"));
            stringBuilder.AppendLine(string.Join(DelimiterChar, columnNames));
            // Data
            foreach (DataRow row in data.Rows)
            {
                stringBuilder.AppendLine(string.Join(DelimiterChar, row.ItemArray
                    .Select(field => field.ToString())));
            }
            // Write
            File.WriteAllText(filename, stringBuilder.ToString(), Encoding.UTF8);
        }
    }
}
