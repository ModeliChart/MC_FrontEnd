﻿using Google.Protobuf;
using ModeliChart.Basics;
using ModeliChart.UserControls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModeliChart.Log;

namespace ModeliChart.Files
{
    /// <summary>
    /// This class saves and loads the instrument area settings from a proto file.
    /// </summary>
    class ProtoUiConfig : IUiConfig
    {
        private readonly string path;
        public double RefreshRate_Hz { get; set; }
        public IEnumerable<(string AreaTitle, IEnumerable<UniversalOxyInstrument> Instruments)> InstrumentAreas { get; set; }

        /// <summary>
        /// The repository uses a proto file to store the data.
        /// </summary>
        /// <param name="path">The path to the protofile that will be used.</param>
        public ProtoUiConfig(string path)
        {
            // Dependencies
            this.path = path;
            // Default values
            RefreshRate_Hz = 50;
            InstrumentAreas = Enumerable.Empty<(string AreaTitle, IEnumerable<UniversalOxyInstrument> Instruments)>();
        }

        public void LoadConfig(ISimulation simulation, IDataRepository dataRepository)
        {
            try
            {
                using (var fs = File.OpenRead(path))
                {
                    var settings = ProtoUiSettings.Parser.ParseFrom(fs);
                    InstrumentAreas = settings.InstrumentAreas.Select(area =>
                        ProtoConverter.ToModeliInstrumentArea(area,simulation, dataRepository));
                    RefreshRate_Hz = settings.RefreshRateHz;
                }
            }
            catch (FileNotFoundException)
            {
                ConsoleLogger.WriteLine("Ui Configuration", "Could not load the configuration, the file has not been found.");
            }
            catch (InvalidProtocolBufferException)
            {
                ConsoleLogger.WriteLine("Ui Configuration", "Could not load the configuration, the protocol buffer is invalid.");
            }
        }

        public void SaveConfig()
        {
            var settings = new ProtoUiSettings { RefreshRateHz = RefreshRate_Hz };
            settings.InstrumentAreas.AddRange(InstrumentAreas.Select(ProtoConverter.ToProtoInstrumentArea));
            // Save the changes
            using (var fs = File.Create(path))
            {
                settings.WriteTo(fs);
            }
        }
    }
}

