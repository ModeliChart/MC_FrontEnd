﻿using Google.Protobuf;
using ModeliChart.Files.Serializable_Classes;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ModeliChart.Files
{
    /// <summary>
    /// implement the ModeliBuilder.
    /// IDisposable clears temporary files.
    /// </summary>
    public class XmlModeliLoader
    {
        private static string getIndexXmlPath(string directory) => Path.Combine(directory, "Description.xml");
        private static string getChannelLinksXmlPath(string directory) => Path.Combine(directory, "ChannelLinks.xml");
        private static string getRemoteConnectionXmlPath(string directory) => Path.Combine(directory, "RemoteConnection.xml");
        private static string getInstrumentXmlPath(string directory) => Path.Combine(directory, "InstrumentSetup.xml");

        /// <summary>
        /// This method converts the workspace to proto workspace.
        /// </summary>
        /// <param name="workspace"></param>
        public static void ConvertWorkspace(Workspace workspace)
        {
            // Convert simulation settings
            var simulationSettings = new ProtoSimulationSettings()
            {
                RemoteSettings = ConvertRemoteSettings(workspace.Path),
                StepSize = 0.01 // 10 ms
            };
            // This will also create the model folders by utilizing the repository
            simulationSettings.FmuInstances.AddRange(ConvertDataSources(workspace.Path, workspace.ModelRepository));
            simulationSettings.ChannelLinks.AddRange(ConvertChannelLinks(workspace.Path));
            using (var fs = File.OpenWrite(workspace.SimulationProtoPath))
            {
                simulationSettings.WriteTo(fs);
            }
            // Convert instrument settings
            var uiSettings = new ProtoUiSettings();
            uiSettings.InstrumentAreas.AddRange(ConvertInstrumentAreas(workspace.Path));
            using (var fs = File.OpenWrite(workspace.UiProtoPath))
            {
                uiSettings.WriteTo(fs);
            }
        }

        /// <summary>
        /// Build the simulation
        /// </summary>
        private static ProtoRemoteSettings ConvertRemoteSettings(string directory)
        {
            // Default remote settings if no xml exists
            var remoteSettings = new ProtoRemoteSettings()
            {
                Connected = false,
                TargetUri = ""
            };
            // Load settings from xml
            var filename = getRemoteConnectionXmlPath(directory);
            if (File.Exists(filename))
            {
                var settings = MyXmlSerializer.Deserialize<SerializableRemoteConnection>(filename);
                remoteSettings.Connected = settings.Connected;
                if (settings.RemoteAdress != null)
                {
                    remoteSettings.TargetUri = settings.RemoteAdress + ":" + settings.Port;
                }
            }
            return remoteSettings;
        }

        /// <summary>
        /// The DataSources must have been built before this.
        /// </summary>
        private static IEnumerable<ProtoChannelLink> ConvertChannelLinks(string directory)
        {
            var deserializedChannelLinks = MyXmlSerializer.Deserialize<SerializableChannelLinkList>(getChannelLinksXmlPath(directory));
            return deserializedChannelLinks.ChannelLinks.Select(l => new ProtoChannelLink()
            {
                Enabled = l.Enabled,
                Factor = l.Factor,
                Offset = l.Offset,
                MasterChannel = ConvertChannel(l.MasterChannel),
                SlaveChannel = ConvertChannel(l.SlaveChannel)
            });
        }

        /// <summary>
        /// Extracts the models to the workspace and converts the datasources to fmuinstances.
        /// </summary>
        /// <param name="unzipDir">Where the modeli file has been extracted to.</param>
        /// <param name="modelRepository">The repository of the workspace.</param>
        /// <returns></returns>
        private static IEnumerable<ProtoFmuInstance> ConvertDataSources(string unzipDir, IModelRepository modelRepository)
        {
            var index = MyXmlSerializer.Deserialize<ModeliIndex>(getIndexXmlPath(unzipDir));
            var fmuInstances = new List<ProtoFmuInstance>();
            // Old format FmuFiles
            if (index.DataSources.Count == 0)
            {
                foreach (string relPath in index.FmuFiles)
                {
                    string fullPath = Path.Combine(unzipDir, relPath);
                    string instanceName = Path.GetFileNameWithoutExtension(fullPath);
                    var model = modelRepository.AddOrGetModel(fullPath);
                    fmuInstances.Add(new ProtoFmuInstance()
                    {
                        InstanceName = instanceName,
                        Guid = model.Guid
                    });
                }
            }
            // New format DataSource
            foreach (var dataSource in index.DataSources)
            {
                // Load the datasource
                var model = modelRepository.AddOrGetModel(Path.Combine(unzipDir, dataSource.Location));
                var instance = new ProtoFmuInstance()
                {
                    InstanceName = dataSource.Name,
                    Guid = model.Guid
                };
                fmuInstances.Add(instance);
                // Configure the channels
                var channels = MyXmlSerializer.Deserialize<SerializableChannelList>(Path.Combine(unzipDir, dataSource.Name + ".xml"));
                instance.Channels.AddRange(channels.Channels.Select(ConvertChannel));
            }
            return fmuInstances;
        }

        /// <summary>
        /// The DataSources must have been built before this.
        /// </summary>
        private static IEnumerable<ProtoInstrumentArea> ConvertInstrumentAreas(string directory)
        {
            var serializableAreas = MyXmlSerializer.Deserialize<List<SerializableInstrumentArea>>(getInstrumentXmlPath(directory));
            var areas = new List<ProtoInstrumentArea>();
            // Convert each area
            foreach (var serializableArea in serializableAreas)
            {
                var instrumentArea = new ProtoInstrumentArea() { Name = serializableArea.Name };
                foreach (var serializableInstrument in serializableArea.Instruments)
                {
                    var instrument = new ProtoInstrument()
                    {
                        IntervalSec = serializableInstrument.Settings.TimeInterval,
                        InstrumentType = ProtoConverter.ToProtoInstrumentType(serializableInstrument.Settings.InstrumentTypeEnum),
                        SweepMode = serializableInstrument.Settings.SweepMode
                    };
                    instrument.Channels.AddRange(serializableInstrument.Channels.Select(ConvertChannel));
                    instrumentArea.Instruments.Add(instrument);
                }
                areas.Add(instrumentArea);
            }
            return areas;
        }

        private static ProtoChannel ConvertChannel(SerializableChannel c) => new ProtoChannel()
        {
            Enabled = c.Enabled,
            FmuInstanceName = c.DataSourceName,
            Name = c.Name,
            ValueRef = c.ValueRef
        };
    }
}
