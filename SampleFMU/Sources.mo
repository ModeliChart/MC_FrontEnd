within ;
model Sources
  Modelica.Blocks.Sources.Sine sine
    annotation (Placement(transformation(extent={{-58,32},{-38,52}})));
  Modelica.Blocks.Sources.Pulse pulse
    annotation (Placement(transformation(extent={{-58,0},{-38,20}})));
  Modelica.Blocks.Sources.SawTooth sawTooth
    annotation (Placement(transformation(extent={{-58,-36},{-38,-16}})));
  SourceSelector sourceSelector
    annotation (Placement(transformation(extent={{-18,-30},{64,44}})));
  Modelica.Blocks.Nonlinear.FixedDelay fixedDelay
    annotation (Placement(transformation(extent={{84,-4},{104,16}})));
  Modelica.Blocks.Continuous.FirstOrder firstOrder(k=0.9, T=0.2)
    annotation (Placement(transformation(extent={{120,-4},{140,16}})));
equation
  connect(sine.y, sourceSelector.u_Sin) annotation (Line(
      points={{-37,42},{-24,42},{-24,36.6},{-9.8,36.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(pulse.y, sourceSelector.u_Pulse) annotation (Line(
      points={{-37,10},{-26,10},{-26,7},{-9.8,7}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sawTooth.y, sourceSelector.u_Saw) annotation (Line(
      points={{-37,-26},{-26,-26},{-26,-22.6},{-9.8,-22.6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(fixedDelay.y, firstOrder.u) annotation (Line(
      points={{105,6},{118,6}},
      color={0,0,127},
      smooth=Smooth.None));
  connect(sourceSelector.y, fixedDelay.u) annotation (Line(
      points={{59.9,7},{70,7},{70,6},{82,6}},
      color={0,0,127},
      smooth=Smooth.None));
  annotation (uses(Modelica(version="3.2.1")), Diagram(coordinateSystem(
          preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics));
end Sources;
