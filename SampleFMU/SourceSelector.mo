within ;
model SourceSelector
  type SignalType = enumeration(
      sinus,
      pulse,
      saw);
  parameter SignalType SelectedSignalType(start=SignalType.sinus) annotation (Evaluate=false);
  Modelica.Blocks.Interfaces.RealInput u_Sin annotation (Placement(
        transformation(extent={{-100,60},{-60,100}}), iconTransformation(extent=
           {{-100,60},{-60,100}})));
  Modelica.Blocks.Interfaces.RealInput u_Pulse annotation (Placement(
        transformation(extent={{-100,-20},{-60,20}}), iconTransformation(extent=
           {{-100,-20},{-60,20}})));
  Modelica.Blocks.Interfaces.RealInput u_Saw annotation (Placement(
        transformation(extent={{-100,-100},{-60,-60}}), iconTransformation(
          extent={{-100,-100},{-60,-60}})));
  Modelica.Blocks.Interfaces.RealOutput y annotation (Placement(transformation(
          extent={{80,-10},{100,10}}), iconTransformation(extent={{80,-10},{100,
            10}})));
  Modelica.Blocks.Interfaces.RealInput u
    annotation (Placement(transformation(extent={{-26,80},{14,120}})));
  annotation (
    uses(Modelica(version="3.2.1")),
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,
            100}}), graphics),
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}}),
        graphics={
        Text(
          extent={{-60,100},{-8,62}},
          lineColor={0,0,255},
          textString="Sin"),
        Text(
          extent={{2,-18},{-56,20}},
          lineColor={0,0,255},
          textString="Pulse"),
        Text(
          extent={{-56,-62},{-4,-100}},
          lineColor={0,0,255},
          textString="Saw")}));
equation
  if SelectedSignalType==SignalType.sinus then
    y=u_Sin;
  elseif SelectedSignalType==SignalType.saw then
    y=u_Saw;
  elseif SelectedSignalType==SignalType.pulse then
    y=u_Pulse;
  else
    y=0;
  end if;
end SourceSelector;
