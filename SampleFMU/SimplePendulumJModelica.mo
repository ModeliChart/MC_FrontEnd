﻿model SimplePendulumJModelica
  parameter Modelica.SIunits.Acceleration g = 9.81 "Erdbeschleunigung" annotation(Evaluate=false);
  parameter Modelica.SIunits.Length l= 1 "Pendellänge" annotation(Evaluate=false);

  parameter Modelica.SIunits.Mass m = 1 "Masse des Pendels" annotation(Evaluate=false);

  Modelica.SIunits.Angle phi(start= Modelica.SIunits.Conversions.from_deg(90))
    "Gelenkwinkel";
Modelica.SIunits.AngularVelocity omega "Winkelgeschwindigkeit";
  Modelica.SIunits.Position x "x-Position";
  Modelica.SIunits.Position y "y-Position";
  Modelica.Blocks.Interfaces.RealInput D "Dämpfungskonstante im Drehgelenk"
    annotation (Placement(transformation(extent={{-64,-10},{-24,30}})));
equation
  omega=der(phi);
  der(omega)*m*l^2+D*omega=g/l*sin(phi);
  x=sin(phi)/l;
  y=cos(phi)/l;

  annotation (uses(Modelica(version="3.2.1")),
    experiment(StopTime=100, Interval=0.01));
end SimplePendulumJModelica;
