# About

ModeliChart is a fmu simulation environment for soft realtime execution.

## ModeliChart backend

ModeliChart can utilize the MC_Backend application for simulating the fmus.
The communication is realized via the grpc framework.
The backend can run on Windows and Linux independently from the frontend.

The backends project page is:

https://git.rwth-aachen.de/ModeliChart/MC_BackEnd

The protocol for the communication via grpc is placed here:

https://git.rwth-aachen.de/ModeliChart/ModeliProtocol

## Modeli file

ModeliChart uses a zip archive with protocol buffers to store the setup
of the simulation and UI.

The file format has its own project:

https://git.rwth-aachen.de/tim.uebelhoer/MC_ModeliFile

## Release Builds

https://git.rwth-aachen.de/ModeliChart/MC_FrontEnd/tags

## Build & vcpkg

The vcpkg system tends to include unwanted dependencies to grpc.

```
vcpkg integrate install // adds vcpkg to the msbuild system
vcpkg integrate remove // removes vcpkg from the msbuild system
```