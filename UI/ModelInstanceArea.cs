﻿using ModeliChart.Basics;
using WeifenLuo.WinFormsUI.Docking;

namespace ModeliChart.UI
{
    public partial class ModelInstanceArea : DockContent
    {
        // For designer
        public ModelInstanceArea()
        {
            InitializeComponent();
            AllowDrop = true;
        }

        public ModelInstanceArea(IModelInstance modelInstance, ISimulation simulation, IDataRepository dataRepository)
            : this()
        {
            // Remember the DataSource
            ModelInstance = modelInstance;
            // Set the Available Channels
            availableChannelsArea.Simulation = simulation;
            availableChannelsArea.DataRepository = dataRepository;
            availableChannelsArea.AvailableChannels = modelInstance.Channels;
            // Set the title
            Text = modelInstance.Name;
            TabText = modelInstance.Name;
        }

        public IModelInstance ModelInstance { get; }

        /// <summary>
        /// Refreshes the channels recursively, repopulates the tree
        /// </summary>
        public void RefreshChannels()
        {
            availableChannelsArea.RefreshShownChannels();
        }
    }
}
