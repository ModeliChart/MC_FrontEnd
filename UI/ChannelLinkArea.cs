﻿using ModeliChart.Basics;
using ModeliChart.UserControls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ModeliChart.UI
{
    public partial class ChannelLinkArea : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private ISimulation _simulation;

        /// <summary>
        /// Creates a ChannelLinkArea from an existing setup
        /// </summary>
        /// <param name="channelLinkSetup"></param>
        public ChannelLinkArea(ISimulation simulation)
        {
            InitializeComponent();
            _simulation = simulation;
            foreach (LinearChannelLink link in simulation.ChannelLinks)
            {
                var channelLinker = CreateChannelLinker();
                channelLinker.ChannelLink = link;
            }
        }

        /// <summary>
        /// Gets the current collection of ChannelLinks
        /// </summary>
        public IEnumerable<LinearChannelLink> ChannelLinks
        {
            get
            {
                var result = new List<LinearChannelLink>();
                foreach (var control in tableLayoutPanel.Controls)
                {
                    if (control is ChannelLinker channelLinker)
                    {
                        result.Add(channelLinker.ChannelLink);
                    }
                }
                return result;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Create a plain Linker
            CreateChannelLinker();
        }

        private ChannelLinker CreateChannelLinker()
        {
            // Create the control
            ChannelLinker channelLinker = new ChannelLinker();
            channelLinker.Removed += ChannelLinker_Removed;
            channelLinker.Changed += ChannelLinker_Changed;
            // Add at the end in a new row
            tableLayoutPanel.Controls.Add(channelLinker, 0, tableLayoutPanel.Controls.Count);
            channelLinker.Dock = DockStyle.Top;
            return channelLinker;
        }

        private void ChannelLinker_Changed(object sender, ChannelLinkChangedArgs e)
        {
            _simulation.RemoveChannelLink(e.OldLink);
            _simulation.AddChannelLink(e.NewLink);
        }

        private void ChannelLinker_Removed(object sender, EventArgs e)
        {
            if (sender is ChannelLinker channelLinker)
            {
                // Remove from simulation
                _simulation.RemoveChannelLink(channelLinker.ChannelLink);
                // Remove from UI
                tableLayoutPanel.Controls.Remove(sender as Control);
            }
        }
    }
}
