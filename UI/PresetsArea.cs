﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ModeliChart.UI
{
    public partial class PresetsArea : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private readonly IEnumerable<IModelInstance> modelInstances;

        public PresetsArea(IEnumerable<IModelInstance> modelInstances)
        {
            InitializeComponent();
            this.modelInstances = modelInstances;
            comBoxDataSources.Items.AddRange(modelInstances.Select(i => i.Name).ToArray());
            // If not empty select the first dataSource for initial display
            if (comBoxDataSources.Items.Count > 0)
            {
                comBoxDataSources.SelectedIndex = 0;
            }
        }

        private void ComBoxDataSources_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Find the matching dataSource and load the channels into the display area
            availableChannelsArea.AvailableChannels =
                modelInstances
                .FirstOrDefault(dataSource => dataSource.Name == (comBoxDataSources.SelectedItem as string))
                ?.Channels;
        }

        public static void ShowDialog(IEnumerable<IModelInstance> modelInstances)
        {
            PresetsArea area = new PresetsArea(modelInstances);
            area.ShowDialog();
        }

        private void PresetsArea_FormClosing(object sender, FormClosingEventArgs e)
        {
            availableChannelsArea.CheckFoldersEnabled();
        }
    }
}
