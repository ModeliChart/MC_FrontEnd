﻿using ModeliChart.Basics;
using ModeliChart.Files;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModeliChart.UI
{
    public partial class ExportDialog : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        private enum FileFormat
        {
            LabChart,
            CSV,
            TXT_Initial_Values
        }

        // Store copy of channels with their dataSource so the originals enabled state does not get changed
        private ILookup<string, Channel> channelsByInstanceName;
        // Store the dataSources, so we can get the actual values for the channels
        private IDataRepository dataRepository;
        // The simulations step size is needed for the labchart export
        private double simulationStepSize;


        public ExportDialog(
            IDataRepository dataRepository,
            IEnumerable<IChannel> channels,
            double simulationStepSize)
        {
            InitializeComponent();
            // Dependencies
            this.dataRepository = dataRepository;
            availableChannelsArea.DataRepository = dataRepository;
            channelsByInstanceName = channels.ToLookup(
                c => c.ModelInstanceName,
                c => new Channel(c));
            this.simulationStepSize = simulationStepSize;
            // Setup UI
            comboFileFormat.DataSource = Enum.GetValues(typeof(FileFormat));
            comboFileFormat.SelectedIndex = (int)FileFormat.LabChart;
            comBoxDataSources.Items.AddRange(
                channelsByInstanceName.Select(g => g.Key)
                .ToArray());
            if (comBoxDataSources.Items.Count > 0)
            {
                comBoxDataSources.SelectedIndex = 0;
            }
        }

        public static void ShowDialog(IDataRepository dataRepository,
            IEnumerable<IChannel> channels,
            double simulationStepSize)
        {
            ExportDialog area = new ExportDialog(dataRepository, channels, simulationStepSize);
            area.ShowDialog();
        }

        private void ExportDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            availableChannelsArea.CheckFoldersEnabled();
        }

        private void ComBoxDataSources_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Show copy of the channels information, this dialog shall not modify the simulation
            availableChannelsArea.AvailableChannels = channelsByInstanceName[comBoxDataSources.SelectedItem as string];
        }

        private void ComboFileFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Default settings
            lblTime.Text = "Time Interval[sec]:";
            lblLine.Visible = true;
            txtTimeEnd.Visible = true;
            switch (comboFileFormat.SelectedIndex)
            {
                case (int)FileFormat.TXT_Initial_Values:
                    // Only one sample
                    lblTime.Text = "Time [sec]:";
                    lblLine.Visible = false;
                    txtTimeEnd.Visible = false;
                    break;
                case (int)FileFormat.CSV:
                    break;
                case (int)FileFormat.LabChart:
                    break;
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void BtnExport_Click(object sender, EventArgs e)
        {
            // Remember the LastPath automatically
            SaveFileDialog dialog = new SaveFileDialog();

            switch (comboFileFormat.SelectedIndex)
            {
                case (int)FileFormat.TXT_Initial_Values:
                    dialog.DefaultExt = ".txt";
                    dialog.Filter = "Text Files (*.txt)|*.txt";
                    // Show the dialog and set the ModelInstance 
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        await ExportSettablesAsync(dialog.FileName);
                    }
                    break;
                case (int)FileFormat.CSV:
                    dialog.DefaultExt = ".csv";
                    dialog.Filter = "CSV files (*.csv)|*.csv";
                    // Show the dialog and set the ModelInstance 
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        await ExportCsvAsync(dialog.FileName);
                    }
                    break;
                case (int)FileFormat.LabChart:
                    dialog.DefaultExt = ".adidat";
                    dialog.Filter = "LabChart/ADInstruments Simple Data Files (*.adidat)|*.adidat";
                    // Show the dialog and set the ModelInstance 
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        await ExportLabChartAsync(dialog.FileName);
                    }
                    break;
            }
            // Close the dialog (do not use ConfigureAwait(false) beforehand)
            this.Close();
        }

        /// <summary>
        /// Returns an enumerable of all enabled channels.
        /// </summary>
        private IEnumerable<IChannel> EnabledChannels => from channels in channelsByInstanceName
                                                         from channel in channels
                                                         where channel.Enabled
                                                         select channel;
        private double StartTime
        {
            get
            {
                if (double.TryParse(txtTimeStart.Text, out double res))
                {
                    return res;
                }
                return 0;
            }
            set
            {
                txtTimeStart.Text = value.ToString();
            }
        }

        private double EndTime
        {
            get
            {
                if (double.TryParse(txtTimeEnd.Text, out double res))
                {
                    return res;
                }
                return 0;
            }
            set
            {
                txtTimeEnd.Text = value.ToString();
            }
        }


        private Task<DataTable> GetEnabledChannelsDataAsync() => dataRepository.GetValuesAsync(EnabledChannels, StartTime, EndTime);

        /// <summary>
        /// Settable Values (initial)
        /// </summary>
        /// <param name="path"></param>
        private async Task ExportSettablesAsync(string path)
        {
            // Get the time for the export
            double time = double.NaN;
            // Do not change time if nothing is written in txtTime
            if (!string.IsNullOrWhiteSpace(txtTimeStart.Text))
            {
                double.TryParse(txtTimeStart.Text, out time);
            }
            var repo = new TxtParameterRepository(path);
            await repo.SaveAsync(dataRepository, EnabledChannels)
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Export the Samples of the selected Channels as CSV
        /// </summary>
        /// <param name="destPath">The path the user gives us from the SaveFileDialog</param>
        private async Task ExportCsvAsync(String destPath)
        {
            // Write the file
            CSVExport.Write(destPath,
                await GetEnabledChannelsDataAsync()
                .ConfigureAwait(false));
        }

        /// <summary>
        /// Export in Labchart simple DataFile format
        /// </summary>
        /// <param name="destPath"></param>
        private async Task ExportLabChartAsync(string destPath)
        {
            // Write the file, absolute, delimited path to the file, e.g. "C:\\MyData\\NewFile.adidat"
            await LabChartFile.WriteRecordAsync(
                destPath, EnabledChannels, dataRepository, simulationStepSize, StartTime, EndTime)
                .ConfigureAwait(false);
        }
    }
}
