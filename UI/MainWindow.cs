﻿using ModeliChart.Basics;
using ModeliChart.Files;
using ModeliChart.LocalMode;
using ModeliChart.Log;
using ModeliChart.RemoteMode;
using ModeliChart.UserControls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace ModeliChart.UI
{
    /// <summary>
    /// The MainWindows is the Controler of the Simulation!
    /// </summary>
    public partial class MainWindow : RibbonForm
    {
        private Workspace workspace = new Workspace(Workspace.DefaultPath);
        private ISimulation simulation;
        private IDataRepository dataRepository;
        private SimulationDataStorer dataStorer;
        // Display areas
        private List<ModelInstanceArea> modelInstanceAreas = new List<ModelInstanceArea>();
        private List<InstrumentArea> instrumentAreas = new List<InstrumentArea>();
        // UI worker task
        private const int REFRESH_RATE = 30;    // milliseconds
        private readonly System.Windows.Forms.Timer refreshTimer = new System.Windows.Forms.Timer();
        private readonly Task oxyConversionTask;
        private readonly CancellationTokenSource oxyConversionCanceller = new CancellationTokenSource();


        // Create a new Mainwindow with all dependencies injected
        public MainWindow(Workspace workspace, ISimulation simulation, IDataRepository dataRepository)
        {
            InitializeComponent();
            // Load the simulation
            this.workspace = workspace;
            this.simulation = simulation;
            this.dataRepository = dataRepository;
            dataStorer = new SimulationDataStorer(simulation, dataRepository);
            // Init refresh task
            refreshTimer.Interval = REFRESH_RATE;
            refreshTimer.Tick += RefreshTimer_Tick;
            refreshTimer.Start();
            oxyConversionTask = new Task(
                () => OxyConversionLoop(REFRESH_RATE, oxyConversionCanceller.Token),
                TaskCreationOptions.LongRunning);
            oxyConversionTask.Start();
            // Make it good looking
            dockPanel.Theme = new VS2015LightTheme();
            // Show the console
            ConsoleLogger.Console.Show(dockPanel, DockState.DockBottom);
            // Create the statusStrip
            StatusLogger.StatusStrip.Parent = this;
            StatusLogger.StatusStrip.Show();
            StatusLogger.Status = "Starting";
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            var currentTime = simulation.CurrentTime;
            // Invoke refresh on UI thread
            foreach (var area in instrumentAreas)
            {
                area.InvalidateArea(currentTime);
            }
        }

        /// <summary>
        /// Keeps looping and refreshes the UI approximately every interval ms.
        /// </summary>
        /// <param name="interval"></param>
        private void OxyConversionLoop(int interval, CancellationToken token)
        {
            long nextUpdate = 0;
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            while (!token.IsCancellationRequested)
            {
                var elapsedMs = stopwatch.ElapsedMilliseconds;
                if (elapsedMs > nextUpdate)
                {
                    // Update the data in the Task which calls this method
                    var currentTime = simulation.CurrentTime;
                    foreach (var area in instrumentAreas)
                    {
                        area.UpdateDataToTime(currentTime);
                    }
                    nextUpdate += interval;
                }
                Thread.Sleep(1);
            }
        }

        /// <summary>
        /// Replace the current simulation with a new one.
        /// </summary>
        /// <param name="simulation"></param>
        private async Task ReplaceSimulation(ISimulation newSimulation)
        {
            // Copy old setup
            ConsoleLogger.WriteLine("MainWindow", "Replacing data sources...");
            foreach (var modelInstance in simulation.ModelInstances)
            {
                await newSimulation.AddModelInstance(modelInstance.Model, modelInstance.Name);
            }
            ConsoleLogger.WriteLine("MainWindow", "Replacing channel links...");
            foreach (var channelLink in simulation.ChannelLinks)
            {
                await newSimulation.AddChannelLink(channelLink);
            }
            ConsoleLogger.WriteLine("MainWindow", "Updating UI elements...");
            foreach (var instance in simulation.ModelInstances)
            {
                CloseModelInstanceArea(instance);
            }
            foreach (var instance in newSimulation.ModelInstances)
            {
                CreateModelInstanceArea(instance);
            }
            // Cleanup old simulation            
            simulation.Dispose();
            await dataRepository.ClearAsync();
            // Replace the simulation
            simulation = newSimulation;
            dataStorer = new SimulationDataStorer(simulation, dataRepository);
            ReplaceInstrumentAreasSimulation(simulation);
        }

        #region Connection

        private async Task SwitchToRemote(string address, int port)
        {
            ConsoleLogger.WriteLine("MainWindow", "Connecting to the backend...");
            // Create the new Simulation and start connecting
            var remoteSimulation = new RemoteSimulation(address + ":" + port);
            var connectTask = remoteSimulation.ConnectAsync();
            // Timeout
            if (await Task.WhenAny(connectTask, Task.Delay(500)) == connectTask)
            {
                // Await the actual Task to handle exceptions
                await connectTask;
                ConsoleLogger.WriteLine("MainWindow", "Connect succeeded.");
                try
                {
                    await ReplaceSimulation(remoteSimulation);
                    ConsoleLogger.WriteLine("MainWindow", "Remote mode is active.");
                    ribBtnConnectTCP.Text = "Disconnect TCP";
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteLine("MainWindow", "Failed to switch to remote mode: " + ex.Message);
                }
            }
            else
            {
                ConsoleLogger.WriteLine("MainWindow", "Failed to connect to the backend.");
                // Shut down the the rpc
                remoteSimulation.Dispose();
            }
        }

        private async Task SwitchToLocal()
        {
            ConsoleLogger.WriteLine("MainWindow", "Switching to local mode...");
            var realtimeSimulation = new LocalSimulation();
            await ReplaceSimulation(realtimeSimulation);
            ribBtnConnectTCP.Text = "Connect TCP";
            ConsoleLogger.WriteLine("MainWindow", "Finished. Local mode is active.");
        }
        #endregion

        /// <summary>
        /// When the mainWindow is done loading
        /// Create the InstrumentArea
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MainWindow_Shown(object sender, EventArgs e)
        {
            StatusLogger.Reset();
            // Load the default workspace
            await LoadWorkspaceAsync();
        }

        // Stop the Simulation to prevent Errors when closing (Log in the non existing console)
        private async void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            await SaveWorkspaceAsync();
            await simulation.Stop();
            oxyConversionCanceller.Cancel();
            await oxyConversionTask;
            // Free all resources from the simulation
            simulation.Dispose();
        }

        #region InstrumentAreas Management

        private InstrumentArea CreateEmptyInstrumentArea()
        {
            var instruments = new List<UniversalOxyInstrument>();
            for (int i = 0; i < 4; i++)
            {
                instruments.Add(new UniversalOxyInstrument(simulation, dataRepository));
            }
            return new InstrumentArea("Instruments", instruments);
        }

        private void AddInstrumentArea(InstrumentArea area)
        {
            instrumentAreas.Add(area);
            area.Show(dockPanel);
            // Subscribe Closed Event to Dispose the Area correctly
            area.FormClosed += InstrArea_FormClosed;
            // Show the new area
            area.Activate();
        }

        private void ReplaceInstrumentAreasSimulation(ISimulation newSimulation)
        {
            // Create areas with new simulation
            var newAreas = new List<InstrumentArea>();
            foreach (var area in instrumentAreas)
            {
                var newInstruments = new List<UniversalOxyInstrument>();
                foreach (var instrument in area.Instruments)
                {
                    var newInstrument = new UniversalOxyInstrument(newSimulation, dataRepository)
                    {
                        InstrumentType = instrument.InstrumentType,
                        SweepMode = instrument.SweepMode
                    };
                    foreach (var channel in instrument.Channels)
                    {
                        newInstrument.AddChannel(channel);
                    }
                    newInstruments.Add(newInstrument);
                }
                newAreas.Add(new InstrumentArea(area.Title, newInstruments));
            }
            while (instrumentAreas.Count > 0)
            {
                instrumentAreas[0].Close();
            }
            foreach (var area in newAreas)
            {
                AddInstrumentArea(area);
            }
        }

        private void RibbonButtonAddTab_Click(object sender, EventArgs e)
        {
            AddInstrumentArea(CreateEmptyInstrumentArea());
        }

        /// <summary>
        /// Removes the InstrumentArea from the list so it isn't refreshed anymore
        /// The area also gets disposed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void InstrArea_FormClosed(object sender, FormClosedEventArgs e)
        {
            instrumentAreas.Remove(sender as InstrumentArea);
            (sender as InstrumentArea).Dispose();
        }

        #endregion

        #region Play Pause Stop

        // Starts the Simulation
        private async void BtPlay_Click(object sender, EventArgs e)
        {
            try
            {
                await simulation.Play().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteLine("MainWindow", "Play failed: " + ex.Message);
            }
        }
        private async void BtPlayFast_Click(object sender, EventArgs e)
        {
            if (double.TryParse(InputMessageBox.Show("Enter the time in seconds (E.g.: 5,25)", "PlayTo custom"), out double res))
            {
                try
                {
                    await simulation.PlayFast(res).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteLine("MainWindow", "Play fast failed: " + ex.Message);
                }
            }
        }

        // Pauses the Simulation, can be continued
        private async void BtPause_Click(object sender, EventArgs e)
        {
            try
            {
                await simulation.Pause().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteLine("MainWindow", "Pause failed: " + ex.Message);
            }
        }

        // Stops the Simulation
        private async void BtStop_Click(object sender, EventArgs e)
        {
            try
            {
                await simulation.Stop();
                // Auto reload settables
                if (ribChkAutoload.Checked)
                {
                    await workspace.ParameterRepository.LoadAsync(simulation,
                        simulation.ModelInstances.SelectMany(instance => instance.Channels));
                }
                else
                {
                    // Clear the channels and keep the latest value that has been set
                    foreach (var instance in simulation.ModelInstances)
                    {
                        foreach (IChannel channel in instance.Channels)
                        {
                            if (channel.Settable)
                            {
                                // Only set if any values are available yet.
                                var values = dataRepository.GetValues(channel);
                                if (values.Any())
                                {
                                    await simulation.SetValue(channel, values.Last().Value);
                                }
                            }
                        };
                    }
                }
                // Clear the data.
                await dataRepository.ClearAsync();
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteLine("MainWindow", "Stop failed: " + ex.Message);
            }
        }

        #endregion

        #region Events
        // Shows or hides the DataSource Areas
        public void CheckBoxShowChannels_CheckChanged(object sender, EventArgs e)
        {
            if (ribChkBoxShowAvailableChannels.Checked == true)
            {
                for (int i = 0; i < modelInstanceAreas.Count; i++)
                {
                    modelInstanceAreas[i].Show();
                }
            }
            else
            {
                for (int i = 0; i < modelInstanceAreas.Count; i++)
                {
                    modelInstanceAreas[i].Hide();
                }
            }
        }

        // When a DataSourceArea is beeing closed by the user
        private void DataSrcArea_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sender is ModelInstanceArea area)
            {
                simulation.RemoveModelInstance(area.ModelInstance);
            }
        }

        // Changes from light to dark theme
        private void RibChkDarkTheme_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            // TODO close and restore panes
            //if (this.ribChkDarkTheme.Checked)
            //{
            //    dockPanel.Theme = new VS2015DarkTheme();
            //}
            //else
            //{
            //    dockPanel.Theme = new VS2015LightTheme();
            //}
        }

        private void RibbonOrbMenuItemQuit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void RibbonButtonLink_Click(object sender, EventArgs e)
        {
            // Show the linkArea
            var linkArea = new ChannelLinkArea(simulation);
            linkArea.Show(dockPanel, DockState.Float);
        }

        private void RibBtnPreset_Click(object sender, EventArgs e)
        {
            // Show the dialog
            PresetsArea.ShowDialog(simulation.ModelInstances);
            foreach (ModelInstanceArea area in modelInstanceAreas)
            {
                area.RefreshChannels();
            }
        }

        // Adjust the timers frequency
        private async void RibTxtSimInterval_TextBoxTextChanged(object sender, EventArgs e)
        {
            // Try Parse
            if (int.TryParse(ribTxtSimInterval.TextBoxText, out int Hz) && Hz > 0)
            {
                // Update the Settings, on next Play the Rate will be updated
                await simulation.SetStepSize(1.0 / Hz).ConfigureAwait(false);
            }
        }
        private void RibTxtUIInterval_TextBoxTextChanged(object sender, EventArgs e)
        {
            // Try Parse
            if (int.TryParse(ribTxtUIInterval.TextBoxText, out int Hz) && Hz > 0)
            {
                // Set interval in milliseconds
                // TODO timerUi.Interval = (int)((1.0 / Hz) * 1000);
                // Save to persistence
                workspace.UiConfig.RefreshRate_Hz = Hz;
            }
        }

        // Parameter Autoload
        private void RibChkAutoload_CheckBoxCheckChanged(object sender, EventArgs e)
        {
            // Load settables from a new text file if checked
            if (ribChkAutoload.Checked == true)
            {
                OpenSettablesTxtFile();
            }
            ribBtnReloadParam.Enabled = ribChkAutoload.Checked;
        }
        private async void RibBtnReloadParameters_Click(object sender, EventArgs e)
        {
            await workspace.ParameterRepository.LoadAsync(simulation,
                simulation.ModelInstances.SelectMany(d => d.Channels));
        }

        private void RibBtnLogo_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.irt.rwth-aachen.de/");
        }

        #endregion

        #region DataSources

        /// <summary>
        /// Create a new DataSourceArea for an existing DataSource
        /// </summary>
        /// <param name="instance"></param>
        private void CreateModelInstanceArea(IModelInstance instance)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(() => CreateModelInstanceArea(instance)));
            }
            // New DataSourceArea
            ModelInstanceArea dataSrcArea = new ModelInstanceArea(instance, simulation, dataRepository)
            {
                Size = new Size(175, 175)
            };
            dataSrcArea.Show(dockPanel, DockState.DockLeft);
            modelInstanceAreas.Add(dataSrcArea);

            // Subscribe closed event of area
            dataSrcArea.FormClosing += DataSrcArea_FormClosing;
        }

        /// <summary>
        /// Create a datasource name which is unique.
        /// Start with an initial value, which is returned, if it is unqiue.
        /// </summary>
        /// <param name="initialName"></param>
        /// <returns></returns>
        private string GetUniqueDataSourceName(IModel model)
        {
            string name = model.Name;
            while (simulation.ModelInstances.Count(instance => instance.Name == name) > 0)
            {
                name = InputMessageBox.Show("The datasource is already in use. Please use a unique name.", "Datasource name");
            }
            return name;
        }

        /// <summary>
        /// Loads the fmu from the filesystem, adds the datasource to the simulation and creates the datasource area.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private async Task AddFmuAsync(string path)
        {
            // Load model
            var model = workspace.ModelRepository.AddOrGetModel(path);
            var name = GetUniqueDataSourceName(model);
            var modelInstance = await simulation.AddModelInstance(model, name);
            CreateModelInstanceArea(modelInstance);
        }

        /// <summary>
        /// Closes the data source area without removing it from the simulation.
        /// </summary>
        /// <param name="instance"></param>
        private void CloseModelInstanceArea(IModelInstance instance)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new Action(() => CloseModelInstanceArea(instance)));
            }
            var instanceArea = modelInstanceAreas
               .FirstOrDefault(area => area.ModelInstance.Name.Equals(instance.Name));
            if (instanceArea != null)
            {
                modelInstanceAreas.Remove(instanceArea);
                // The handler will remove the datasource from the simulation. This is not the intention of this method.
                instanceArea.FormClosing -= DataSrcArea_FormClosing;
                instanceArea.Close();
            }
        }

        #endregion

        #region Open & Save & Connect & Export

        /// <summary>
        /// Open .fmu or .modeli files
        /// </summary>
        /// <param name="path">Full path to the file</param>
        public async Task OpenFile(string path)
        {
            StatusLogger.Status = "Opening File";
            // Check the extension
            String ext = Path.GetExtension(path);
            StatusLogger.Progress = 10;
            if (ext == ".fmu")
            {
                // Name the dataSource 
                string instanceName = Path.GetFileNameWithoutExtension(path);
                try
                {
                    // Load the model
                    await AddFmuAsync(path);
                    // Save changes
                    await SaveWorkspaceAsync();
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteLine("MainWindow", "Failed to add data source  " + instanceName + ", message: " + ex.Message);
                }
                finally
                {
                    StatusLogger.Progress = 100;
                }
            }
            else if (ext == ".modeli")
            {
                await OpenModeli(path);
            }
            StatusLogger.Reset();
        }

        /// <summary>
        /// Saves the current UI and Simualtion configuration.
        /// </summary>
        /// <returns></returns>
        private async Task SaveWorkspaceAsync()
        {
            workspace.UiConfig.InstrumentAreas = instrumentAreas
                .Select(a => (a.Title, a.Instruments));
            workspace.UiConfig.RefreshRate_Hz = REFRESH_RATE;
            workspace.UiConfig.SaveConfig();
            await workspace.SimulationConfig.SaveSimulationAsync(simulation);
        }

        /// <summary>
        /// Cleares the current simulation and view.
        /// Creates simulation and instruments from workspace.
        /// </summary>
        /// <returns></returns>
        private async Task LoadWorkspaceAsync()
        {
            StatusLogger.Progress = 10;
            // Clean up UI
            foreach (var modelInstance in simulation.ModelInstances)
            {
                // Closes the area but keeps the datasource alive
                CloseModelInstanceArea(modelInstance);
            }
            StatusLogger.Progress = 20;
            while (instrumentAreas.Count > 0)
            {
                // Closing the area will also remove it from the list.
                instrumentAreas[0].Close();
            }
            StatusLogger.Progress = 30;

            // Load the workspace
            try
            {
                // Swap the simulation only if loading the simulation was successfull
                var newSimulation = await workspace.SimulationConfig.LoadSimulationAsync(workspace.ModelRepository);
                await ReplaceSimulation(newSimulation);
                ConsoleLogger.WriteLine("MainWindow", "Replaced the simulation");
                StatusLogger.Progress = 50;
                // Reload parameters from workspace
                await workspace.ParameterRepository.LoadAsync(simulation,
                    simulation.ModelInstances.SelectMany(instance => instance.Channels));
                StatusLogger.Progress = 70;
                ConsoleLogger.WriteLine("MainWindow", "Loaded parametrized channels");
                // Create instruments
                workspace.UiConfig.LoadConfig(simulation, dataRepository);
                foreach (var (AreaTitle, Instruments) in workspace.UiConfig.InstrumentAreas)
                {
                    AddInstrumentArea(new InstrumentArea(AreaTitle, Instruments));
                }
                // Create the first area if no other is available
                if (instrumentAreas.Count == 0)
                {
                    AddInstrumentArea(CreateEmptyInstrumentArea());
                }
                StatusLogger.Progress = 90;
                ConsoleLogger.WriteLine("MainWindow", "Updated instruments");
                // Load time values
                // TODO use properties
                ribTxtSimInterval.TextBoxText = (1.0 / simulation.GetStepSize()).ToString();
                ribTxtUIInterval.TextBoxText = workspace.UiConfig.RefreshRate_Hz.ToString();
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteLine("MainWindow", "Loading the workspace failed, message: " + ex.Message);
            }
            finally
            {
                StatusLogger.Progress = 0;
            }
        }

        // Load a .modeli File
        private async Task OpenModeli(string filename)
        {
            // Free the used resources so the workspace can be overriden
            simulation.Dispose();
            ModeliFile.LoadModeli(filename, workspace);
            await LoadWorkspaceAsync();
        }

        /// <summary>
        /// Shows a dialog to load the txt file.
        /// If dialog was closed with OK the settables will be loaded and _settablesPath set.
        /// </summary>
        private async void OpenSettablesTxtFile()
        {
            // Show dialog to load initials
            // Create a dialog with default Filter .fmu
            OpenFileDialog dialog = new OpenFileDialog
            {
                // Remember the LastPath automatically by Dialog
                // dialog.InitialDirectory = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                Filter = "Text Files (*.txt)|*.txt"
            };
            // Show the dialog and set the ModelInstance 
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // Replace the parameter file of the workspace
                File.Copy(dialog.FileName, workspace.SettablesTxtPath);
                await workspace.ParameterRepository.LoadAsync(simulation,
                    simulation.ModelInstances.SelectMany(instance => instance.Channels));
            }
        }

        // Load a .modeli or .fmu file
        private async void RbOrbItOpen_Click(object sender, EventArgs e)
        {
            // Create a dialog with default Filter .fmu
            OpenFileDialog dialog = new OpenFileDialog
            {
                // Remember the LastPath automatically by Dialog
                // dialog.InitialDirectory = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
                Filter = "ModeliChart Files (*.modeli,*.fmu)|*.modeli;*.fmu"
            };
            // Show the dialog and set the ModelInstance 
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                await OpenFile(dialog.FileName);
            }
        }

        // Save the fmu & setup to a modeli file
        private async void RbOrbItSave_Click(object sender, EventArgs e)
        {
            // Create a dialog with default Extension .modeli
            SaveFileDialog dialog = new SaveFileDialog
            {
                // Remember the LastPath automatically
                DefaultExt = ".modeli",
                Filter = "Modilchart files (*.modeli)|*.modeli"
            };
            // Show the dialog and set the ModelInstance 
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // Save the file
                try
                {
                    // The workspace is wrapped as modeli file
                    await SaveWorkspaceAsync();
                    ModeliFile.SaveToModeli(workspace, dialog.FileName);
                }
                catch (IOException ex)
                {
                    ConsoleLogger.WriteLine("MainWindow", ex.Message);
                }
            }
        }

        // Connect to the TCP Backend
        private async void RibBtnConnectTCP_Click(object sender, EventArgs e)
        {
            // Already connected?
            if (simulation is RemoteSimulation)
            {
                // Disconnect
                await SwitchToLocal();
            }
            else if (simulation is LocalSimulation)
            {
                // Check if we can connect before changing the controller
                if (!int.TryParse(ribTxtPort.TextBoxText, out int port))
                {
                    ConsoleLogger.WriteLine("MainWindow", "Failed parsing the port.");
                }
                await SwitchToRemote(ribTxtRemoteHost.TextBoxText, port);
            }
            // Save changes
            await SaveWorkspaceAsync();
        }

        // Export a csv File
        private async void RibOrbItExport_Click(object sender, EventArgs e)
        {
            // Pause the simulation mutex would kill performance
            await simulation.Pause();
            // Show the dialog
            ExportDialog.ShowDialog(dataRepository,
                simulation.ModelInstances.SelectMany(mi => mi.Channels),
                simulation.GetStepSize());
        }

        private async void RibOrbItImport_Click(object sender, EventArgs e)
        {
            // Load a new settables file
            OpenSettablesTxtFile();
            // Save changes
            await SaveWorkspaceAsync();
        }

        private void RibBtnClearAutoLoad_Click(object sender, EventArgs e)
        {
            if (File.Exists(workspace.SettablesTxtPath))
            {
                File.Delete(workspace.SettablesTxtPath);
            }
        }

        #endregion

        private void RbnBtnAbout_Click(object sender, EventArgs e)
        {
            var box = new AboutBox();
            box.Show();
        }
    }
}
