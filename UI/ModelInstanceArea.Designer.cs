﻿namespace ModeliChart.UI
{
    partial class ModelInstanceArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.availableChannelsArea = new ModeliChart.UserControls.AvailableChannelsArea();
            this.SuspendLayout();
            // 
            // availableChannels
            // 
            this.availableChannelsArea.AutoSize = true;
            this.availableChannelsArea.BackColor = System.Drawing.SystemColors.Control;
            this.availableChannelsArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availableChannelsArea.Location = new System.Drawing.Point(0, 0);
            this.availableChannelsArea.Name = "availableChannels";
            this.availableChannelsArea.PresetMode = false;
            this.availableChannelsArea.Size = new System.Drawing.Size(239, 300);
            this.availableChannelsArea.TabIndex = 0;
            // 
            // DataSourceArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(239, 300);
            this.Controls.Add(this.availableChannelsArea);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)((WeifenLuo.WinFormsUI.Docking.DockAreas.DockLeft | WeifenLuo.WinFormsUI.Docking.DockAreas.DockRight)));
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DataSourceArea";
            this.Text = "AvailableChannelsArea";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UserControls.AvailableChannelsArea availableChannelsArea;

    }
}