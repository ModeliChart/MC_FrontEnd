﻿using System;

namespace ModeliChart.UI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.ribTabView = new System.Windows.Forms.RibbonTab();
            this.ribbonPanelPresentation = new System.Windows.Forms.RibbonPanel();
            this.ribChkBoxShowAvailableChannels = new System.Windows.Forms.RibbonCheckBox();
            this.ribChkDarkTheme = new System.Windows.Forms.RibbonCheckBox();
            this.ribPanelAbout = new System.Windows.Forms.RibbonPanel();
            this.rbnBtnAbout = new System.Windows.Forms.RibbonButton();
            this.ribbonTabSimulation = new System.Windows.Forms.RibbonTab();
            this.ribbonPanelControl = new System.Windows.Forms.RibbonPanel();
            this.btPlay = new System.Windows.Forms.RibbonButton();
            this.btnPlayFast = new System.Windows.Forms.RibbonButton();
            this.btPause = new System.Windows.Forms.RibbonButton();
            this.btStop = new System.Windows.Forms.RibbonButton();
            this.ribbonPanelHardware = new System.Windows.Forms.RibbonPanel();
            this.ribBtnConnectTCP = new System.Windows.Forms.RibbonButton();
            this.ribbonPanelAutolad = new System.Windows.Forms.RibbonPanel();
            this.ribChkAutoload = new System.Windows.Forms.RibbonCheckBox();
            this.ribBtnReloadParam = new System.Windows.Forms.RibbonButton();
            this.ribBtnClearAutoLoad = new System.Windows.Forms.RibbonButton();
            this.ribbonPanelTabs = new System.Windows.Forms.RibbonPanel();
            this.ribbonButtonAddTab = new System.Windows.Forms.RibbonButton();
            this.ribbonPanelLogo = new System.Windows.Forms.RibbonPanel();
            this.ribBtnLogo = new System.Windows.Forms.RibbonButton();
            this.dockPanel = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.Ribbon = new System.Windows.Forms.Ribbon();
            this.rbOrbItOpen = new System.Windows.Forms.RibbonOrbMenuItem();
            this.rbOrbItSave = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribOrbItExport = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribOrbItImport = new System.Windows.Forms.RibbonButton();
            this.ribSeperatorQuit = new System.Windows.Forms.RibbonSeparator();
            this.ribbonOrbItQuit = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribTabChannels = new System.Windows.Forms.RibbonTab();
            this.ribPanLinks = new System.Windows.Forms.RibbonPanel();
            this.ribBtnLinks = new System.Windows.Forms.RibbonButton();
            this.ribPanPresets = new System.Windows.Forms.RibbonPanel();
            this.ribBtnPreset = new System.Windows.Forms.RibbonButton();
            this.ribTabSettings = new System.Windows.Forms.RibbonTab();
            this.ribPanelSimInterval = new System.Windows.Forms.RibbonPanel();
            this.ribLblSimInterval = new System.Windows.Forms.RibbonLabel();
            this.ribTxtSimInterval = new System.Windows.Forms.RibbonTextBox();
            this.ribPanelUIRefresh = new System.Windows.Forms.RibbonPanel();
            this.ribLblUIInterval = new System.Windows.Forms.RibbonLabel();
            this.ribTxtUIInterval = new System.Windows.Forms.RibbonTextBox();
            this.ribPanelRemoteHost = new System.Windows.Forms.RibbonPanel();
            this.ribLblRemoteHost = new System.Windows.Forms.RibbonLabel();
            this.ribTxtRemoteHost = new System.Windows.Forms.RibbonTextBox();
            this.ribTxtPort = new System.Windows.Forms.RibbonTextBox();
            this.ribbonPanelLink = new System.Windows.Forms.RibbonPanel();
            this.SuspendLayout();
            // 
            // ribTabView
            // 
            this.ribTabView.Name = "ribTabView";
            this.ribTabView.Panels.Add(this.ribbonPanelPresentation);
            this.ribTabView.Panels.Add(this.ribPanelAbout);
            this.ribTabView.Text = "View";
            // 
            // ribbonPanelPresentation
            // 
            this.ribbonPanelPresentation.Items.Add(this.ribChkBoxShowAvailableChannels);
            this.ribbonPanelPresentation.Items.Add(this.ribChkDarkTheme);
            this.ribbonPanelPresentation.Name = "ribbonPanelPresentation";
            this.ribbonPanelPresentation.Text = "Presentation";
            // 
            // ribChkBoxShowAvailableChannels
            // 
            this.ribChkBoxShowAvailableChannels.Checked = true;
            this.ribChkBoxShowAvailableChannels.Name = "ribChkBoxShowAvailableChannels";
            this.ribChkBoxShowAvailableChannels.Text = "Show Available Channels";
            this.ribChkBoxShowAvailableChannels.CheckBoxCheckChanged += new System.EventHandler(this.CheckBoxShowChannels_CheckChanged);
            // 
            // ribChkDarkTheme
            // 
            this.ribChkDarkTheme.Name = "ribChkDarkTheme";
            this.ribChkDarkTheme.Text = "Dark Theme";
            this.ribChkDarkTheme.CheckBoxCheckChanged += new System.EventHandler(this.RibChkDarkTheme_CheckBoxCheckChanged);
            // 
            // ribPanelAbout
            // 
            this.ribPanelAbout.Items.Add(this.rbnBtnAbout);
            this.ribPanelAbout.Name = "ribPanelAbout";
            this.ribPanelAbout.Text = "About";
            // 
            // rbnBtnAbout
            // 
            this.rbnBtnAbout.Image = ((System.Drawing.Image)(resources.GetObject("rbnBtnAbout.Image")));
            this.rbnBtnAbout.LargeImage = ((System.Drawing.Image)(resources.GetObject("rbnBtnAbout.LargeImage")));
            this.rbnBtnAbout.Name = "rbnBtnAbout";
            this.rbnBtnAbout.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbnBtnAbout.SmallImage")));
            this.rbnBtnAbout.Text = "About";
            this.rbnBtnAbout.Click += new System.EventHandler(this.RbnBtnAbout_Click);
            // 
            // ribbonTabSimulation
            // 
            this.ribbonTabSimulation.Name = "ribbonTabSimulation";
            this.ribbonTabSimulation.Panels.Add(this.ribbonPanelControl);
            this.ribbonTabSimulation.Panels.Add(this.ribbonPanelHardware);
            this.ribbonTabSimulation.Panels.Add(this.ribbonPanelAutolad);
            this.ribbonTabSimulation.Panels.Add(this.ribbonPanelTabs);
            this.ribbonTabSimulation.Panels.Add(this.ribbonPanelLogo);
            this.ribbonTabSimulation.Text = "Simulation";
            // 
            // ribbonPanelControl
            // 
            this.ribbonPanelControl.Items.Add(this.btPlay);
            this.ribbonPanelControl.Items.Add(this.btnPlayFast);
            this.ribbonPanelControl.Items.Add(this.btPause);
            this.ribbonPanelControl.Items.Add(this.btStop);
            this.ribbonPanelControl.Name = "ribbonPanelControl";
            this.ribbonPanelControl.Text = "Simulation";
            // 
            // btPlay
            // 
            this.btPlay.Image = global::ModeliChart.UI.Properties.Resources.appbar_control_play76x46;
            this.btPlay.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_control_play76x46;
            this.btPlay.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.btPlay.Name = "btPlay";
            this.btPlay.SmallImage = ((System.Drawing.Image)(resources.GetObject("btPlay.SmallImage")));
            this.btPlay.Text = "Play";
            this.btPlay.Click += new System.EventHandler(this.BtPlay_Click);
            // 
            // btnPlayFast
            // 
            this.btnPlayFast.Image = global::ModeliChart.UI.Properties.Resources.appbar_playfast_74x46;
            this.btnPlayFast.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_playfast_74x46;
            this.btnPlayFast.Name = "btnPlayFast";
            this.btnPlayFast.SmallImage = global::ModeliChart.UI.Properties.Resources.appbar_playfast_74x46;
            this.btnPlayFast.Text = "Play Fast";
            this.btnPlayFast.Click += new System.EventHandler(this.BtPlayFast_Click);
            // 
            // btPause
            // 
            this.btPause.Image = global::ModeliChart.UI.Properties.Resources.appbar_control_pause76x46;
            this.btPause.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_control_pause76x46;
            this.btPause.Name = "btPause";
            this.btPause.SmallImage = ((System.Drawing.Image)(resources.GetObject("btPause.SmallImage")));
            this.btPause.Text = "Pause";
            this.btPause.Click += new System.EventHandler(this.BtPause_Click);
            // 
            // btStop
            // 
            this.btStop.Image = ((System.Drawing.Image)(resources.GetObject("btStop.Image")));
            this.btStop.LargeImage = ((System.Drawing.Image)(resources.GetObject("btStop.LargeImage")));
            this.btStop.Name = "btStop";
            this.btStop.SmallImage = ((System.Drawing.Image)(resources.GetObject("btStop.SmallImage")));
            this.btStop.Text = "Stop";
            this.btStop.Click += new System.EventHandler(this.BtStop_Click);
            // 
            // ribbonPanelHardware
            // 
            this.ribbonPanelHardware.Items.Add(this.ribBtnConnectTCP);
            this.ribbonPanelHardware.Name = "ribbonPanelHardware";
            this.ribbonPanelHardware.Text = "HardwareIO";
            // 
            // ribBtnConnectTCP
            // 
            this.ribBtnConnectTCP.Image = global::ModeliChart.UI.Properties.Resources.appbar_control_connect76x46;
            this.ribBtnConnectTCP.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_control_connect76x46;
            this.ribBtnConnectTCP.Name = "ribBtnConnectTCP";
            this.ribBtnConnectTCP.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribBtnConnectTCP.SmallImage")));
            this.ribBtnConnectTCP.Text = "Connect TCP";
            this.ribBtnConnectTCP.Click += new System.EventHandler(this.RibBtnConnectTCP_Click);
            // 
            // ribbonPanelAutolad
            // 
            this.ribbonPanelAutolad.Items.Add(this.ribChkAutoload);
            this.ribbonPanelAutolad.Items.Add(this.ribBtnReloadParam);
            this.ribbonPanelAutolad.Items.Add(this.ribBtnClearAutoLoad);
            this.ribbonPanelAutolad.Name = "ribbonPanelAutolad";
            this.ribbonPanelAutolad.Text = "Autoload Parameters";
            // 
            // ribChkAutoload
            // 
            this.ribChkAutoload.Name = "ribChkAutoload";
            this.ribChkAutoload.Text = "Enable Autoload";
            this.ribChkAutoload.CheckBoxCheckChanged += new System.EventHandler(this.RibChkAutoload_CheckBoxCheckChanged);
            // 
            // ribBtnReloadParam
            // 
            this.ribBtnReloadParam.Enabled = false;
            this.ribBtnReloadParam.Image = global::ModeliChart.UI.Properties.Resources.appbar_reload_74x46;
            this.ribBtnReloadParam.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_reload_74x46;
            this.ribBtnReloadParam.Name = "ribBtnReloadParam";
            this.ribBtnReloadParam.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribBtnReloadParam.SmallImage")));
            this.ribBtnReloadParam.Text = "Reload Now";
            this.ribBtnReloadParam.Click += new System.EventHandler(this.RibBtnReloadParameters_Click);
            // 
            // ribBtnClearAutoLoad
            // 
            this.ribBtnClearAutoLoad.Image = global::ModeliChart.UI.Properties.Resources.cross;
            this.ribBtnClearAutoLoad.LargeImage = global::ModeliChart.UI.Properties.Resources.cross;
            this.ribBtnClearAutoLoad.Name = "ribBtnClearAutoLoad";
            this.ribBtnClearAutoLoad.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribBtnClearAutoLoad.SmallImage")));
            this.ribBtnClearAutoLoad.Text = "Clear Parameters";
            this.ribBtnClearAutoLoad.Click += new System.EventHandler(this.RibBtnClearAutoLoad_Click);
            // 
            // ribbonPanelTabs
            // 
            this.ribbonPanelTabs.Items.Add(this.ribbonButtonAddTab);
            this.ribbonPanelTabs.Name = "ribbonPanelTabs";
            this.ribbonPanelTabs.Text = "Tabs";
            // 
            // ribbonButtonAddTab
            // 
            this.ribbonButtonAddTab.Image = global::ModeliChart.UI.Properties.Resources.add_46;
            this.ribbonButtonAddTab.LargeImage = global::ModeliChart.UI.Properties.Resources.add_46;
            this.ribbonButtonAddTab.Name = "ribbonButtonAddTab";
            this.ribbonButtonAddTab.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButtonAddTab.SmallImage")));
            this.ribbonButtonAddTab.Text = "Instruments";
            this.ribbonButtonAddTab.Click += new System.EventHandler(this.RibbonButtonAddTab_Click);
            // 
            // ribbonPanelLogo
            // 
            this.ribbonPanelLogo.Items.Add(this.ribBtnLogo);
            this.ribbonPanelLogo.Name = "ribbonPanelLogo";
            this.ribbonPanelLogo.Text = null;
            // 
            // ribBtnLogo
            // 
            this.ribBtnLogo.Image = global::ModeliChart.UI.Properties.Resources.rwth_irt_de_rgb_281x68;
            this.ribBtnLogo.LargeImage = global::ModeliChart.UI.Properties.Resources.rwth_irt_de_rgb_281x68;
            this.ribBtnLogo.Name = "ribBtnLogo";
            this.ribBtnLogo.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribBtnLogo.SmallImage")));
            this.ribBtnLogo.Click += new System.EventHandler(this.RibBtnLogo_Click);
            // 
            // dockPanel
            // 
            this.dockPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dockPanel.DefaultFloatWindowSize = new System.Drawing.Size(720, 480);
            this.dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel.DockLeftPortion = 200D;
            this.dockPanel.DockRightPortion = 200D;
            this.dockPanel.DocumentStyle = WeifenLuo.WinFormsUI.Docking.DocumentStyle.DockingWindow;
            this.dockPanel.Location = new System.Drawing.Point(0, 160);
            this.dockPanel.Name = "dockPanel";
            this.dockPanel.Size = new System.Drawing.Size(962, 345);
            this.dockPanel.TabIndex = 1;
            // 
            // Ribbon
            // 
            this.Ribbon.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Ribbon.Location = new System.Drawing.Point(0, 0);
            this.Ribbon.Minimized = false;
            this.Ribbon.Name = "Ribbon";
            // 
            // 
            // 
            this.Ribbon.OrbDropDown.BorderRoundness = 8;
            this.Ribbon.OrbDropDown.ContentRecentItemsMinWidth = 0;
            this.Ribbon.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.Ribbon.OrbDropDown.MenuItems.Add(this.rbOrbItOpen);
            this.Ribbon.OrbDropDown.MenuItems.Add(this.rbOrbItSave);
            this.Ribbon.OrbDropDown.MenuItems.Add(this.ribOrbItExport);
            this.Ribbon.OrbDropDown.MenuItems.Add(this.ribOrbItImport);
            this.Ribbon.OrbDropDown.MenuItems.Add(this.ribSeperatorQuit);
            this.Ribbon.OrbDropDown.MenuItems.Add(this.ribbonOrbItQuit);
            this.Ribbon.OrbDropDown.Name = "";
            this.Ribbon.OrbDropDown.Size = new System.Drawing.Size(160, 295);
            this.Ribbon.OrbDropDown.TabIndex = 0;
            this.Ribbon.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2013;
            this.Ribbon.OrbText = "File";
            this.Ribbon.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F);
            this.Ribbon.Size = new System.Drawing.Size(962, 160);
            this.Ribbon.TabIndex = 0;
            this.Ribbon.Tabs.Add(this.ribbonTabSimulation);
            this.Ribbon.Tabs.Add(this.ribTabChannels);
            this.Ribbon.Tabs.Add(this.ribTabView);
            this.Ribbon.Tabs.Add(this.ribTabSettings);
            this.Ribbon.TabsMargin = new System.Windows.Forms.Padding(12, 30, 20, 0);
            this.Ribbon.TabSpacing = 4;
            this.Ribbon.Text = "ModeliChart";
            this.Ribbon.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            // 
            // rbOrbItOpen
            // 
            this.rbOrbItOpen.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.rbOrbItOpen.Image = global::ModeliChart.UI.Properties.Resources.appbar_open76x46;
            this.rbOrbItOpen.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_open76x46;
            this.rbOrbItOpen.Name = "rbOrbItOpen";
            this.rbOrbItOpen.SmallImage = global::ModeliChart.UI.Properties.Resources.appbar_open76x46;
            this.rbOrbItOpen.Text = "Open";
            this.rbOrbItOpen.Click += new System.EventHandler(this.RbOrbItOpen_Click);
            // 
            // rbOrbItSave
            // 
            this.rbOrbItSave.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.rbOrbItSave.Image = global::ModeliChart.UI.Properties.Resources.appbar_save_76x46;
            this.rbOrbItSave.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_save_76x46;
            this.rbOrbItSave.Name = "rbOrbItSave";
            this.rbOrbItSave.SmallImage = global::ModeliChart.UI.Properties.Resources.appbar_save_76x46;
            this.rbOrbItSave.Text = "Save";
            this.rbOrbItSave.Click += new System.EventHandler(this.RbOrbItSave_Click);
            // 
            // ribOrbItExport
            // 
            this.ribOrbItExport.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribOrbItExport.Image = global::ModeliChart.UI.Properties.Resources.appbar_export_74x46;
            this.ribOrbItExport.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_export_74x46;
            this.ribOrbItExport.Name = "ribOrbItExport";
            this.ribOrbItExport.SmallImage = global::ModeliChart.UI.Properties.Resources.appbar_export_74x46;
            this.ribOrbItExport.Text = "Export";
            this.ribOrbItExport.Click += new System.EventHandler(this.RibOrbItExport_Click);
            // 
            // ribOrbItImport
            // 
            this.ribOrbItImport.Image = global::ModeliChart.UI.Properties.Resources.appbar_import_74x46;
            this.ribOrbItImport.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_import_74x46;
            this.ribOrbItImport.Name = "ribOrbItImport";
            this.ribOrbItImport.SmallImage = global::ModeliChart.UI.Properties.Resources.appbar_import_74x46;
            this.ribOrbItImport.Text = "Import";
            this.ribOrbItImport.Click += new System.EventHandler(this.RibOrbItImport_Click);
            // 
            // ribSeperatorQuit
            // 
            this.ribSeperatorQuit.Name = "ribSeperatorQuit";
            // 
            // ribbonOrbItQuit
            // 
            this.ribbonOrbItQuit.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbItQuit.Image = ((System.Drawing.Image)(resources.GetObject("ribbonOrbItQuit.Image")));
            this.ribbonOrbItQuit.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbItQuit.LargeImage")));
            this.ribbonOrbItQuit.Name = "ribbonOrbItQuit";
            this.ribbonOrbItQuit.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonOrbItQuit.SmallImage")));
            this.ribbonOrbItQuit.Text = "Quit";
            this.ribbonOrbItQuit.Click += new System.EventHandler(this.RibbonOrbMenuItemQuit_Click);
            // 
            // ribTabChannels
            // 
            this.ribTabChannels.Name = "ribTabChannels";
            this.ribTabChannels.Panels.Add(this.ribPanLinks);
            this.ribTabChannels.Panels.Add(this.ribPanPresets);
            this.ribTabChannels.Text = "Channels";
            // 
            // ribPanLinks
            // 
            this.ribPanLinks.Items.Add(this.ribBtnLinks);
            this.ribPanLinks.Name = "ribPanLinks";
            this.ribPanLinks.Text = "Channel Links";
            // 
            // ribBtnLinks
            // 
            this.ribBtnLinks.Image = global::ModeliChart.UI.Properties.Resources.appbar_link_74x46;
            this.ribBtnLinks.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_link_74x46;
            this.ribBtnLinks.Name = "ribBtnLinks";
            this.ribBtnLinks.SmallImage = global::ModeliChart.UI.Properties.Resources.appbar_link_74x46;
            this.ribBtnLinks.Text = "Links";
            this.ribBtnLinks.Click += new System.EventHandler(this.RibbonButtonLink_Click);
            // 
            // ribPanPresets
            // 
            this.ribPanPresets.Items.Add(this.ribBtnPreset);
            this.ribPanPresets.Name = "ribPanPresets";
            this.ribPanPresets.Text = "Presets";
            // 
            // ribBtnPreset
            // 
            this.ribBtnPreset.Image = global::ModeliChart.UI.Properties.Resources.appbar_checks_74x46;
            this.ribBtnPreset.LargeImage = global::ModeliChart.UI.Properties.Resources.appbar_checks_74x46;
            this.ribBtnPreset.Name = "ribBtnPreset";
            this.ribBtnPreset.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribBtnPreset.SmallImage")));
            this.ribBtnPreset.Text = "En-/Disable";
            this.ribBtnPreset.Click += new System.EventHandler(this.RibBtnPreset_Click);
            // 
            // ribTabSettings
            // 
            this.ribTabSettings.Name = "ribTabSettings";
            this.ribTabSettings.Panels.Add(this.ribPanelSimInterval);
            this.ribTabSettings.Panels.Add(this.ribPanelUIRefresh);
            this.ribTabSettings.Panels.Add(this.ribPanelRemoteHost);
            this.ribTabSettings.Text = "Settings";
            // 
            // ribPanelSimInterval
            // 
            this.ribPanelSimInterval.Items.Add(this.ribLblSimInterval);
            this.ribPanelSimInterval.Items.Add(this.ribTxtSimInterval);
            this.ribPanelSimInterval.Name = "ribPanelSimInterval";
            this.ribPanelSimInterval.Text = "[Hz] Play!";
            // 
            // ribLblSimInterval
            // 
            this.ribLblSimInterval.Name = "ribLblSimInterval";
            this.ribLblSimInterval.Text = "Simulation frequency:";
            // 
            // ribTxtSimInterval
            // 
            this.ribTxtSimInterval.Name = "ribTxtSimInterval";
            this.ribTxtSimInterval.TextBoxText = "";
            this.ribTxtSimInterval.TextBoxTextChanged += new System.EventHandler(this.RibTxtSimInterval_TextBoxTextChanged);
            // 
            // ribPanelUIRefresh
            // 
            this.ribPanelUIRefresh.Items.Add(this.ribLblUIInterval);
            this.ribPanelUIRefresh.Items.Add(this.ribTxtUIInterval);
            this.ribPanelUIRefresh.Name = "ribPanelUIRefresh";
            this.ribPanelUIRefresh.Text = "[Hz]";
            // 
            // ribLblUIInterval
            // 
            this.ribLblUIInterval.Name = "ribLblUIInterval";
            this.ribLblUIInterval.Text = "Refresh frequency:";
            // 
            // ribTxtUIInterval
            // 
            this.ribTxtUIInterval.Name = "ribTxtUIInterval";
            this.ribTxtUIInterval.TextBoxText = "";
            this.ribTxtUIInterval.TextBoxTextChanged += new System.EventHandler(this.RibTxtUIInterval_TextBoxTextChanged);
            // 
            // ribPanelRemoteHost
            // 
            this.ribPanelRemoteHost.Items.Add(this.ribLblRemoteHost);
            this.ribPanelRemoteHost.Items.Add(this.ribTxtRemoteHost);
            this.ribPanelRemoteHost.Items.Add(this.ribTxtPort);
            this.ribPanelRemoteHost.Name = "ribPanelRemoteHost";
            this.ribPanelRemoteHost.Text = "IP-Endpoint";
            // 
            // ribLblRemoteHost
            // 
            this.ribLblRemoteHost.Name = "ribLblRemoteHost";
            this.ribLblRemoteHost.Text = "Remote Connection:";
            // 
            // ribTxtRemoteHost
            // 
            this.ribTxtRemoteHost.Name = "ribTxtRemoteHost";
            this.ribTxtRemoteHost.Text = "IP: ";
            this.ribTxtRemoteHost.TextBoxText = "localhost";
            // 
            // ribTxtPort
            // 
            this.ribTxtPort.Name = "ribTxtPort";
            this.ribTxtPort.Text = "Port:";
            this.ribTxtPort.TextBoxText = "52062";
            // 
            // ribbonPanelLink
            // 
            this.ribbonPanelLink.Name = "ribbonPanelLink";
            this.ribbonPanelLink.Text = "Channel Link";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 505);
            this.Controls.Add(this.dockPanel);
            this.Controls.Add(this.Ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainWindow";
            this.Text = "ModeliChart";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RibbonTab ribTabView;
        private System.Windows.Forms.RibbonPanel ribbonPanelPresentation;
        private System.Windows.Forms.RibbonTab ribbonTabSimulation;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel;
        public  System.Windows.Forms.RibbonCheckBox ribChkBoxShowAvailableChannels;
        private System.Windows.Forms.RibbonPanel ribbonPanelControl;
        private System.Windows.Forms.RibbonButton btPlay;
        private System.Windows.Forms.RibbonButton btPause;
        private System.Windows.Forms.RibbonOrbMenuItem rbOrbItOpen;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbItQuit;
        private System.Windows.Forms.Ribbon Ribbon;
        private System.Windows.Forms.RibbonButton btStop;
        private System.Windows.Forms.RibbonPanel ribbonPanelHardware;
        private System.Windows.Forms.RibbonOrbMenuItem rbOrbItSave;
        private System.Windows.Forms.RibbonTab ribTabChannels;
        private System.Windows.Forms.RibbonPanel ribPanLinks;
        private System.Windows.Forms.RibbonButton ribBtnLinks;
        private System.Windows.Forms.RibbonPanel ribbonPanelLink;
        private System.Windows.Forms.RibbonPanel ribPanPresets;
        private System.Windows.Forms.RibbonButton ribBtnPreset;
        private System.Windows.Forms.RibbonOrbMenuItem ribOrbItExport;
        private System.Windows.Forms.RibbonSeparator ribSeperatorQuit;
        private System.Windows.Forms.RibbonTab ribTabSettings;
        private System.Windows.Forms.RibbonPanel ribPanelSimInterval;
        private System.Windows.Forms.RibbonLabel ribLblSimInterval;
        private System.Windows.Forms.RibbonTextBox ribTxtSimInterval;
        private System.Windows.Forms.RibbonPanel ribPanelUIRefresh;
        private System.Windows.Forms.RibbonLabel ribLblUIInterval;
        private System.Windows.Forms.RibbonTextBox ribTxtUIInterval;
        private System.Windows.Forms.RibbonButton ribOrbItImport;
        private System.Windows.Forms.RibbonButton btnPlayFast;
        private System.Windows.Forms.RibbonPanel ribbonPanelAutolad;
        private System.Windows.Forms.RibbonCheckBox ribChkAutoload;
        private System.Windows.Forms.RibbonButton ribBtnReloadParam;
        private System.Windows.Forms.RibbonPanel ribbonPanelLogo;
        private System.Windows.Forms.RibbonButton ribBtnLogo;
        private System.Windows.Forms.RibbonButton ribBtnConnectTCP;
        private System.Windows.Forms.RibbonPanel ribPanelRemoteHost;
        private System.Windows.Forms.RibbonLabel ribLblRemoteHost;
        private System.Windows.Forms.RibbonTextBox ribTxtRemoteHost;
        private System.Windows.Forms.RibbonCheckBox ribChkDarkTheme;
        private System.Windows.Forms.RibbonTextBox ribTxtPort;
        private System.Windows.Forms.RibbonPanel ribbonPanelTabs;
        private System.Windows.Forms.RibbonButton ribbonButtonAddTab;
        private System.Windows.Forms.RibbonButton ribBtnClearAutoLoad;
        private System.Windows.Forms.RibbonPanel ribPanelAbout;
        private System.Windows.Forms.RibbonButton rbnBtnAbout;
    }
}