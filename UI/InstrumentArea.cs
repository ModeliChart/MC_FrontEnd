﻿using ModeliChart.Log;
using ModeliChart.UserControls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;

namespace ModeliChart.UI
{
    public partial class InstrumentArea : DockContent
    {
        private string title;
        private readonly List<UniversalOxyInstrument> instruments = new List<UniversalOxyInstrument>();


        public InstrumentArea(string title, IEnumerable<UniversalOxyInstrument> instruments)
        {
            InitializeComponent();
            // Add the Instruments to the Area
            foreach(var instrument in instruments)
            {
                AddInstrument(instrument);
            }
            // Setup TabPage Context Menu
            ContextMenu contextMenu = new ContextMenu();
            MenuItem renameItem = new MenuItem("Rename");
            renameItem.Click += RenameItem_Click;
            contextMenu.MenuItems.Add(renameItem);
            TabPageContextMenu = contextMenu;
        }

        public IEnumerable<UniversalOxyInstrument> Instruments => instruments;

        private void AddInstrument(UniversalOxyInstrument instrument)
        {
            instruments.Add(instrument);
            tableLayoutPanel.Controls.Add(instrument);
            instrument.Dock = DockStyle.Fill;
        }

        public String Title
        {
            get => title;
            set
            {
                title = value;
                Text = value;
            }
        }

        /// <summary>
        /// Updates the instruments data to the given time.
        /// It does not refresh the view, call InvalidateArea!
        /// </summary>
        /// <param name="value"></param>
        public void UpdateDataToTime(double time)
        {
            foreach(var instrument in instruments)
            {
                instrument.UpdateDataToTime(time);
            }
        }

        /// <summary>
        /// Refreshes the data view.
        /// </summary>
        public void InvalidateArea(double currentTime)
        {
            foreach(var instrument in instruments)
            {
                instrument.InvalidateInstrument(currentTime);
            }
        }

        void RenameItem_Click(object sender, EventArgs e)
        {
            Title = InputMessageBox.Show("Give this instrument area a new title", "Rename instrument area");
        }
    }
}