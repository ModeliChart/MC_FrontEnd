﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using ModeliChart;
using ModeliChart.Basics;
namespace ModeliChart.UserControls
{
    public partial class GlobalControl : UserControl,ISkinnable
    {
        private AvailableChannels _myAvailableChannels;
        private Form _myAvailableChannelsForm = new Form();
        private ICollection<ISkinnable> _registeredControls;
        private IDictionary<string, IChannel<double>> _registeredChannels;

        public GlobalControl()
        {
            InitializeComponent();
            _registeredControls = new List<ISkinnable>();
            _registeredChannels = new Dictionary<string, IChannel<double>>();
        }
        public void registerControl(ISkinnable control){
            _registeredControls.Add(control);
        }
        public void registerChannel(IChannel<double> channel)
        {
            _registeredChannels.Add(channel.Name,channel);
        }
        private void applyNewSkin(Skin skin)
        {
            foreach (var item in _registeredControls)
            {
                item.Skin = skin;
            }
        }


        private void checkBoxShowChannels_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxShowChannels.Checked == true)
            {
                //if (_myAvailableChannels == null)
                //{
                //    //_myAvailableChannels = new AvailableChannels(_registeredChannels);
                //    _myAvailableChannelsForm.Controls.Add(_myAvailableChannels);
                //    _myAvailableChannelsForm.Width = _myAvailableChannels.Width;
                //    _myAvailableChannelsForm.Height = _myAvailableChannels.Height;
                //    _myAvailableChannelsForm.Show();
                //}
                //else
                //{
                //    _myAvailableChannelsForm.Show();
                //}
            }
            else 
            {
                _myAvailableChannelsForm.Hide();
            }
        }
        public Skin Skin
        {
            set {
#warning 'Adapt Colors'
                //this.designToolStripMenuItem.ForeColor = value.myForeColor;
                //this.grauToolStripMenuItem.ForeColor = value.myForeColor; 
                //this.weißToolStripMenuItem.ForeColor = value.myForeColor;
                //this.checkBoxShowChannels.ForeColor = value.myForeColor;
                //this.checkBoxShowChannels.BackColor = value.myBackColor;
                //this.toolStrip1.Renderer = value.myModeliChartToolStripRenderer;
            }
        }
    }
}
