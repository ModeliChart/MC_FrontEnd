﻿namespace ModeliChart.UserControls
{
    partial class OxyPlotLineInstrumentBase
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OxyPlotLineInstrumentBase));
            this.trackBarSetter = new System.Windows.Forms.TrackBar();
            this.plotView = new OxyPlot.WindowsForms.PlotView();
            this.toolStripOxy = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButtonPlotSettings = new System.Windows.Forms.ToolStripDropDownButton();
            this.menuItemTimeSpan = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem10s = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItem20s = new System.Windows.Forms.ToolStripMenuItem();
            this.menutItem30s = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemCustomTime = new System.Windows.Forms.ToolStripMenuItem();
            this.themeColorPicker = new ExHtmlEditor.ColorPicker.ThemeColorPickerToolStripSplitButton();
            this.buttonAutoScale = new System.Windows.Forms.ToolStripButton();
            this.panelToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSetter)).BeginInit();
            this.toolStripOxy.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelToolBar
            // 
            this.panelToolBar.Controls.Add(this.toolStripOxy);
            this.panelToolBar.Size = new System.Drawing.Size(640, 26);
            this.panelToolBar.Controls.SetChildIndex(this.toolStripOxy, 0);
            // 
            // trackBarSetter
            // 
            this.trackBarSetter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarSetter.Location = new System.Drawing.Point(4, 3);
            this.trackBarSetter.Maximum = 25;
            this.trackBarSetter.Name = "trackBarSetter";
            this.trackBarSetter.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarSetter.Size = new System.Drawing.Size(45, 170);
            this.trackBarSetter.TabIndex = 5;
            this.trackBarSetter.Value = 12;
            this.trackBarSetter.ValueChanged += new System.EventHandler(this.TrackBarSetter_ValueChanged);
            this.trackBarSetter.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TrackBarSetter_MouseDown);
            this.trackBarSetter.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackBarSetter_MouseUp);
            // 
            // plot
            // 
            this.plotView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plotView.Location = new System.Drawing.Point(0, 0);
            this.plotView.Name = "plot";
            this.plotView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plotView.Size = new System.Drawing.Size(0, 0);
            this.plotView.TabIndex = 1;
            this.plotView.Text = "plot1";
            this.plotView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plotView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plotView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            this.plotView.DoubleClick += new System.EventHandler(this.Plot_DoubleClick);
            this.plotView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OxyPlotLineInstrumentBase_KeyDown);
            this.plotView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OxyPlotLineInstrumentBase_KeyUp);
            // 
            // toolStripOxy
            // 
            this.toolStripOxy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripOxy.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButtonPlotSettings,
            this.themeColorPicker,
            this.buttonAutoScale});
            this.toolStripOxy.Location = new System.Drawing.Point(0, 0);
            this.toolStripOxy.Name = "toolStripOxy";
            this.toolStripOxy.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripOxy.Size = new System.Drawing.Size(640, 26);
            this.toolStripOxy.TabIndex = 20;
            this.toolStripOxy.Text = "toolStripOxy";
            // 
            // toolStripDropDownButtonPlotSettings
            // 
            this.toolStripDropDownButtonPlotSettings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripDropDownButtonPlotSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButtonPlotSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemTimeSpan});
            this.toolStripDropDownButtonPlotSettings.Image = global::ModeliChart.UserControls.Properties.Resources.options1;
            this.toolStripDropDownButtonPlotSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonPlotSettings.Name = "toolStripDropDownButtonPlotSettings";
            this.toolStripDropDownButtonPlotSettings.Size = new System.Drawing.Size(29, 23);
            this.toolStripDropDownButtonPlotSettings.Text = "Settings";
            this.toolStripDropDownButtonPlotSettings.ToolTipText = "Settings";
            // 
            // menuItemTimeSpan
            // 
            this.menuItemTimeSpan.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem10s,
            this.menuItem20s,
            this.menutItem30s,
            this.menuItemCustomTime});
            this.menuItemTimeSpan.Name = "menuItemTimeSpan";
            this.menuItemTimeSpan.Size = new System.Drawing.Size(126, 22);
            this.menuItemTimeSpan.Text = "Timespan";
            // 
            // menuItem10s
            // 
            this.menuItem10s.Name = "menuItem10s";
            this.menuItem10s.Size = new System.Drawing.Size(116, 22);
            this.menuItem10s.Text = "10s";
            this.menuItem10s.Click += new System.EventHandler(this.MenuItem10s_Click);
            // 
            // menuItem20s
            // 
            this.menuItem20s.Name = "menuItem20s";
            this.menuItem20s.Size = new System.Drawing.Size(116, 22);
            this.menuItem20s.Text = "20s";
            this.menuItem20s.Click += new System.EventHandler(this.MenuItem20s_Click);
            // 
            // menutItem30s
            // 
            this.menutItem30s.Name = "menutItem30s";
            this.menutItem30s.Size = new System.Drawing.Size(116, 22);
            this.menutItem30s.Text = "30s";
            this.menutItem30s.Click += new System.EventHandler(this.MenuItem30s_Click);
            // 
            // menuItemCustomTime
            // 
            this.menuItemCustomTime.Name = "menuItemCustomTime";
            this.menuItemCustomTime.Size = new System.Drawing.Size(116, 22);
            this.menuItemCustomTime.Text = "Custom";
            this.menuItemCustomTime.Click += new System.EventHandler(this.MenuItemCustomTime_Click);
            // 
            // themeColorPicker
            // 
            this.themeColorPicker.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.themeColorPicker.Color = System.Drawing.Color.White;
            this.themeColorPicker.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.themeColorPicker.Image = ((System.Drawing.Image)(resources.GetObject("themeColorPicker.Image")));
            this.themeColorPicker.ImageHeight = 16;
            this.themeColorPicker.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.themeColorPicker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.themeColorPicker.ImageWidth = 32;
            this.themeColorPicker.Name = "themeColorPicker";
            this.themeColorPicker.Size = new System.Drawing.Size(48, 23);
            this.themeColorPicker.ToolTipText = "Set a color";
            this.themeColorPicker.ColorSelected += new ExHtmlEditor.ColorPicker.ThemeColorPickerToolStripSplitButton.colorSelected(this.ColorSelected);
            // 
            // buttonAutoScale
            // 
            this.buttonAutoScale.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.buttonAutoScale.BackColor = System.Drawing.Color.Transparent;
            this.buttonAutoScale.BackgroundImage = global::ModeliChart.UserControls.Properties.Resources.zoom;
            this.buttonAutoScale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonAutoScale.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonAutoScale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAutoScale.Name = "buttonAutoScale";
            this.buttonAutoScale.Size = new System.Drawing.Size(23, 23);
            this.buttonAutoScale.Text = "Auto Scale";
            this.buttonAutoScale.ToolTipText = "Autoscale";
            this.buttonAutoScale.Click += new System.EventHandler(this.ToolStripButtonAutoScale_Click);
            // 
            // OxyPlotLineInstrumentBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "OxyPlotLineInstrumentBase";
            this.Size = new System.Drawing.Size(642, 332);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OxyPlotLineInstrumentBase_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OxyPlotLineInstrumentBase_KeyUp);
            this.panelToolBar.ResumeLayout(false);
            this.panelToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSetter)).EndInit();
            this.toolStripOxy.ResumeLayout(false);
            this.toolStripOxy.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBarSetter;
        protected OxyPlot.WindowsForms.PlotView plotView;
        protected System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButtonPlotSettings;
        private System.Windows.Forms.ToolStripMenuItem menuItemTimeSpan;
        private System.Windows.Forms.ToolStripMenuItem menuItem10s;
        private System.Windows.Forms.ToolStripMenuItem menuItem20s;
        private System.Windows.Forms.ToolStripMenuItem menutItem30s;
        protected ExHtmlEditor.ColorPicker.ThemeColorPickerToolStripSplitButton themeColorPicker;
        protected System.Windows.Forms.ToolStripButton buttonAutoScale;
        protected System.Windows.Forms.ToolStrip toolStripOxy;
        private System.Windows.Forms.ToolStripMenuItem menuItemCustomTime;
    }
}

