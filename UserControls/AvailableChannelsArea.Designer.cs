﻿using ModeliChart.Basics;
using System.Windows.Forms;


namespace ModeliChart.UserControls
{
    partial class AvailableChannelsArea
    {


        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeColumnInfo = new Aga.Controls.Tree.TreeColumn();
            this.icoSettable = new Aga.Controls.Tree.NodeControls.NodeIcon();
            this.txtTitle = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.txtValue = new Aga.Controls.Tree.NodeControls.NodeTextBox();
            this.tblLayoutBase = new System.Windows.Forms.TableLayoutPanel();
            this._availableChannelEntry = new ModeliChart.UserControls.AvailableChannelEntry();
            this.treeView = new Aga.Controls.Tree.TreeViewAdv();
            this.treeColumnValue = new Aga.Controls.Tree.TreeColumn();
            this.chkBoxFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.chkSettables = new System.Windows.Forms.CheckBox();
            this.tblLayoutBase.SuspendLayout();
            this.chkBoxFlow.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeColumnInfo
            // 
            this.treeColumnInfo.Header = "Information";
            this.treeColumnInfo.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumnInfo.TooltipText = "Information";
            this.treeColumnInfo.Width = 120;
            // 
            // icoSettable
            // 
            this.icoSettable.DataPropertyName = "Icon";
            this.icoSettable.LeftMargin = 1;
            this.icoSettable.ParentColumn = this.treeColumnInfo;
            this.icoSettable.ScaleMode = Aga.Controls.Tree.ImageScaleMode.Fit;
            // 
            // txtTitle
            // 
            this.txtTitle.DataPropertyName = "Title";
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.IncrementalSearchEnabled = true;
            this.txtTitle.LeftMargin = 3;
            this.txtTitle.ParentColumn = this.treeColumnInfo;
            // 
            // txtValue
            // 
            this.txtValue.DataPropertyName = "Value";
            this.txtValue.EditEnabled = true;
            this.txtValue.EditOnClick = true;
            this.txtValue.IncrementalSearchEnabled = true;
            this.txtValue.LeftMargin = 3;
            this.txtValue.ParentColumn = null;
            // 
            // tblLayoutBase
            // 
            this.tblLayoutBase.ColumnCount = 1;
            this.tblLayoutBase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutBase.Controls.Add(this._availableChannelEntry, 0, 2);
            this.tblLayoutBase.Controls.Add(this.treeView, 0, 1);
            this.tblLayoutBase.Controls.Add(this.chkBoxFlow, 0, 0);
            this.tblLayoutBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblLayoutBase.Location = new System.Drawing.Point(0, 0);
            this.tblLayoutBase.Margin = new System.Windows.Forms.Padding(6);
            this.tblLayoutBase.Name = "tblLayoutBase";
            this.tblLayoutBase.RowCount = 3;
            this.tblLayoutBase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblLayoutBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblLayoutBase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tblLayoutBase.Size = new System.Drawing.Size(460, 955);
            this.tblLayoutBase.TabIndex = 2;
            // 
            // _availableChannelEntry
            // 
            this._availableChannelEntry.BackColor = System.Drawing.SystemColors.Control;
            this._availableChannelEntry.Channel = null;
            this._availableChannelEntry.ChannelDescription = "Description";
            this._availableChannelEntry.ChannelName = "Name";
            this._availableChannelEntry.DisplayChannelType = ModeliChart.Basics.ChannelType.None;
            this._availableChannelEntry.Dock = System.Windows.Forms.DockStyle.Fill;
            this._availableChannelEntry.ForeColor = System.Drawing.Color.Black;
            this._availableChannelEntry.Location = new System.Drawing.Point(12, 767);
            this._availableChannelEntry.Margin = new System.Windows.Forms.Padding(12);
            this._availableChannelEntry.MinimumSize = new System.Drawing.Size(4, 154);
            this._availableChannelEntry.Name = "_availableChannelEntry";
            this._availableChannelEntry.Settable = false;
            this._availableChannelEntry.Size = new System.Drawing.Size(436, 176);
            this._availableChannelEntry.TabIndex = 10;
            this._availableChannelEntry.Unit = "Unit";
            // 
            // treeView
            // 
            this.treeView.AutoRowHeight = true;
            this.treeView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView.Columns.Add(this.treeColumnInfo);
            this.treeView.Columns.Add(this.treeColumnValue);
            this.treeView.DefaultToolTipProvider = null;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.DragDropMarkColor = System.Drawing.Color.Black;
            this.treeView.LineColor = System.Drawing.SystemColors.ControlDarkDark;
            this.treeView.LoadOnDemand = true;
            this.treeView.Location = new System.Drawing.Point(6, 59);
            this.treeView.Margin = new System.Windows.Forms.Padding(6);
            this.treeView.Model = null;
            this.treeView.Name = "treeView";
            this.treeView.NodeControls.Add(this.icoSettable);
            this.treeView.NodeControls.Add(this.txtTitle);
            this.treeView.NodeControls.Add(this.txtValue);
            this.treeView.RowHeight = 24;
            this.treeView.SelectedNode = null;
            this.treeView.ShowNodeToolTips = true;
            this.treeView.Size = new System.Drawing.Size(448, 690);
            this.treeView.TabIndex = 9;
            this.treeView.UseColumns = true;
            this.treeView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.TreeView_ItemDrag);
            this.treeView.NodeMouseClick += new System.EventHandler<Aga.Controls.Tree.TreeNodeAdvMouseEventArgs>(this.TreeView_NodeMouseClick);
            this.treeView.SelectionChanged += new System.EventHandler(this.TreeView_SelectionChanged);
            this.treeView.SizeChanged += new System.EventHandler(this.TreeView_SizeChanged);
            this.treeView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TreeView_KeyUp);
            this.treeView.MouseLeave += new System.EventHandler(this.TreeView_MouseLeave);
            // 
            // treeColumnValue
            // 
            this.treeColumnValue.Header = "Value";
            this.treeColumnValue.SortOrder = System.Windows.Forms.SortOrder.None;
            this.treeColumnValue.TooltipText = "Value";
            // 
            // chkBoxFlow
            // 
            this.chkBoxFlow.AutoSize = true;
            this.chkBoxFlow.Controls.Add(this.chkAll);
            this.chkBoxFlow.Controls.Add(this.chkSettables);
            this.chkBoxFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkBoxFlow.Location = new System.Drawing.Point(6, 6);
            this.chkBoxFlow.Margin = new System.Windows.Forms.Padding(6);
            this.chkBoxFlow.Name = "chkBoxFlow";
            this.chkBoxFlow.Size = new System.Drawing.Size(448, 41);
            this.chkBoxFlow.TabIndex = 7;
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Checked = true;
            this.chkAll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAll.Location = new System.Drawing.Point(6, 6);
            this.chkAll.Margin = new System.Windows.Forms.Padding(6);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(133, 29);
            this.chkAll.TabIndex = 7;
            this.chkAll.Text = "Check all";
            this.chkAll.UseVisualStyleBackColor = true;
            this.chkAll.CheckedChanged += new System.EventHandler(this.ChkAll_CheckedChanged);
            // 
            // chkSettables
            // 
            this.chkSettables.AutoSize = true;
            this.chkSettables.Location = new System.Drawing.Point(151, 6);
            this.chkSettables.Margin = new System.Windows.Forms.Padding(6);
            this.chkSettables.Name = "chkSettables";
            this.chkSettables.Size = new System.Drawing.Size(198, 29);
            this.chkSettables.TabIndex = 8;
            this.chkSettables.Text = "Check settables";
            this.chkSettables.UseVisualStyleBackColor = true;
            this.chkSettables.Click += new System.EventHandler(this.ChkSettables_Click);
            // 
            // AvailableChannelsArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tblLayoutBase);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "AvailableChannelsArea";
            this.Size = new System.Drawing.Size(460, 955);
            this.tblLayoutBase.ResumeLayout(false);
            this.tblLayoutBase.PerformLayout();
            this.chkBoxFlow.ResumeLayout(false);
            this.chkBoxFlow.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        private Aga.Controls.Tree.NodeControls.NodeTextBox txtTitle;
        private Aga.Controls.Tree.NodeControls.NodeIcon icoSettable;
        private Aga.Controls.Tree.TreeColumn treeColumnInfo;
        private Aga.Controls.Tree.NodeControls.NodeTextBox txtValue;
        private TableLayoutPanel tblLayoutBase;
        private Aga.Controls.Tree.TreeColumn treeColumnValue;
        private FlowLayoutPanel chkBoxFlow;
        private CheckBox chkAll;
        private CheckBox chkSettables;
        private AvailableChannelEntry _availableChannelEntry;
        private Aga.Controls.Tree.TreeViewAdv treeView;
    }
}
