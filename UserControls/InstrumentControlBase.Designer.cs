﻿using System.Windows.Forms;
namespace ModeliChart.UserControls
{
    partial class InstrumentControlBase
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstrumentControlBase));
            this.tableLayoutPanelBase = new System.Windows.Forms.TableLayoutPanel();
            this.panelSetter = new System.Windows.Forms.Panel();
            this.tableLayoutPanelSetter = new System.Windows.Forms.TableLayoutPanel();
            this.panelPlusMinus = new System.Windows.Forms.Panel();
            this.txtBoxSetValue = new System.Windows.Forms.TextBox();
            this.panelTrackBar = new System.Windows.Forms.Panel();
            this.panelInstrument = new System.Windows.Forms.Panel();
            this.splitContainerBase = new System.Windows.Forms.SplitContainer();
            this.panelToolBar = new System.Windows.Forms.Panel();
            this.toolStripBase = new System.Windows.Forms.ToolStrip();
            this.toolStripSplitButtonChannels = new System.Windows.Forms.ToolStripSplitButton();
            this.tableLayoutPanelBase.SuspendLayout();
            this.panelSetter.SuspendLayout();
            this.tableLayoutPanelSetter.SuspendLayout();
            this.panelPlusMinus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerBase)).BeginInit();
            this.splitContainerBase.Panel1.SuspendLayout();
            this.splitContainerBase.Panel2.SuspendLayout();
            this.splitContainerBase.SuspendLayout();
            this.panelToolBar.SuspendLayout();
            this.toolStripBase.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelBase
            // 
            this.tableLayoutPanelBase.ColumnCount = 2;
            this.tableLayoutPanelBase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelBase.Controls.Add(this.panelSetter, 0, 0);
            this.tableLayoutPanelBase.Controls.Add(this.panelInstrument, 0, 0);
            this.tableLayoutPanelBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelBase.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelBase.Name = "tableLayoutPanelBase";
            this.tableLayoutPanelBase.RowCount = 1;
            this.tableLayoutPanelBase.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelBase.Size = new System.Drawing.Size(605, 263);
            this.tableLayoutPanelBase.TabIndex = 13;
            // 
            // panelSetter
            // 
            this.panelSetter.AutoSize = true;
            this.panelSetter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelSetter.Controls.Add(this.tableLayoutPanelSetter);
            this.panelSetter.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelSetter.Location = new System.Drawing.Point(540, 3);
            this.panelSetter.Name = "panelSetter";
            this.panelSetter.Size = new System.Drawing.Size(62, 257);
            this.panelSetter.TabIndex = 3;
            // 
            // tableLayoutPanelSetter
            // 
            this.tableLayoutPanelSetter.AutoSize = true;
            this.tableLayoutPanelSetter.ColumnCount = 1;
            this.tableLayoutPanelSetter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSetter.Controls.Add(this.panelPlusMinus, 0, 1);
            this.tableLayoutPanelSetter.Controls.Add(this.panelTrackBar, 0, 0);
            this.tableLayoutPanelSetter.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanelSetter.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelSetter.Name = "tableLayoutPanelSetter";
            this.tableLayoutPanelSetter.RowCount = 2;
            this.tableLayoutPanelSetter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelSetter.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelSetter.Size = new System.Drawing.Size(62, 257);
            this.tableLayoutPanelSetter.TabIndex = 5;
            // 
            // panelPlusMinus
            // 
            this.panelPlusMinus.AutoSize = true;
            this.panelPlusMinus.Controls.Add(this.txtBoxSetValue);
            this.panelPlusMinus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPlusMinus.Location = new System.Drawing.Point(3, 228);
            this.panelPlusMinus.Name = "panelPlusMinus";
            this.panelPlusMinus.Size = new System.Drawing.Size(56, 26);
            this.panelPlusMinus.TabIndex = 0;
            // 
            // txtBoxSetValue
            // 
            this.txtBoxSetValue.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtBoxSetValue.Location = new System.Drawing.Point(3, 3);
            this.txtBoxSetValue.Name = "txtBoxSetValue";
            this.txtBoxSetValue.Size = new System.Drawing.Size(50, 20);
            this.txtBoxSetValue.TabIndex = 7;
            this.txtBoxSetValue.Text = "Set value";
            this.txtBoxSetValue.Click += new System.EventHandler(this.TxtBoxSetValue_Click);
            this.txtBoxSetValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtBoxSetValue_KeyDown);
            // 
            // panelTrackBar
            // 
            this.panelTrackBar.AutoSize = true;
            this.panelTrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTrackBar.Location = new System.Drawing.Point(3, 3);
            this.panelTrackBar.Name = "panelTrackBar";
            this.panelTrackBar.Size = new System.Drawing.Size(56, 219);
            this.panelTrackBar.TabIndex = 1;
            // 
            // panelInstrument
            // 
            this.panelInstrument.BackColor = System.Drawing.SystemColors.Window;
            this.panelInstrument.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInstrument.Location = new System.Drawing.Point(3, 3);
            this.panelInstrument.Name = "panelInstrument";
            this.panelInstrument.Size = new System.Drawing.Size(531, 257);
            this.panelInstrument.TabIndex = 2;
            // 
            // splitContainerBase
            // 
            this.splitContainerBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainerBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerBase.Location = new System.Drawing.Point(0, 0);
            this.splitContainerBase.Name = "splitContainerBase";
            this.splitContainerBase.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerBase.Panel1
            // 
            this.splitContainerBase.Panel1.Controls.Add(this.panelToolBar);
            // 
            // splitContainerBase.Panel2
            // 
            this.splitContainerBase.Panel2.Controls.Add(this.tableLayoutPanelBase);
            this.splitContainerBase.Size = new System.Drawing.Size(607, 294);
            this.splitContainerBase.SplitterDistance = 25;
            this.splitContainerBase.TabIndex = 16;
            // 
            // panelToolBar
            // 
            this.panelToolBar.AutoSize = true;
            this.panelToolBar.Controls.Add(this.toolStripBase);
            this.panelToolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelToolBar.Location = new System.Drawing.Point(0, 0);
            this.panelToolBar.Name = "panelToolBar";
            this.panelToolBar.Size = new System.Drawing.Size(605, 23);
            this.panelToolBar.TabIndex = 0;
            // 
            // toolStripBase
            // 
            this.toolStripBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripBase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButtonChannels});
            this.toolStripBase.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStripBase.Location = new System.Drawing.Point(0, 0);
            this.toolStripBase.Name = "toolStripBase";
            this.toolStripBase.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripBase.Size = new System.Drawing.Size(605, 23);
            this.toolStripBase.TabIndex = 19;
            // 
            // toolStripSplitButtonChannels
            // 
            this.toolStripSplitButtonChannels.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButtonChannels.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButtonChannels.Image")));
            this.toolStripSplitButtonChannels.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButtonChannels.Name = "toolStripSplitButtonChannels";
            this.toolStripSplitButtonChannels.Size = new System.Drawing.Size(72, 20);
            this.toolStripSplitButtonChannels.Text = "Channels";
            this.toolStripSplitButtonChannels.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ToolStripSplitButtonEnum_DropDownItemClicked);
            // 
            // InstrumentControlBase
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.splitContainerBase);
            this.Name = "InstrumentControlBase";
            this.Size = new System.Drawing.Size(607, 294);
            this.tableLayoutPanelBase.ResumeLayout(false);
            this.tableLayoutPanelBase.PerformLayout();
            this.panelSetter.ResumeLayout(false);
            this.panelSetter.PerformLayout();
            this.tableLayoutPanelSetter.ResumeLayout(false);
            this.tableLayoutPanelSetter.PerformLayout();
            this.panelPlusMinus.ResumeLayout(false);
            this.panelPlusMinus.PerformLayout();
            this.splitContainerBase.Panel1.ResumeLayout(false);
            this.splitContainerBase.Panel1.PerformLayout();
            this.splitContainerBase.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerBase)).EndInit();
            this.splitContainerBase.ResumeLayout(false);
            this.panelToolBar.ResumeLayout(false);
            this.panelToolBar.PerformLayout();
            this.toolStripBase.ResumeLayout(false);
            this.toolStripBase.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel tableLayoutPanelBase;
        protected Panel panelSetter;
        protected Panel panelInstrument;
        private SplitContainer splitContainerBase;
        private TableLayoutPanel tableLayoutPanelSetter;
        private Panel panelPlusMinus;
        protected Panel panelTrackBar;
        protected ToolStrip toolStripBase;
        protected ToolStripSplitButton toolStripSplitButtonChannels;
        protected Panel panelToolBar;
        private TextBox txtBoxSetValue;



    }
}
