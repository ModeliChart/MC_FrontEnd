﻿using ModeliChart.Basics;
using System;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    public partial class ChannelLinker : UserControl
    {
        private LinearChannelLink _channelLink = new LinearChannelLink(null, null, 1, 0, false);

        /// <summary>
        /// Gets called if the channel link has been changed by the user.
        /// </summary>
        public event EventHandler<ChannelLinkChangedArgs> Changed;

        /// <summary>
        /// Gets called if remove has been clicked.
        /// </summary>
        public event EventHandler Removed;

        public ChannelLinker()
        {
            InitializeComponent();
        }

        public LinearChannelLink ChannelLink
        {
            get
            {
                return _channelLink;
            }
            set
            {
                if (value != null)
                {
                    // Set the _channelLink Data in the UI
                    MasterChannel = value.MasterChannel;
                    SlaveChannel = value.SlaveChannel;
                    Offset = value.Offset;
                    Factor = value.Factor;
                    ChannelLinkEnabled = value.Enabled;
                }
            }
        }

        public IChannel MasterChannel
        {
            set
            {
                _channelLink = new LinearChannelLink(value, _channelLink.SlaveChannel, _channelLink.Factor, _channelLink.Offset, _channelLink.Enabled);
                sourceEntry.Channel = value;
                if (value != null && value.ModelInstanceName != null)
                {
                    sourceEntry.ChannelName = value.ModelInstanceName + "." + value.Name;
                }
            }
        }

        public IChannel SlaveChannel
        {
            set
            {
                if (value != null && value.Settable)
                {
                    _channelLink = new LinearChannelLink(_channelLink.MasterChannel, value, _channelLink.Factor, _channelLink.Offset, _channelLink.Enabled);
                    targetEntry.Channel = value;
                    if (value != null && value.ModelInstanceName != null)
                    {
                        targetEntry.ChannelName = value.ModelInstanceName + "." + value.Name;
                    }
                }
            }
        }

        public double Offset
        {
            set
            {
                _channelLink = new LinearChannelLink(_channelLink.MasterChannel, _channelLink.SlaveChannel, _channelLink.Factor, value, _channelLink.Enabled);
                textBoxOffset.Text = value.ToString();
            }
        }

        public double Factor
        {
            get => parseNumber(textBoxScale.Text);
            set
            {
                _channelLink = new LinearChannelLink(_channelLink.MasterChannel, _channelLink.SlaveChannel, value, _channelLink.Offset, _channelLink.Enabled);
                textBoxScale.Text = value.ToString();
            }
        }

        // Enabled is already used by Control class
        public bool ChannelLinkEnabled
        {
            set
            {
                _channelLink = new LinearChannelLink(_channelLink.MasterChannel, _channelLink.SlaveChannel, _channelLink.Factor, _channelLink.Offset, value);
                if (value)
                {
                    linkButton.BackgroundImage = Properties.Resources.unlink_128;
                }
                else
                {
                    linkButton.BackgroundImage = Properties.Resources.link_128;

                }
            }
        }

        #region Drag'n'Drop

        private void availableChannelEntrySource_DragEnter(object sender, DragEventArgs e)
        {
            // Get actual Data to check if it impliments the Interface
            var obj = e.Data.GetData(e.Data.GetFormats()[0]);
            if (obj is Channel)
            {
                e.Effect = DragDropEffects.Copy; // zuweisen der möglichen Effekte
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void availableChannelEntryTarget_DragEnter(object sender, DragEventArgs e)
        {
            // Get actual Data to check if it impliments the Interface
            var obj = e.Data.GetData(e.Data.GetFormats()[0]);
            if (obj is Channel && (obj as Channel).Settable)
            {
                e.Effect = DragDropEffects.Copy; // zuweisen der möglichen Effekte
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void availableChannelEntrySource_DragDrop(object sender, DragEventArgs e)
        {
            // Get actual Data to check if it impliments the Interface
            var data = e.Data.GetData(e.Data.GetFormats()[0]);
            if (data is Channel)
            {
                if (e.Effect == DragDropEffects.Copy)
                {
                    var old = _channelLink;
                    MasterChannel = data as Channel;
                    Changed?.Invoke(this, new ChannelLinkChangedArgs { OldLink = old, NewLink = _channelLink });
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void availableChannelEntryTarget_DragDrop(object sender, DragEventArgs e)
        {
            // Get actual Data to check if it impliments the Interface
            var data = e.Data.GetData(e.Data.GetFormats()[0]);
            if (data is Channel && (data as Channel).Settable)
            {
                if (e.Effect == DragDropEffects.Copy)
                {
                    var old = _channelLink;
                    SlaveChannel = data as Channel;
                    Changed?.Invoke(this, new ChannelLinkChangedArgs { OldLink = old, NewLink = _channelLink });
                }
            }
            Cursor.Current = Cursors.Default;
        }

        #endregion


        private void btnRemove_Click(object sender, EventArgs e)
        {
            Removed?.Invoke(this, EventArgs.Empty);
        }

        private void linkButton_Click(object sender, EventArgs e)
        {
            var old = _channelLink;
            ChannelLinkEnabled = !_channelLink.Enabled;
            Changed?.Invoke(this, new ChannelLinkChangedArgs { OldLink = old, NewLink = _channelLink });
        }

        private double parseNumber(string text)
        {
            text = text.Replace(',', '.');
            if (Double.TryParse(text,
                System.Globalization.NumberStyles.AllowDecimalPoint,
                System.Globalization.CultureInfo.InvariantCulture,
                out double value))
            {
                return value;
            }
            else
            {
                return value;
            }
        }

        private void textBoxOffset_KeyDown(object sender, KeyEventArgs e)
        {
            var old = _channelLink;
            if (e.KeyCode == Keys.Enter)
            {
                Offset = parseNumber(textBoxOffset.Text);
            }
            Changed?.Invoke(this, new ChannelLinkChangedArgs { OldLink = old, NewLink = _channelLink });
        }

        private void textBoxScale_KeyDown(object sender, KeyEventArgs e)
        {
            var old = _channelLink;
            if (e.KeyCode == Keys.Enter)
            {
                Factor = parseNumber(textBoxScale.Text);
            }
            Changed?.Invoke(this, new ChannelLinkChangedArgs { OldLink = old, NewLink = _channelLink });
        }
    }

    // LinearChannelLink is immutable so the receiver needs to know the old one to remove it.
    public class ChannelLinkChangedArgs: EventArgs
    {
        public LinearChannelLink NewLink { get; set; }
        public LinearChannelLink OldLink { get; set; }
    }
}
