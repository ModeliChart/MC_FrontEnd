﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    public interface IDraggable 
    {
        void DragSource_MouseDown(object sender, MouseEventArgs e);
        void DragSource_MouseUp(object sender, MouseEventArgs e);
        void DragSource_MouseLeave(object sender, EventArgs e);
        void DragSource_MouseEnter(object sender, EventArgs e);
    }

    public interface IDroppable 
    {
        void DragTarget_DragOver(object sender, DragEventArgs e);
        void DragTarget_DragDrop(object sender, DragEventArgs e);
        void DragTarget_DragEnter(object sender, DragEventArgs e);
        void DragTarget_DragLeave();
    }
}