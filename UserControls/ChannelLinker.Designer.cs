﻿namespace ModeliChart.UserControls
{
    partial class ChannelLinker
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRemove = new System.Windows.Forms.Button();
            this.panelScale = new System.Windows.Forms.Panel();
            this.labelScale = new System.Windows.Forms.Label();
            this.textBoxScale = new System.Windows.Forms.TextBox();
            this.textBoxOffset = new System.Windows.Forms.TextBox();
            this.labelOffset = new System.Windows.Forms.Label();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.linkButton = new System.Windows.Forms.Button();
            this.targetEntry = new ModeliChart.UserControls.AvailableChannelEntry();
            this.sourceEntry = new ModeliChart.UserControls.AvailableChannelEntry();
            this.panelScale.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRemove
            // 
            this.btnRemove.BackgroundImage = global::ModeliChart.UserControls.Properties.Resources.remove_128;
            this.btnRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRemove.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnRemove.Location = new System.Drawing.Point(614, 3);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(52, 51);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // panelScale
            // 
            this.panelScale.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelScale.Controls.Add(this.labelScale);
            this.panelScale.Controls.Add(this.textBoxScale);
            this.panelScale.Controls.Add(this.textBoxOffset);
            this.panelScale.Controls.Add(this.labelOffset);
            this.panelScale.Location = new System.Drawing.Point(446, 3);
            this.panelScale.Name = "panelScale";
            this.panelScale.Size = new System.Drawing.Size(162, 51);
            this.panelScale.TabIndex = 5;
            // 
            // labelScale
            // 
            this.labelScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelScale.Location = new System.Drawing.Point(3, 31);
            this.labelScale.Name = "labelScale";
            this.labelScale.Size = new System.Drawing.Size(37, 13);
            this.labelScale.TabIndex = 3;
            this.labelScale.Text = "Scale:";
            // 
            // textBoxScale
            // 
            this.textBoxScale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxScale.Location = new System.Drawing.Point(44, 28);
            this.textBoxScale.Name = "textBoxScale";
            this.textBoxScale.Size = new System.Drawing.Size(115, 20);
            this.textBoxScale.TabIndex = 2;
            this.textBoxScale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxScale_KeyDown);
            // 
            // textBoxOffset
            // 
            this.textBoxOffset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOffset.Location = new System.Drawing.Point(44, 3);
            this.textBoxOffset.Name = "textBoxOffset";
            this.textBoxOffset.Size = new System.Drawing.Size(115, 20);
            this.textBoxOffset.TabIndex = 1;
            this.textBoxOffset.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxOffset_KeyDown);
            // 
            // labelOffset
            // 
            this.labelOffset.Location = new System.Drawing.Point(3, 6);
            this.labelOffset.Name = "labelOffset";
            this.labelOffset.Size = new System.Drawing.Size(38, 13);
            this.labelOffset.TabIndex = 0;
            this.labelOffset.Text = "Offset:";
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 5;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.btnRemove, 4, 0);
            this.tableLayoutPanel.Controls.Add(this.linkButton, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.panelScale, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.targetEntry, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.sourceEntry, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(669, 57);
            this.tableLayoutPanel.TabIndex = 1;
            // 
            // linkButton
            // 
            this.linkButton.BackgroundImage = global::ModeliChart.UserControls.Properties.Resources.link_128;
            this.linkButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.linkButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.linkButton.Location = new System.Drawing.Point(184, 3);
            this.linkButton.Name = "linkButton";
            this.linkButton.Size = new System.Drawing.Size(75, 51);
            this.linkButton.TabIndex = 5;
            this.linkButton.UseVisualStyleBackColor = true;
            this.linkButton.Click += new System.EventHandler(this.linkButton_Click);
            // 
            // targetEntry
            // 
            this.targetEntry.AllowDrop = true;
            this.targetEntry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.targetEntry.Channel = null;
            this.targetEntry.ChannelDescription = "Needs to be settable";
            this.targetEntry.ChannelName = "Target Channel";
            this.targetEntry.DisplayChannelType = ModeliChart.Basics.ChannelType.Real;
            this.targetEntry.Dock = System.Windows.Forms.DockStyle.Left;
            this.targetEntry.ForeColor = System.Drawing.Color.Black;
            this.targetEntry.Location = new System.Drawing.Point(265, 3);
            this.targetEntry.MinimumSize = new System.Drawing.Size(2, 46);
            this.targetEntry.Name = "targetEntry";
            this.targetEntry.Settable = false;
            this.targetEntry.Size = new System.Drawing.Size(175, 51);
            this.targetEntry.TabIndex = 1;
            this.targetEntry.DragDrop += new System.Windows.Forms.DragEventHandler(this.availableChannelEntryTarget_DragDrop);
            this.targetEntry.DragEnter += new System.Windows.Forms.DragEventHandler(this.availableChannelEntryTarget_DragEnter);
            // 
            // sourceEntry
            // 
            this.sourceEntry.AllowDrop = true;
            this.sourceEntry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sourceEntry.Channel = null;
            this.sourceEntry.ChannelDescription = "Will be send to the target";
            this.sourceEntry.ChannelName = "Source Channel";
            this.sourceEntry.DisplayChannelType = ModeliChart.Basics.ChannelType.Real;
            this.sourceEntry.Dock = System.Windows.Forms.DockStyle.Left;
            this.sourceEntry.ForeColor = System.Drawing.Color.Black;
            this.sourceEntry.Location = new System.Drawing.Point(3, 3);
            this.sourceEntry.MinimumSize = new System.Drawing.Size(2, 46);
            this.sourceEntry.Name = "sourceEntry";
            this.sourceEntry.Settable = false;
            this.sourceEntry.Size = new System.Drawing.Size(175, 51);
            this.sourceEntry.TabIndex = 0;
            this.sourceEntry.DragDrop += new System.Windows.Forms.DragEventHandler(this.availableChannelEntrySource_DragDrop);
            this.sourceEntry.DragEnter += new System.Windows.Forms.DragEventHandler(this.availableChannelEntrySource_DragEnter);
            // 
            // ChannelLinker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "ChannelLinker";
            this.Size = new System.Drawing.Size(669, 57);
            this.panelScale.ResumeLayout(false);
            this.panelScale.PerformLayout();
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private AvailableChannelEntry sourceEntry;
        private AvailableChannelEntry targetEntry;
        private System.Windows.Forms.Panel panelScale;
        private System.Windows.Forms.Label labelScale;
        private System.Windows.Forms.TextBox textBoxScale;
        private System.Windows.Forms.TextBox textBoxOffset;
        private System.Windows.Forms.Label labelOffset;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button linkButton;
    }
}
