﻿using ModeliChart.Basics;
using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    /// <summary>
    /// This control can render the simulation data.
    /// Warning it can not handle mutliplethreads as refreshing is executed asynchronously.
    /// So use a single thread to update the data and refresh the plot.
    /// It is expected that the instrument is refreshed frequently by the user.
    /// </summary>
    public partial class UniversalOxyInstrument : OxyPlotLineInstrumentBase
    {
        // Drag and drop overlay
        private readonly OxyOverlay overlay = new OxyOverlay();
        // Display as sweep mode 
        private bool sweepMode;
        // Start as OneChannelInstrument
        private InstrumentType instrumentType = InstrumentType.OneChannelInstrument;
        // Thread safe data conversion
        private readonly OxyPlotDataConverter dataConverter;
        // Channels for different modes
        private readonly List<IChannel> multiChannels = new List<IChannel>();
        private readonly List<(IChannel ChannelX, IChannel ChannelY)> twoChannels = new List<(IChannel ChannelX, IChannel ChannelY)>();
        // Clear the datapoints of the twochannel instrument before this timepoint
        double refreshTime = 0;

        public UniversalOxyInstrument(ISimulation simulation, IDataRepository dataRepository)
            : base(simulation, dataRepository)
        {
            InitializeComponent();
            dataConverter = new OxyPlotDataConverter(dataRepository);
            overlay.Hide();
            overlay.ChannelDropped += Overlay_ChannelDropped;
            // Default Mode: OneChannelInstrument
            SetUpOneChannelInstrument(channel);
            // Merge Toolstrips
            ToolStripManager.Merge(toolStripUniversal, toolStripBase);
            toolStripUniversal.Visible = false;
        }

        public InstrumentType InstrumentType
        {
            get { return instrumentType; }
            set
            {
                switch (value)
                {
                    case InstrumentType.OneChannelInstrument:
                        switch (InstrumentType)
                        {
                            case InstrumentType.OneChannelInstrument:
                                return;
                            case InstrumentType.MultiChannelInstrument:
                                SetUpOneChannelInstrument(multiChannels.FirstOrDefault());
                                break;
                            case InstrumentType.TwoChannelInstrument:
                                SetUpOneChannelInstrument(twoChannels.FirstOrDefault().ChannelX);
                                break;
                        }
                        break;
                    case InstrumentType.MultiChannelInstrument:
                        switch (InstrumentType)
                        {
                            case InstrumentType.MultiChannelInstrument:
                                return;
                            // Do not move out setUpMultiChannelInstrument(channels) from the case!!!
                            case InstrumentType.OneChannelInstrument:
                                if (channel != null)
                                {
                                    SetUpMultiChannelInstrument(new IChannel[] { channel });
                                }
                                else
                                {
                                    SetUpMultiChannelInstrument(Enumerable.Empty<IChannel>());
                                }
                                break;
                            case InstrumentType.TwoChannelInstrument:
                                // Zip alternating
                                SetUpMultiChannelInstrument(twoChannels
                                    .Select(p => new IChannel[] { p.ChannelX, p.ChannelY })
                                    .SelectMany(p => p));
                                break;
                        }
                        break;
                    case InstrumentType.TwoChannelInstrument:
                        switch (InstrumentType)
                        {
                            case InstrumentType.TwoChannelInstrument:
                                return;
                            case InstrumentType.OneChannelInstrument:
                                if (channel != null)
                                {
                                    SetUpTwoChannelInstrument(new IChannel[] { channel });
                                }
                                else
                                {
                                    SetUpTwoChannelInstrument(Enumerable.Empty<IChannel>());
                                }
                                break;
                            case InstrumentType.MultiChannelInstrument:
                                SetUpTwoChannelInstrument(multiChannels);
                                break;
                        }
                        break;
                }
                instrumentType = value;
            }
        }

        public bool SweepMode
        {
            get { return sweepMode; }
            set
            {
                sweepMode = value;
                if (sweepMode)
                {
                    toolStripButtonSweep.Image = Properties.Resources.time_mode;
                }
                else
                {
                    toolStripButtonSweep.Image = Properties.Resources.sweep_mode;
                }
            }
        }

        /// <summary>
        /// Returns a List of Channels regarding the current mode
        /// </summary>
        public override IEnumerable<IChannel> Channels
        {
            get
            {
                switch (InstrumentType)
                {
                    case InstrumentType.OneChannelInstrument:
                        return new IChannel[] { channel };
                    case InstrumentType.TwoChannelInstrument:
                        return twoChannels
                            .Select(p => new IChannel[] { p.ChannelX, p.ChannelY })
                            .SelectMany(p => p);
                    case InstrumentType.MultiChannelInstrument:
                        return multiChannels;
                    default:
                        return Enumerable.Empty<IChannel>();
                }
            }
        }

        /// <summary>
        /// Factory method for new LineSerieses
        /// </summary>
        /// <returns></returns>
        private LineSeries CreateLineSeries(string title)
        {
            var lineSeries = new LineSeries
            {
                StrokeThickness = 2,
                MarkerSize = 1,
                Title = title
            };
            // Prevent garbage collection for a minute
            lineSeries.Points.Capacity = 6000;
            return lineSeries;
        }

        #region UI Events

        // Clear the Samples and Restart collecting
        private void ToolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            // Set the refresh time point
            refreshTime = xAxis.Maximum;
        }

        // Swap the axis in the TwoChannelInstrument
        private void ToolStripButtonSwapAxis_Click(object sender, EventArgs e)
        {
            switch (InstrumentType)
            {
                case InstrumentType.TwoChannelInstrument:
                    SwapAxis();
                    break;
            }
        }

        // Change sweepMode
        private void ToolStripButtonSweep_Click(object sender, EventArgs e)
        {
            SweepMode = !SweepMode;
        }

        #endregion

        #region InstrumentTypeMenu

        private void OneChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstrumentType = InstrumentType.OneChannelInstrument;
        }

        private void MultiChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstrumentType = InstrumentType.MultiChannelInstrument;
        }

        private void TwoChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstrumentType = InstrumentType.TwoChannelInstrument;
        }

        #endregion

        #region Drag'n'Drop

        void Overlay_ChannelDropped(object sender, ChannelDroppedArgs e)
        {
            InstrumentType = e.InstrumentType;
            AddChannel(e.Channel);
        }

        public void DragTarget_DragLeave(object sender, EventArgs e)
        {
            // Also leaves when the overlay is shown which would lead to infinite show and hide swtich
            if (!overlay.ClientRectangle.Contains(PointToClient(Control.MousePosition)) && !this.ClientRectangle.Contains(PointToClient(Control.MousePosition)))
            {
                overlay.Hide();
            }
        }

        public void DragTarget_DragEnter(object sender, DragEventArgs e)
        {
            // Only consume if the overlay is not visible
            if (e.Data.GetData(e.Data.GetFormats()[0]) is IChannel channel && overlay.Visible != true)
            {
                e.Effect = DragDropEffects.Copy;
                overlay.ShowOverlay(this, InstrumentType);
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        public void DragTarget_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetData(e.Data.GetFormats()[0]) is IChannel channel)
            {
                AddChannel(channel);
            }
            overlay.Hide();
        }

        private void DragTarget_DragOver(object sender, DragEventArgs e)
        {
        }

        #endregion

        #region Data update

        /// <summary>
        /// Updates the underlying data to the given timepoint.
        /// Does not Refresh the view, call InvalidateInstrument!
        /// </summary>
        public void UpdateDataToTime(double currentTime)
        {
            switch (InstrumentType)
            {
                case InstrumentType.OneChannelInstrument:
                    UpdateDataOneChannel(currentTime);
                    break;
                case InstrumentType.MultiChannelInstrument:
                    UpdateDataMultiChannel(currentTime);
                    break;
                case InstrumentType.TwoChannelInstrument:
                    UpdateDataTwoChannel(currentTime);
                    break;
            }
        }

        private double SweepTime(double currentTime) => Math.Truncate(currentTime / DisplayedInterval) * DisplayedInterval;

        private void UpdateDataOneChannel(double currentTime)
        {
            // Update axes to currentTime
            if (SweepMode)
            {
                if (channel != null)
                {
                    dataConverter.PollDataMultiChannelSweep(
                        new IChannel[] { channel }, currentTime - DisplayedInterval, currentTime, SweepTime(currentTime));
                }
            }
            else
            {
                if (channel != null)
                {
                    dataConverter.PollDataMultiChannel(
                        new IChannel[] { channel }, currentTime - DisplayedInterval, currentTime);
                }
            }
        }

        private void UpdateDataTwoChannel(double currentTime)
        {
            dataConverter.PollDataTwoChannel(
                twoChannels.Where(t => t.ChannelX != null && t.ChannelY != null),
                currentTime - DisplayedInterval, currentTime);
        }

        private void UpdateDataMultiChannel(double currentTime)
        {
            if (SweepMode)
            {
                dataConverter.PollDataMultiChannelSweep(multiChannels, currentTime - DisplayedInterval, currentTime, SweepTime(currentTime));
            }
            else
            {
                dataConverter.PollDataMultiChannel(multiChannels, currentTime - DisplayedInterval, currentTime);
            }
        }


        #endregion

        #region General Methods: IInstrument implementations, setLegendTitles

        public override void InvalidateInstrument(double currentTime)
        {
            switch (InstrumentType)
            {
                // Type dependent methods
                case InstrumentType.OneChannelInstrument:
                    // Get latest value that has been set.
                    if (channel?.Settable ?? false)
                    {
                        CheckTrackBarSetter();
                    }
                    if (SweepMode)
                    {
                        SetAxisLimits(SweepTime(currentTime) + DisplayedInterval);
                    }
                    else
                    {
                        SetAxisLimits(currentTime);
                    }
                    break;
                case InstrumentType.MultiChannelInstrument:
                    if (SweepMode)
                    {
                        SetAxisLimits(SweepTime(currentTime) + DisplayedInterval);
                    }
                    else
                    {
                        SetAxisLimits(currentTime);
                    }
                    break;
                case InstrumentType.TwoChannelInstrument:
                    SetAxisLimits(double.NaN);
                    break;

            }
            // Update seres data
            var data = dataConverter.GetData();
            plotModel.Annotations.Clear();
            var zipped = data.Zip(plotModel.Series, (d, s) => (Data: d, Series: s));
            foreach (var element in zipped)
            {
                if (element.Series is LineSeries lineSeries)
                {
                    lineSeries.Points.Clear();
                    lineSeries.Points.AddRange(element.Data.Values);
                }
                plotModel.Annotations.Add(CreateAnnotationDot(element.Data.Annotation));
            }
            // Update everthing
            plotView.InvalidatePlot(true);
        }

        private void SetAxisLimits(double minimum)
        {
            xAxis.Minimum = minimum - DisplayedInterval;
            xAxis.Maximum = minimum;
        }

        // Implement IInstrument and override OxyPlotBase
        public void AddChannel(IChannel channel)
        {
            if (channel != null)
            {
                switch (InstrumentType)
                {
                    case InstrumentType.OneChannelInstrument:
                        AddChannelOneChannelInstrument(channel);
                        break;
                    case InstrumentType.MultiChannelInstrument:
                        AddChannelMutliChannelInstrument(channel);
                        break;
                    case InstrumentType.TwoChannelInstrument:
                        AddChannelTwoChannelInstrument(channel);
                        break;
                }
            }
        }

        /// <summary>
        /// Creates a title with Channel.Name.
        /// This method is save to use with channel = null, returns "null".
        /// </summary>
        /// <param name="channel"></param>
        private string CreateTitle(IChannel channel)
        {
            if (channel != null)
            {
                string title = channel.ModelInstanceName + ":" + channel.Name;
                if (!string.IsNullOrEmpty(channel.DisplayedUnit))
                {
                    title += " [" + channel.DisplayedUnit + "]";
                }
                return title;
            }
            return "null";
        }

        #endregion

        #region Rendering Methods

        /// <summary>
        /// Shows the dot at the dataPoints position
        /// </summary>
        /// <param name="dataPoint"></param>
        private PointAnnotation CreateAnnotationDot(DataPoint dataPoint) => new PointAnnotation
        {
            Stroke = OxyColors.Black,
            StrokeThickness = 1,
            X = dataPoint.X,
            Y = dataPoint.Y
        };

        #endregion

        #region OneChannelInstrument

        /// <summary>
        /// Set up the Instrument as OneChannelInstrument
        /// </summary>
        private void SetUpOneChannelInstrument(IChannel channel)
        {
            AddChannelOneChannelInstrument(channel);
            toolStripButtonRefresh.Visible = false;
            toolStripButtonSwapAxis.Visible = false;
            toolStripButtonSweep.Visible = true;
            themeColorPicker.Visible = true;
            RefreshSetterControls();
            RefreshToolbar();
            instrumentType = InstrumentType.OneChannelInstrument;
        }


        private void AddChannelOneChannelInstrument(IChannel channelToBeAdded)
        {
            if (channelToBeAdded != null)
            {
                // Set current Channel
                channel = channelToBeAdded;
                plotModel.Series.Clear();
                plotModel.Series.Add(CreateLineSeries(CreateTitle(channel)));
                // Refresh the Setter Controls and the EnumBar
                RefreshSetterControls();
                RefreshToolbar();
            }
        }

        #endregion

        #region MultiChannelInstrument

        private void SetUpMultiChannelInstrument(IEnumerable<IChannel> channels)
        {
            channel = null;
            multiChannels.Clear();
            plotModel.Series.Clear();
            toolStripSplitButtonChannels.DropDownItems.Clear();
            foreach (IChannel currentChannel in channels)
            {
                AddChannelMutliChannelInstrument(currentChannel);
            }
            toolStripButtonRefresh.Visible = false;
            toolStripButtonSweep.Visible = true;
            toolStripSplitButtonChannels.Visible = true;
            toolStripButtonSwapAxis.Visible = false;
            themeColorPicker.Visible = false;
            RefreshSetterControls();
        }

        private void AddChannelMutliChannelInstrument(IChannel channel)
        {
            if (channel != null)
            {
                var lineSeries = CreateLineSeries(CreateTitle(channel));
                multiChannels.Add(channel);
                plotModel.Series.Add(lineSeries);
                CreateMultiItem(CreateTitle(channel));
            }
        }

        #region MenuItems

        /// <summary>
        /// Creates a ToolStripItem with the remove submenu
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        private void CreateMultiItem(String title)
        {
            // Create the item itself
            ToolStripMenuItem item = new ToolStripMenuItem(title);
            // Create & add the Remove Submenu
            ToolStripMenuItem remove = new ToolStripMenuItem("Remove");
            remove.Click += Remove_Multi_Click;
            item.DropDownItems.Add(remove);
            ToolStripMenuItem color = new ToolStripMenuItem("Color");
            color.Click += Color_Multi_Click;
            item.DropDownItems.Add(color);
            // Return the item
            toolStripSplitButtonChannels.DropDownItems.Add(item);
        }
        /// <summary>
        /// Sets the PlotColor of the Channel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Color_Multi_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem senderItem = sender as ToolStripMenuItem;
            ToolStripItem channelItem = senderItem.OwnerItem;
            // Find the current Channel
            int index = toolStripSplitButtonChannels.DropDownItems.IndexOf(channelItem);
            if (index > -1)
            {
                ThemeColorPickerWindow picker = new ThemeColorPickerWindow(Cursor.Position, FormBorderStyle.FixedSingle, ThemeColorPickerWindow.Action.CloseWindow, ThemeColorPickerWindow.Action.DoNothing);
                picker.ShowDialog();
                System.Drawing.Color color = picker.Color;
                (plotModel.Series[index] as LineSeries).Color = OxyColor.FromArgb(color.A, color.R, color.G, color.B);
            }

        }

        /// <summary>
        /// Handles the Click event of the DropDown Items to remove the Channel and the Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Remove_Multi_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem senderItem = sender as ToolStripMenuItem;
            ToolStripItem channelItem = senderItem.OwnerItem;
            // Find the current Channel
            int index = toolStripSplitButtonChannels.DropDownItems.IndexOf(channelItem);
            if (index > -1)
            {
                plotModel.Series.RemoveAt(index);
                multiChannels.RemoveAt(index);
                // Reset the toolstripbutton
                toolStripSplitButtonChannels.DropDownItems.RemoveAt(index);
                toolStripSplitButtonChannels.Text = "Channels";
            }
        }

        #endregion

        #endregion

        #region TwoChannelInstrument

        private void SetUpTwoChannelInstrument(IEnumerable<IChannel> channels)
        {
            channel = null;
            twoChannels.Clear();
            toolStripSplitButtonChannels.DropDownItems.Clear();
            plotModel.Series.Clear();
            foreach (IChannel channel in channels)
            {
                AddChannelTwoChannelInstrument(channel);
            }
            toolStripButtonRefresh.Visible = true;
            toolStripButtonSwapAxis.Visible = true;
            toolStripSplitButtonChannels.Visible = true;
            toolStripButtonSweep.Visible = false;
            themeColorPicker.Visible = false;
            // Set the timespan for the plot: autoscale
            xAxis.Maximum = double.NaN;
            xAxis.Minimum = double.NaN;
            RefreshSetterControls();
            refreshTime = 0;
        }

        private string CreateTwoChannelTitle(IChannel channelX, IChannel channelY) =>
            CreateTitle(channelX) + " / " + CreateTitle(channelY);

        private void AddNewTwoChannelItem(IChannel channelX)
        {
            var lineSeries = CreateLineSeries(CreateTwoChannelTitle(channelX, null));
            twoChannels.Add((channelX, null));
            plotModel.Series.Add(lineSeries);
            CreatePairItem();
        }

        private void AddChannelTwoChannelInstrument(IChannel channel)
        {
            if (twoChannels.Any())
            {
                // Needs a partner?
                if (twoChannels.Last().ChannelY == null)
                {
                    var (channelX, _) = twoChannels.Last();
                    twoChannels.RemoveAt(twoChannels.Count - 1);
                    twoChannels.Add((channelX, channel));
                    plotModel.Series.Last().Title = CreateTwoChannelTitle(channelX, channel);
                }
                else
                {
                    // Already got one!
                    AddNewTwoChannelItem(channel);
                }
            }
            else
            {
                // First channel in the collection
                AddNewTwoChannelItem(channel);
            }
            CreateTwoMenuItem(CreateTitle(channel));
        }

        private void SwapAxis()
        {
            // Create copy via ToArray(), linq query is executed delayed
            var swapped = from tuple in twoChannels.ToArray()
                          select (tuple.ChannelY, tuple.ChannelX);
            var zipped = swapped.Zip(plotModel.Series, (c, s) => (c, s));
            foreach (var (channels, series) in zipped)
            {
                series.Title = CreateTwoChannelTitle(channels.ChannelX, channels.ChannelY);
            }
            twoChannels.Clear();
            twoChannels.AddRange(swapped);
        }

        #region MenuItems

        /// <summary>
        /// Creates a ToolStripItem with the remove submenu
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        private void CreateTwoMenuItem(String title)
        {
            ToolStripMenuItem item = new ToolStripMenuItem(title);
            // Add the item to the lates pairItem
            int number = toolStripSplitButtonChannels.DropDownItems.Count - 1;
            ToolStripMenuItem lastPair = toolStripSplitButtonChannels.DropDownItems[number] as ToolStripMenuItem;
            lastPair.DropDownItems.Insert(0, item);
        }
        private void CreatePairItem()
        {
            int number = toolStripSplitButtonChannels.DropDownItems.Count + 1;
            ToolStripMenuItem item = new ToolStripMenuItem("Pair " + number);
            // Create & add the Remove Submenu
            ToolStripMenuItem remove = new ToolStripMenuItem("Remove");
            remove.Click += Remove_Two_Click;
            item.DropDownItems.Add(remove);
            ToolStripMenuItem color = new ToolStripMenuItem("Color");
            color.Click += Color_Two_Click;
            item.DropDownItems.Add(color);
            // Add the item
            toolStripSplitButtonChannels.DropDownItems.Add(item);
        }

        /// <summary>
        /// Sets the PlotColor of the Channel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Color_Two_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem senderItem = sender as ToolStripMenuItem;
            ToolStripItem ownerItem = senderItem.OwnerItem;
            // Find the current Pair Index
            int index = toolStripSplitButtonChannels.DropDownItems.IndexOf(ownerItem);

            if (index > -1)
            {
                ThemeColorPickerWindow picker = new ThemeColorPickerWindow(Cursor.Position, FormBorderStyle.FixedSingle, ThemeColorPickerWindow.Action.CloseWindow, ThemeColorPickerWindow.Action.DoNothing);
                picker.ShowDialog();
                System.Drawing.Color color = picker.Color;
                (plotModel.Series[index] as LineSeries).Color = OxyColor.FromArgb(color.A, color.R, color.G, color.B);
            }

        }

        /// <summary>
        /// Handles the Click event of the DropDown Items to remove the Channel and the Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Remove_Two_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem senderItem = sender as ToolStripMenuItem;
            ToolStripItem ownerItem = senderItem.OwnerItem;
            // Find the current Pair
            int index = toolStripSplitButtonChannels.DropDownItems.IndexOf(ownerItem);
            // Check if one of the Child items was clicked
            if (index > -1)
            {
                // Remove the Channel
                twoChannels.RemoveAt(index);
                plotModel.Series.RemoveAt(index);
                channel = null;
                // Reset the toolstripbutton
                toolStripSplitButtonChannels.DropDownItems.RemoveAt(index);
                toolStripSplitButtonChannels.Text = "Channels";
            }
        }

        #endregion

        #endregion
    }
}