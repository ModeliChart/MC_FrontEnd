﻿using ModeliChart.Basics;
using OxyPlot;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace ModeliChart.UserControls
{
    /// <summary>
    /// The access is threadsafe.
    /// </summary>
    internal class OxyPlotDataConverter
    {
        private readonly IDataRepository dataRepository;
        private IEnumerable<(DataPoint[] values, DataPoint annotation)> data;
        private readonly object dataLock = new object();

        public OxyPlotDataConverter(IDataRepository dataRepository)
        {
            this.dataRepository = dataRepository;
        }

        /// <summary>
        /// Executes the conversion of the query.
        /// </summary>
        private DataPoint[] Convert(IEnumerable<(double X, double Y)> values) =>
            values
            .Select(p => new DataPoint(p.X, p.Y))
            .ToArray();

        /// <summary>
        /// Qeries the data from the repository and cuts it to the given interval.
        /// </summary>
        private IEnumerable<(double Time, double Value)> PollData(IChannel channel, double minTime, double maxTime) =>
            from point in dataRepository.GetValues(channel)
            where point.Time >= minTime && point.Time <= maxTime
            select point;

        private ((double Time, double Value) SweepPoint, IEnumerable<(double Time, double Value)> Values) PollDataSweep(
            IChannel channel, double minTime, double maxTime, double sweepTime)
        {
            var channelData = PollData(channel, minTime, maxTime);
            var beforeSweepTime = from point in channelData
                                  where point.Time < sweepTime
                                  select (point.Time + maxTime - minTime, point.Value);
            var afterSweepTime = from point in channelData
                                 where point.Time >= sweepTime
                                 select point;
            return (afterSweepTime.LastOrDefault(), afterSweepTime.Concat(beforeSweepTime));
        }

        /// <summary>
        /// Queries the data for two channels and uses the value of channelX as x-Coordinate
        /// and the value of channelY as y-Coordinate of the points.
        /// </summary>
        private ((double X, double Y) Annotation, IEnumerable<(double X, double Y)> Values) PollDataTwoChannel(
            IChannel channelX, IChannel channelY, double minTime, double maxTime)
        {
            var values = from pointX in PollData(channelX, minTime, maxTime)
                         join pointY in PollData(channelY, minTime, maxTime) on pointX.Time equals pointY.Time
                         select (pointX.Value, pointY.Value);
            return (values.LastOrDefault(), values);
        }

        /// <summary>
        /// Convert the data of multiple channels.
        /// </summary>
        /// <param name="channels"></param>
        /// <param name="minTime"></param>
        /// <param name="maxTime"></param>
        public void PollDataMultiChannel(IEnumerable<IChannel> channels, double minTime, double maxTime)
        {
            lock (dataLock)
            {
                data = from channel in channels
                       select (Convert(PollData(channel, minTime, maxTime)), DataPoint.Undefined);
            }
        }

        public void PollDataMultiChannelSweep(IEnumerable<IChannel> channels, double minTime, double maxTime, double sweepTime)
        {
            lock (dataLock)
            {
                data = from channel in channels
                       let polled = PollDataSweep(channel, minTime, maxTime, sweepTime)
                       select (Convert(polled.Values), new DataPoint(polled.SweepPoint.Time, polled.SweepPoint.Value));
            }
        }

        /// <summary>
        /// Convert the data of multiple channel pairs.
        /// Uuses the value of channelX as x-Coordinate
        /// and the value of channelY as y-Coordinate of the points.
        /// </summary>
        /// <param name="tuples"></param>
        /// <param name="minTime"></param>
        /// <param name="maxTime"></param>
        public void PollDataTwoChannel(IEnumerable<(IChannel channelX, IChannel channelY)> tuples, double minTime, double maxTime)
        {
            lock (dataLock)
            {
                data = from tuple in tuples
                       let polled = PollDataTwoChannel(tuple.channelX, tuple.channelY, minTime, maxTime)
                       select (Convert(polled.Values), new DataPoint(polled.Annotation.X, polled.Annotation.Y));
            }
        }

        /// <summary>
        /// This call blocks until new converted data is available.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<(DataPoint[] Values, DataPoint Annotation)> GetData()
        {
            // Execute the query by calling .ToArray()
            // Otherwise it might be evaluated somewhere else while it changes.
            lock (dataLock)
            {
                if (data == null)
                {
                    return Enumerable.Empty<(DataPoint[] Values, DataPoint Annotation)>();
                }
                else
                {
                    return data.ToArray();
                }
            }
        }
    }
}
