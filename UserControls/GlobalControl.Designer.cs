﻿namespace ModeliChart.UserControls
{
    partial class GlobalControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GlobalControl));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.designToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.weißToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxShowChannels = new System.Windows.Forms.CheckBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(542, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.designToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton1.Text = "toolStripDropDownButton1";
            // 
            // designToolStripMenuItem
            // 
            this.designToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grauToolStripMenuItem,
            this.weißToolStripMenuItem});
            this.designToolStripMenuItem.Name = "designToolStripMenuItem";
            this.designToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.designToolStripMenuItem.Text = "Design";
            // 
            // grauToolStripMenuItem
            // 
            this.grauToolStripMenuItem.Name = "grauToolStripMenuItem";
            this.grauToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.grauToolStripMenuItem.Text = "grau";
            // 
            // weißToolStripMenuItem
            // 
            this.weißToolStripMenuItem.Name = "weißToolStripMenuItem";
            this.weißToolStripMenuItem.Size = new System.Drawing.Size(99, 22);
            this.weißToolStripMenuItem.Text = "weiß";
            // 
            // checkBoxShowChannels
            // 
            this.checkBoxShowChannels.AutoSize = true;
            this.checkBoxShowChannels.Location = new System.Drawing.Point(396, 3);
            this.checkBoxShowChannels.Name = "checkBoxShowChannels";
            this.checkBoxShowChannels.Size = new System.Drawing.Size(143, 17);
            this.checkBoxShowChannels.TabIndex = 1;
            this.checkBoxShowChannels.Text = "Zeige verfügbare Kanäle";
            this.checkBoxShowChannels.UseVisualStyleBackColor = true;
            this.checkBoxShowChannels.CheckedChanged += new System.EventHandler(this.checkBoxShowChannels_CheckedChanged);
            // 
            // GlobalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkBoxShowChannels);
            this.Controls.Add(this.toolStrip1);
            this.Name = "GlobalControl";
            this.Size = new System.Drawing.Size(542, 306);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem designToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem weißToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxShowChannels;
    }
}
