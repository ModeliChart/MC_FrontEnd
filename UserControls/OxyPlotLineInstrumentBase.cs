﻿using ModeliChart.Basics;
using ModeliChart.Log;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Linq;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    /// <summary>
    /// Pay attention to the virtual voids that have to be overridden
    /// </summary>
    public partial class OxyPlotLineInstrumentBase : InstrumentControlBase  // Not IInstrument, implement by final control
    {
        protected PlotModel plotModel;
        protected LinearAxis xAxis;
        protected LinearAxis yAxis;

        public OxyPlotLineInstrumentBase(ISimulation simulation, IDataRepository dataRepository)
            : base(simulation, dataRepository)
        {
            InitializeComponent();

            // Default value
            DisplayedInterval = 10;

            // Merge the toolstrips
            ToolStripManager.Merge(toolStripOxy, toolStripBase);
            toolStripOxy.Visible = false;

            // Add the plot and the trackBar, because no visual inheritance is possible
            panelInstrument.Controls.Add(plotView);
            panelTrackBar.Controls.Add(trackBarSetter);
            // PlotModel
            plotModel = new PlotModel
            {
                IsLegendVisible = true,
                LegendPosition = LegendPosition.TopLeft
            };
            xAxis = new LinearAxis
            {
                Position = AxisPosition.Bottom,
                IsZoomEnabled = false,
                IsPanEnabled = false
            };
            plotModel.Axes.Add(xAxis);
            yAxis = new LinearAxis
            {
                Position = AxisPosition.Right
            };
            plotModel.Axes.Add(yAxis);
            // Will invalidate the plot
            plotView.Model = plotModel;
        }

        #region ToolTip Actions
        private void MenuItem10s_Click(object sender, System.EventArgs e)
        {
            DisplayedInterval = 10;
        }
        private void MenuItem20s_Click(object sender, System.EventArgs e)
        {
            DisplayedInterval = 20;
        }
        private void MenuItem30s_Click(object sender, System.EventArgs e)
        {
            DisplayedInterval = 30;
        }
        private void MenuItemCustomTime_Click(object sender, EventArgs e)
        {
            if (double.TryParse(
                InputMessageBox.Show("Please enter the time interval in seconds, you would like to display. (Format: x,xxx)",
                "Time interval"),
                out var interval))
            {
                DisplayedInterval = interval;
            }
        }
        /// <summary>
        /// Apply the Selected Color to the Graph
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ColorSelected(object sender, ColorSelectedArg e)
        {
            // TODO move to the universal oxy instrument
            plotModel.InvalidatePlot(true);
        }
        #endregion

        #region AutoScale
        /// <summary>
        /// Resets the axes which will lead max and min value to be fitted to the graph
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolStripButtonAutoScale_Click(object sender, System.EventArgs e)
        {
            plotModel.ResetAllAxes();
            plotModel.InvalidatePlot(true);
        }
        /// <summary>
        /// Resets the axes which will lead max and min value to be fitted to the graph 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Plot_DoubleClick(object sender, EventArgs e)
        {
            plotModel.ResetAllAxes();
            plotModel.InvalidatePlot(true);
        }
        #endregion

        #region Zoom
        private void OxyPlotLineInstrumentBase_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                xAxis.IsPanEnabled = true;
                xAxis.IsZoomEnabled = true;
                yAxis.IsPanEnabled = false;
                yAxis.IsZoomEnabled = false;
            }
        }

        private void OxyPlotLineInstrumentBase_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.ControlKey)
            {
                xAxis.IsPanEnabled = false;
                xAxis.IsZoomEnabled = false;
                yAxis.IsPanEnabled = true;
                yAxis.IsZoomEnabled = true;
            }
        }

        #endregion

        #region TrackBarSetter Scale & Set
        // TrackBar can only handle int Steps
        // So the actual Position & Size are factored to be able to set it with the TrackBar in every Size
        // TrackBar Max an Min can be chosen freely in the Designer

        private Boolean userInput = false;  // To differ if the Value is set by following the Values or to follow the currentValue

        /// <summary>
        /// Set the userInput in the Channel
        /// </summary>
        private void TrackBarSetter_ValueChanged(object sender, EventArgs e)
        {
            // The User sets the value, so set the value in the channel
            if (userInput)
            {
                // Remember Min & Max
                double min = plotModel.DefaultYAxis.ActualMinimum;
                double max = plotModel.DefaultYAxis.ActualMaximum;
                // Apply min and max so there is no funny rescaling
                plotModel.DefaultYAxis.Minimum = min;
                plotModel.DefaultYAxis.Maximum = max;
                if (channel.Settable)
                {
                    // Calculate the scale factor
                    double factor = (max - min) / (trackBarSetter.Maximum - trackBarSetter.Minimum);
                    // Calculate the value which is to be set
                    double value = (trackBarSetter.Value * factor) + min;
                    // Some magnetic Action for Bool & Int Channels
                    if (channel.ChannelType == ChannelType.Boolean || channel.ChannelType == ChannelType.Integer)
                    {
                        value = Math.Round(value, 0);
                    }
                    // Set the value, but check if it is double or int channel
                    simulation.SetValue(channel, value);
                }
            }
            // else do not change any values
        }

        /// <summary>
        /// The value will continue increasing or decreasing when TrackBar is at Max or Min
        /// </summary>
        protected void CheckTrackBarSetter()
        {
            if (plotModel.DefaultYAxis != null && channel != null)
            {
                // Calculate the scale factor
                double factor = (plotModel.DefaultYAxis.ActualMaximum - plotModel.DefaultYAxis.ActualMinimum) / (trackBarSetter.Maximum - trackBarSetter.Minimum);
                // Calculate the min, value in the scale of the Plot, min of the PlotAxis
                double min = plotModel.DefaultYAxis.ActualMinimum;
                // The USER keeps increasing, Set a Value only at Min or Max, Otherwise ValueChanged will handle it!
                if (userInput)
                {
                    #region user sets the Value
                    // If this stays NaN no value is to be set
                    double value = double.NaN;
                    // If the max or min of the Trackbar is reached continue increasing or decreasing
                    if (trackBarSetter.Value == trackBarSetter.Maximum)
                    {
                        // Calculate the increased Value
                        value = (trackBarSetter.Value * factor) + min;
                        value += 1 * factor;
                        // Now Rescaling function is neccessary
                        plotModel.DefaultYAxis.Minimum = double.NaN;
                        plotModel.DefaultYAxis.Maximum = double.NaN;
                    }
                    else if (trackBarSetter.Value == trackBarSetter.Minimum)
                    {
                        // Calculate the increased Value
                        value = (trackBarSetter.Value * factor) + min;
                        value -= 1 * factor;
                        // Now Rescaling function is neccessary
                        plotModel.DefaultYAxis.Minimum = double.NaN;
                        plotModel.DefaultYAxis.Maximum = double.NaN;
                    }
                    // Set the value, but check if it is double or int channel & if it is to be set
                    if (channel.Settable && !double.IsNaN(value))
                    {
                        simulation.SetValue(channel, value);
                    }

                    #endregion
                }
                // Program lets the trackbar follow the values
                else
                {
                    # region trackBar to currentValue
                    // Get the current Value from the Channel
                    double currentValueRounded = dataRepository.GetValues(channel)
                        .LastOrDefault()
                        .Value;
                    // Make the min -> 0
                    currentValueRounded -= min;
                    // Scale the value to the trackbar
                    currentValueRounded = currentValueRounded / factor;
                    // Trackbar value is an Integer so cast the currentValue rounded to int
                    int trackBarValue = (int)currentValueRounded;
                    // Check if it is out of range
                    if (trackBarValue < 0) { trackBarValue = 0; }
                    if (trackBarValue > trackBarSetter.Maximum) { trackBarValue = trackBarSetter.Maximum; }
                    // Set the value of the trackbar
                    trackBarSetter.Value = trackBarValue;
                    #endregion
                }
            }
        }

        // Now let the Instrument rescale itself
        private void TrackBarSetter_MouseUp(object sender, MouseEventArgs e)
        {
            if (plotModel.DefaultXAxis != null)
            {
                // AutoRescale of the Instrument
                plotModel.DefaultYAxis.Minimum = double.NaN;
                plotModel.DefaultYAxis.Maximum = double.NaN;
                // Now the program handles the value of the trackbar
                userInput = false;
            }
        }
        // The user wants to handle the trackbar value
        private void TrackBarSetter_MouseDown(object sender, MouseEventArgs e)
        {
            userInput = true;
        }
        #endregion

        /// <summary>
        /// In ADDITION to the actions from the base Method: Show or Hide, this will reset the trackBar
        /// </summary>
        protected new void RefreshSetterControls()
        {
            // Call the base Method to show or hide the Controls
            base.RefreshSetterControls();
            trackBarSetter.Value = (trackBarSetter.Maximum - trackBarSetter.Minimum) / 2;
        }
    }
}
