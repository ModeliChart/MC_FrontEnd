﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ModeliChart.UserControls
{
    public class GlobalDesign : UserControl
    {

        protected static System.Drawing.Color _BackColor = System.Drawing.Color.Gray;
        protected static System.Drawing.Color _PanelBackColor = System.Drawing.Color.DimGray;
        protected static System.Drawing.Color _ForeColor = System.Drawing.Color.LightGray;

        static List<GlobalDesign> _allGeneratedUserControls = new List<GlobalDesign>();

        Dictionary<string, Design> _Designs = new Dictionary<string, Design>();

        protected List<GlobalDesign> allGeneratedUserControls
        {
            get { return _allGeneratedUserControls; }
        }
        
        protected System.Drawing.Color globalBackColor
        {
            get { return _BackColor; }
            set { _BackColor = value; }
        }
        protected System.Drawing.Color globalPanelBackColor
        {
            get { return _PanelBackColor; }
            set { _PanelBackColor = value; }
        }
        protected System.Drawing.Color globalForeColor
        {
            get { return _ForeColor; }
            set { _ForeColor = value; }
        }

        protected Dictionary<string, Design> Designs
        {
            get { return _Designs; }
        }

        protected struct Design
        {
            System.Drawing.Color _myPanelBackColor;
            System.Drawing.Color _myForeColor;
            System.Drawing.Color _myBackColor;

            public System.Drawing.Color myPanelBackColor
            {
                get { return _myPanelBackColor; }
                set { _myPanelBackColor = value; }
            }

            public System.Drawing.Color myForeColor
            {
                get { return _myForeColor; }
                set { _myForeColor = value; }
            }

            public System.Drawing.Color myBackColor
            {
                get { return _myBackColor; }
                set { _myBackColor = value; }
            }
        }

        protected GlobalDesign()
        {
            // grey design
            Design greySetup = new Design();
            greySetup.myPanelBackColor = System.Drawing.Color.DimGray;
            greySetup.myForeColor = System.Drawing.Color.LightGray;
            greySetup.myBackColor = System.Drawing.Color.Gray;
            _Designs.Add("grey", greySetup);

            // white design
            Design whiteSetup = new Design();
            whiteSetup.myPanelBackColor = System.Drawing.Color.White;
            whiteSetup.myForeColor = System.Drawing.Color.Black;
            whiteSetup.myBackColor = System.Drawing.Color.LightBlue;
            _Designs.Add("white", whiteSetup);

            _allGeneratedUserControls.Add(this);
            //ToolStripManager.Renderer = new ModeliChartToolStripRenderer(new ModeliChartColorGrey());
        }
    }
}
