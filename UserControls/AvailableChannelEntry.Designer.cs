﻿namespace ModeliChart.UserControls
{
    partial class AvailableChannelEntry
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                openHand.Dispose();
                grabbingHand.Dispose();
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lTitle = new System.Windows.Forms.Label();
            this.lDescription = new System.Windows.Forms.Label();
            this.picSettable = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lType = new System.Windows.Forms.Label();
            this.lUnit = new System.Windows.Forms.Label();
            this.lName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picSettable)).BeginInit();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lTitle
            // 
            this.lTitle.AutoSize = true;
            this.lTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTitle.Location = new System.Drawing.Point(6, 25);
            this.lTitle.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lTitle.Name = "lTitle";
            this.lTitle.Size = new System.Drawing.Size(436, 63);
            this.lTitle.TabIndex = 1;
            this.lTitle.Text = "Title";
            this.lTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragSource_MouseDown);
            // 
            // lDescription
            // 
            this.lDescription.AutoSize = true;
            this.lDescription.Dock = System.Windows.Forms.DockStyle.Top;
            this.lDescription.Location = new System.Drawing.Point(6, 93);
            this.lDescription.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lDescription.Name = "lDescription";
            this.lDescription.Size = new System.Drawing.Size(436, 25);
            this.lDescription.TabIndex = 2;
            this.lDescription.Text = "Description";
            this.lDescription.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragSource_MouseDown);
            // 
            // picSettable
            // 
            this.picSettable.BackgroundImage = global::ModeliChart.UserControls.Properties.Resources.edit_48;
            this.picSettable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picSettable.Dock = System.Windows.Forms.DockStyle.Right;
            this.picSettable.Location = new System.Drawing.Point(454, 31);
            this.picSettable.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.picSettable.Name = "picSettable";
            this.picSettable.Size = new System.Drawing.Size(58, 56);
            this.picSettable.TabIndex = 3;
            this.picSettable.TabStop = false;
            this.picSettable.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragSource_MouseDown);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.AutoSize = true;
            this.tableLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.Controls.Add(this.lTitle, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.picSettable, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.lDescription, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.lName, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.lType, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.lUnit, 1, 2);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel.Size = new System.Drawing.Size(518, 154);
            this.tableLayoutPanel.TabIndex = 4;
            this.tableLayoutPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragSource_MouseDown);
            // 
            // lType
            // 
            this.lType.AutoSize = true;
            this.lType.Dock = System.Windows.Forms.DockStyle.Right;
            this.lType.Location = new System.Drawing.Point(512, 0);
            this.lType.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lType.Name = "lType";
            this.lType.Size = new System.Drawing.Size(0, 25);
            this.lType.TabIndex = 4;
            // 
            // lUnit
            // 
            this.lUnit.AutoSize = true;
            this.lUnit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lUnit.Location = new System.Drawing.Point(462, 93);
            this.lUnit.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lUnit.Name = "lUnit";
            this.lUnit.Size = new System.Drawing.Size(50, 61);
            this.lUnit.TabIndex = 5;
            this.lUnit.Text = "Unit";
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lName.Location = new System.Drawing.Point(16, 0);
            this.lName.Margin = new System.Windows.Forms.Padding(16, 0, 6, 0);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(426, 25);
            this.lName.TabIndex = 0;
            this.lName.Text = "Full Name";
            this.lName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragSource_MouseDown);
            // 
            // AvailableChannelEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MinimumSize = new System.Drawing.Size(4, 154);
            this.Name = "AvailableChannelEntry";
            this.Size = new System.Drawing.Size(518, 154);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DragSource_MouseDown);
            this.MouseEnter += new System.EventHandler(this.Description_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.Description_MouseLeave);
            ((System.ComponentModel.ISupportInitialize)(this.picSettable)).EndInit();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lTitle;
        private System.Windows.Forms.Label lDescription;
        private System.Windows.Forms.PictureBox picSettable;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label lType;
        private System.Windows.Forms.Label lUnit;
        private System.Windows.Forms.Label lName;
    }
}
