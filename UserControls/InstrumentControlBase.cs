﻿using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    /// <summary>
    /// Pay attention to the virtual voids that have to be overridden.
    /// </summary>
    public partial class InstrumentControlBase : UserControl
    {
        protected IChannel channel;
        protected readonly ISimulation simulation;
        protected readonly IDataRepository dataRepository;

        public virtual double DisplayedInterval { get; set; }

        public virtual IEnumerable<IChannel> Channels => new IChannel[] { channel };

        // Konstruktor
        public InstrumentControlBase(ISimulation simulation, IDataRepository dataRepository)
        {
            InitializeComponent();
            this.simulation = simulation;
            this.dataRepository = dataRepository;
            // To let it look right from the start
            RefreshSetterControls();
            RefreshToolbar();
        }

        #region ISettable Methods & Events

        // int and double Channels
        private async void BtPlus_Click(object sender, EventArgs e)
        {
            if (channel.Settable)
            {
                double value = dataRepository.GetValues(channel)
                    .LastOrDefault()
                    .Value;
                switch (channel.ChannelType)
                {
                    case ChannelType.Real:
                        value += 0.1;
                        break;
                    case ChannelType.Integer:
                        value += 1;
                        break;
                }
                await simulation.SetValue(channel, value);
            }
        }
        private async void BtMinus_Click(object sender, EventArgs e)
        {
            if (channel.Settable)
            {
                double value = dataRepository.GetValues(channel)
                    .LastOrDefault()
                    .Value;
                switch (channel.ChannelType)
                {
                    case ChannelType.Real:
                        value -= 0.1;
                        break;
                    case ChannelType.Integer:
                        value -= 1;
                        break;
                }
                await simulation.SetValue(channel, value);
            }
        }

        // int and double Channels by Text
        private void TxtBoxSetValue_Click(object sender, EventArgs e)
        {
            if (txtBoxSetValue.Text.Equals("Set value"))
            {
                // Start editing
                txtBoxSetValue.ForeColor = SystemColors.WindowText;
                txtBoxSetValue.Text = "";
            }
        }
        private void TxtBoxSetValue_KeyDown(object sender, KeyEventArgs e)
        {
            // Check if Enter is pressed
            if (e.KeyCode == Keys.Enter)
            {
                // Check for empty value
                if (txtBoxSetValue.Text.Equals(""))
                {
                    // Start editing
                    txtBoxSetValue.ForeColor = SystemColors.GrayText;
                    txtBoxSetValue.Text = "Set value";
                }
                else
                {
                    // To parse correctly change all "." to ","
                    txtBoxSetValue.Text = txtBoxSetValue.Text.Replace(".", ",");
                    // Try to convert the Value to the Channel
                    if (channel.Settable)
                    {
                        if (double.TryParse(txtBoxSetValue.Text, out double value))
                        {
                            simulation.SetValue(channel, value);
                        }
                    }
                }
            }
        }

        // EnumerationChannels
        private void ToolStripSplitButtonEnum_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            // Only if settable
            if (channel != null && channel.Settable && channel.ChannelType == ChannelType.Enum)
            {
                // Retrieves the clicked Items and sets it as current channel
                int i = toolStripSplitButtonChannels.DropDownItems.IndexOf(e.ClickedItem);
                // Assume Index 1,2,3....
                simulation.SetValue(channel, i + 1);
                // Rename Enumeration
                this.toolStripSplitButtonChannels.Text = "Current: " + e.ClickedItem.Text;
            }
        }
        #endregion


        #region Render Setter Controls

        /// <summary>
        /// This will show the Setter-Controls for int and double channels, when needed
        /// </summary>
        protected void RefreshSetterControls()
        {
            if (channel == null)
            {
                panelSetter.Hide();
            }
            else
            {
                // Check if the Channel is Settable
                // EnumartionChannels Setter Controls are Set in RefreshToolbar() so only set if not enum type
                if (!(channel.ChannelType == ChannelType.Enum) & channel.Settable)
                {
                    // Show the Setter Controls
                    panelSetter.Show();
                }
                else
                {
                    // Hide the Setter Controls
                    panelSetter.Hide();
                }
            }
        }
        /// <summary>
        /// This will show the Toolbar for EnumControls, when needed
        /// </summary>
        protected void RefreshToolbar()
        {
            // Clear the items
            toolStripSplitButtonChannels.DropDownItems.Clear();
            // If it is an EnumerationChannel
            if (channel != null && channel.ChannelType == ChannelType.Enum)
            {
                // Set the Text
                toolStripSplitButtonChannels.Text = "EnumerationChannels";
                toolStripSplitButtonChannels.Visible = true;
                // Short helper Channel
                IEnumerationChannel enumChannel = channel as IEnumerationChannel;
                // Add each item of the dictionary at it's index spot assumin index counts from 1,2,3...
                for (int i = 1; i <= enumChannel.Entries.Count; i++)
                {
                    toolStripSplitButtonChannels.DropDownItems.Add(enumChannel.Entries[i]);
                }
            }
            // no EnumerationChannel
            else
            {
                // Reset Name
                toolStripSplitButtonChannels.Text = "Channels";
                toolStripSplitButtonChannels.Visible = false;
            }
        }

        #endregion

        /// <summary>
        /// Refresh the data view.
        /// </summary>
        public virtual void InvalidateInstrument(double currentTime)
        {
            throw new NotImplementedException();
        }
    }
}
