﻿namespace ModeliChart.UserControls
{
    // Grant the easy access
    public enum InstrumentType
    {
        OneChannelInstrument,
        MultiChannelInstrument,
        TwoChannelInstrument
    }
}