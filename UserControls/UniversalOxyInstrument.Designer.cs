﻿namespace ModeliChart.UserControls
{
    partial class UniversalOxyInstrument
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {       
            this.toolStripUniversal = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButtonPlotSettingsUniversal = new System.Windows.Forms.ToolStripDropDownButton();
            this.displayModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oneChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.twoChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButtonSwapAxis = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSweep = new System.Windows.Forms.ToolStripButton();
            this.panelToolBar.SuspendLayout();
            this.toolStripUniversal.SuspendLayout();
            this.SuspendLayout();
            // 
            // plot
            // 
            this.plotView.Size = new System.Drawing.Size(471, 204);
            // 
            // panelToolBar
            // 
            this.panelToolBar.Controls.Add(this.toolStripUniversal);
            this.panelToolBar.Size = new System.Drawing.Size(545, 23);
            this.panelToolBar.Controls.SetChildIndex(this.toolStripUniversal, 0);
            // 
            // toolStripUniversal
            // 
            this.toolStripUniversal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripUniversal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButtonPlotSettingsUniversal,
            this.toolStripButtonSwapAxis,
            this.toolStripButtonRefresh,
            this.toolStripButtonSweep});
            this.toolStripUniversal.Location = new System.Drawing.Point(0, 0);
            this.toolStripUniversal.Name = "toolStripUniversal";
            this.toolStripUniversal.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripUniversal.Size = new System.Drawing.Size(545, 23);
            this.toolStripUniversal.TabIndex = 21;
            this.toolStripUniversal.Text = "toolStripUniversal";
            // 
            // toolStripDropDownButtonPlotSettingsUniversal
            // 
            this.toolStripDropDownButtonPlotSettingsUniversal.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripDropDownButtonPlotSettingsUniversal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButtonPlotSettingsUniversal.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayModeToolStripMenuItem});
            this.toolStripDropDownButtonPlotSettingsUniversal.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButtonPlotSettingsUniversal.MergeAction = System.Windows.Forms.MergeAction.MatchOnly;
            this.toolStripDropDownButtonPlotSettingsUniversal.Name = "toolStripDropDownButtonPlotSettingsUniversal";
            this.toolStripDropDownButtonPlotSettingsUniversal.Size = new System.Drawing.Size(13, 20);
            this.toolStripDropDownButtonPlotSettingsUniversal.Text = "Settings";
            this.toolStripDropDownButtonPlotSettingsUniversal.ToolTipText = "Settings";
            // 
            // displayModeToolStripMenuItem
            // 
            this.displayModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oneChannelToolStripMenuItem,
            this.multiChannelToolStripMenuItem,
            this.twoChannelToolStripMenuItem});
            this.displayModeToolStripMenuItem.Name = "displayModeToolStripMenuItem";
            this.displayModeToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.displayModeToolStripMenuItem.Text = "Display Mode";
            // 
            // oneChannelToolStripMenuItem
            // 
            this.oneChannelToolStripMenuItem.Name = "oneChannelToolStripMenuItem";
            this.oneChannelToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.oneChannelToolStripMenuItem.Text = "One Channel";
            this.oneChannelToolStripMenuItem.Click += new System.EventHandler(this.OneChannelToolStripMenuItem_Click);
            // 
            // multiChannelToolStripMenuItem
            // 
            this.multiChannelToolStripMenuItem.Name = "multiChannelToolStripMenuItem";
            this.multiChannelToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.multiChannelToolStripMenuItem.Text = "Multi Channel";
            this.multiChannelToolStripMenuItem.Click += new System.EventHandler(this.MultiChannelToolStripMenuItem_Click);
            // 
            // twoChannelToolStripMenuItem
            // 
            this.twoChannelToolStripMenuItem.Name = "twoChannelToolStripMenuItem";
            this.twoChannelToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.twoChannelToolStripMenuItem.Text = "Two Channel";
            this.twoChannelToolStripMenuItem.Click += new System.EventHandler(this.TwoChannelToolStripMenuItem_Click);
            // 
            // toolStripButtonSwapAxis
            // 
            this.toolStripButtonSwapAxis.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSwapAxis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSwapAxis.Image = global::ModeliChart.UserControls.Properties.Resources.swap_axis;
            this.toolStripButtonSwapAxis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSwapAxis.MergeIndex = 1;
            this.toolStripButtonSwapAxis.Name = "toolStripButtonSwapAxis";
            this.toolStripButtonSwapAxis.Size = new System.Drawing.Size(23, 20);
            this.toolStripButtonSwapAxis.Text = "Swap Axis";
            this.toolStripButtonSwapAxis.Click += new System.EventHandler(this.ToolStripButtonSwapAxis_Click);
            // 
            // toolStripButtonRefresh
            // 
            this.toolStripButtonRefresh.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonRefresh.Image = global::ModeliChart.UserControls.Properties.Resources.refresh;
            this.toolStripButtonRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonRefresh.MergeIndex = 2;
            this.toolStripButtonRefresh.Name = "toolStripButtonRefresh";
            this.toolStripButtonRefresh.Size = new System.Drawing.Size(23, 20);
            this.toolStripButtonRefresh.Text = "Refresh";
            this.toolStripButtonRefresh.Click += new System.EventHandler(this.ToolStripButtonRefresh_Click);
            // 
            // toolStripButtonSweep
            // 
            this.toolStripButtonSweep.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButtonSweep.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSweep.Image = global::ModeliChart.UserControls.Properties.Resources.sweep_mode;
            this.toolStripButtonSweep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSweep.Name = "toolStripButtonSweep";
            this.toolStripButtonSweep.Size = new System.Drawing.Size(23, 20);
            this.toolStripButtonSweep.Text = "Sweep";
            this.toolStripButtonSweep.Click += new System.EventHandler(this.ToolStripButtonSweep_Click);
            // 
            // UniversalOxyInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "UniversalOxyInstrument";
            this.Size = new System.Drawing.Size(547, 241);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.DragTarget_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.DragTarget_DragEnter);
            this.DragOver += new System.Windows.Forms.DragEventHandler(this.DragTarget_DragOver);
            this.DragLeave += new System.EventHandler(this.DragTarget_DragLeave);
            this.panelToolBar.ResumeLayout(false);
            this.panelToolBar.PerformLayout();
            this.toolStripUniversal.ResumeLayout(false);
            this.toolStripUniversal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButtonPlotSettingsUniversal;
        private System.Windows.Forms.ToolStripMenuItem displayModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oneChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiChannelToolStripMenuItem;
        protected System.Windows.Forms.ToolStrip toolStripUniversal;
        private System.Windows.Forms.ToolStripButton toolStripButtonSwapAxis;
        private System.Windows.Forms.ToolStripButton toolStripButtonRefresh;
        private System.Windows.Forms.ToolStripMenuItem twoChannelToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonSweep;




    }
}
