﻿using System;
using System.Windows.Forms;
using ModeliChart.Basics;
using ModeliChart.Log;
using System.Threading;

namespace ModeliChart.UserControls
{
    public partial class OxyOverlay : UserControl
    {

        public OxyOverlay()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Will be raised when a Channel has been dropped
        /// </summary>

        public event EventHandler<ChannelDroppedArgs> ChannelDropped;

        public void ShowOverlay(Control parent, InstrumentType oldType)
        {
            Parent = parent;
            Left = (parent.Width / 2) - (Width / 2);
            Top = (parent.Height / 2) - (Height / 2);
            BringToFront();
            Show();
        }

        private void pnlOne_DragDrop(object sender, DragEventArgs e)
        {
            IChannel channel = e.Data.GetData(e.Data.GetFormats()[0]) as IChannel;
            Hide();
            ChannelDroppedArgs args = new ChannelDroppedArgs(channel, InstrumentType.OneChannelInstrument);
            ChannelDropped(this, args);
        }

        private void pnlOne_DragEnter(object sender, DragEventArgs e)
        {
            IChannel channel = e.Data.GetData(e.Data.GetFormats()[0]) as IChannel;
            if (channel != null)
            {
                e.Effect = DragDropEffects.Copy; // zuweisen der möglichen Effekte
            }
        }

        private void pnlMulti_DragDrop(object sender, DragEventArgs e)
        {
            IChannel channel = e.Data.GetData(e.Data.GetFormats()[0]) as IChannel;
            Hide();
            ChannelDroppedArgs args = new ChannelDroppedArgs(channel, InstrumentType.MultiChannelInstrument);
            ChannelDropped(this, args);
        }

        private void pnlMulti_DragEnter(object sender, DragEventArgs e)
        {
            IChannel channel = e.Data.GetData(e.Data.GetFormats()[0]) as IChannel;
            if (channel != null)
            {
                e.Effect = DragDropEffects.Copy; // zuweisen der möglichen Effekte
            }
        }

        private void pnlTwo_DragDrop(object sender, DragEventArgs e)
        {
            IChannel channel = e.Data.GetData(e.Data.GetFormats()[0]) as IChannel;
            Hide();
            ChannelDroppedArgs args = new ChannelDroppedArgs(channel, InstrumentType.TwoChannelInstrument);
            ChannelDropped(this, args);
        }

        private void pnlTwo_DragEnter(object sender, DragEventArgs e)
        {
            IChannel channel = e.Data.GetData(e.Data.GetFormats()[0]) as IChannel;
            if (channel != null)
            {
                e.Effect = DragDropEffects.Copy; // zuweisen der möglichen Effekte
            }
        }
    }

    public class ChannelDroppedArgs : EventArgs
    {
        private IChannel _channel;
        private InstrumentType _type;

        public ChannelDroppedArgs(IChannel channel, InstrumentType instrumentType)
        {
            _channel = channel;
            _type = instrumentType;
        }

        public IChannel Channel
        {
            get { return _channel; }
        }

        public InstrumentType InstrumentType
        {
            get { return _type; }
        }
    }
}
