﻿using Aga.Controls.Tree;
using ModeliChart.Basics;
using System;
using System.Drawing;
using System.Linq;

namespace ModeliChart.UserControls
{
    class ChannelNode : Node
    {
        private readonly ISimulation simulation;
        private readonly IDataRepository dataRepository;
        public IChannel Channel { get; }

        public ChannelNode(IChannel channel, ISimulation simulation, IDataRepository dataRepository)
        {
            Channel = channel;
            this.simulation = simulation;
            this.dataRepository = dataRepository;
        }

        // Extracts the title from the Name
        public String Title => VariableNameParser.ParseVariableTitle(Channel.Name);

        public string Name => Channel.Name;

        public string Description => Channel.Description ?? "";

        public string DisplayedUnit => Channel.DisplayedUnit ?? "";

        public string DataSourceName => Channel.ModelInstanceName ?? "";

        public string Value
        {
            get => dataRepository?.GetValues(Channel)
                .LastOrDefault()
                .Value
                .ToString() ?? "";
            set
            {
                if (Double.TryParse(value, out double parsedValue))
                {
                    simulation?.SetValue(Channel, parsedValue);
                }
            }
        }

        public Image Icon
        {
            get
            {
                if (Channel is IChannel)
                {
                    if (Channel.Settable)
                    {
                        return Properties.Resources.edit_16;
                    }
                    else
                    {
                        return Properties.Resources.eye_16;
                    }

                }
                else
                {
                    return null;
                }
            }
        }

        public bool Enabled
        {
            get => Channel.Enabled;
            set
            {
                Channel.Enabled = value;
                // Check the lower ones too
                foreach (ChannelNode current in this.Nodes)
                {
                    current.Enabled = value;
                }
            }
        }

        public new bool IsLeaf => Nodes.Count == 0;

        public bool Favorite { get; set; }
    }
}
