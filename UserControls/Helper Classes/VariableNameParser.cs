﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModeliChart.UserControls
{
    public static class VariableNameParser
    {
        /// <summary>

        /// </summary>
        /// <param name="name">The String that needs to be parsed</param>
        /// <returns>The needed splitted String, e.g.: [second, third]</returns>

        /// <summary>
        /// Splits the name of a variable from a fmu
        /// Takes care of der(*) 
        /// -> e.g.: name = der(first.second.third) -> {"first","second","der(third)"}
        /// </summary>
        /// <param name="name">Full name to be splitted</param>
        /// <returns></returns>
        public static String[] SplitVariableName(String name)
        {
            bool der = false;
            // Check for the der(***) , remove it and add it to the last part of SplitString         
            if (name.StartsWith("der("))
            {
                // Remove der(
                name = name.Substring(4, name.Length - 5);
                der = true;
            }
            // Now Split it to the wanted format
            String[] result = name.Split('.');
            // If needed add the der again at the right position
            if (der)
            {
                result[result.Length - 1] = "der(" + result[result.Length - 1] + ")";
            }
            // return the rsult
            return result;
        }

        /// <summary>
        /// Returns the title of the actual variable without its "folder-names"
        /// Takes care of der(*)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static String ParseVariableTitle(String name)
        {
            String[] split = SplitVariableName(name);
            if (split.Length > 0)
            {
                return split[split.Length - 1];
            }
            else
            {
                return "Error";
            }
        }

        /// <summary>
        /// Removes first part of array: {"first","second","third"}-> {"second","third"}
        /// </summary>
        /// <param name="splitString"></param>
        /// <returns></returns>
        public static String[] CutFirst(String[] splitString)
        {
            if (splitString.Length > 0)
            {
                String[] res = new String[splitString.Length - 1];
                for (int i = 0; i < splitString.Length - 1; i++ )
                {
                    res[i] = splitString[i + 1];
                }
                return res;
            }
            else
            {
                // Return empty array
                return new String[] {};
            }
        }
    }
}
