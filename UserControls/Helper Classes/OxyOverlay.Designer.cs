﻿namespace ModeliChart.UserControls
{
    partial class OxyOverlay
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OxyOverlay));
            this.pnlBase = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlOne = new System.Windows.Forms.Panel();
            this.pnlMulti = new System.Windows.Forms.Panel();
            this.pnlTwo = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnlBase.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBase
            // 
            this.pnlBase.AutoSize = true;
            this.pnlBase.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlBase.Controls.Add(this.pnlOne);
            this.pnlBase.Controls.Add(this.pnlTwo);
            this.pnlBase.Controls.Add(this.pnlMulti);
            this.pnlBase.Location = new System.Drawing.Point(3, 16);
            this.pnlBase.Name = "pnlBase";
            this.pnlBase.Size = new System.Drawing.Size(228, 76);
            this.pnlBase.TabIndex = 4;
            // 
            // pnlOne
            // 
            this.pnlOne.AllowDrop = true;
            this.pnlOne.BackgroundImage = global::ModeliChart.UserControls.Properties.Resources.OneChannelInstrument;
            this.pnlOne.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlOne.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOne.Location = new System.Drawing.Point(3, 3);
            this.pnlOne.Name = "pnlOne";
            this.pnlOne.Size = new System.Drawing.Size(70, 70);
            this.pnlOne.TabIndex = 3;
            this.pnlOne.DragDrop += new System.Windows.Forms.DragEventHandler(this.pnlOne_DragDrop);
            this.pnlOne.DragEnter += new System.Windows.Forms.DragEventHandler(this.pnlOne_DragEnter);
            // 
            // pnlMulti
            // 
            this.pnlMulti.AllowDrop = true;
            this.pnlMulti.BackgroundImage = global::ModeliChart.UserControls.Properties.Resources.MultiChannelInstrument;
            this.pnlMulti.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMulti.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMulti.Location = new System.Drawing.Point(155, 3);
            this.pnlMulti.Name = "pnlMulti";
            this.pnlMulti.Size = new System.Drawing.Size(70, 70);
            this.pnlMulti.TabIndex = 5;
            this.pnlMulti.DragDrop += new System.Windows.Forms.DragEventHandler(this.pnlMulti_DragDrop);
            this.pnlMulti.DragEnter += new System.Windows.Forms.DragEventHandler(this.pnlMulti_DragEnter);
            // 
            // pnlTwo
            // 
            this.pnlTwo.AllowDrop = true;
            this.pnlTwo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlTwo.BackgroundImage")));
            this.pnlTwo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTwo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTwo.Location = new System.Drawing.Point(79, 3);
            this.pnlTwo.Name = "pnlTwo";
            this.pnlTwo.Size = new System.Drawing.Size(70, 70);
            this.pnlTwo.TabIndex = 6;
            this.pnlTwo.DragDrop += new System.Windows.Forms.DragEventHandler(this.pnlTwo_DragDrop);
            this.pnlTwo.DragEnter += new System.Windows.Forms.DragEventHandler(this.pnlTwo_DragEnter);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(122, 13);
            this.lblTitle.TabIndex = 5;
            this.lblTitle.Text = "Select the display mode:";
            // 
            // OxyOverlay
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.pnlBase);
            this.Controls.Add(this.lblTitle);
            this.Name = "OxyOverlay";
            this.Size = new System.Drawing.Size(234, 95);
            this.pnlBase.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel pnlBase;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnlOne;
        private System.Windows.Forms.Panel pnlMulti;
        private System.Windows.Forms.Panel pnlTwo;

    }
}
