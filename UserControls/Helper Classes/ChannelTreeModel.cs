﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aga.Controls.Tree;
using ModeliChart.Basics;

namespace ModeliChart.UserControls
{
    class ChannelTreeModel : TreeModel, ITreeModel
    {
        // To hide the expand "+"
        public new bool IsLeaf(TreePath treePath)
        {
            ChannelNode node = treePath.LastNode as ChannelNode;
            // Dummy Channels are branches
            if (node?.Channel is IChannel)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
