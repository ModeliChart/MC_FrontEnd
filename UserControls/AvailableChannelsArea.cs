﻿using Aga.Controls.Tree;
using Aga.Controls.Tree.NodeControls;
using ModeliChart.Basics;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    public partial class AvailableChannelsArea : UserControl
    {
        private NodeTextBox valueBox;
        private NodeCheckBox checkBox;
        // private NodeControlFavorite favoriteControl;
        // Model for the Tree
        private TreeModel treeModel;

        public AvailableChannelsArea()
        {
            InitializeComponent();
            // Create the model for the tree
            treeModel = new ChannelTreeModel();
            treeView.Model = treeModel;
            // Initialize Checkbox
            checkBox = new NodeCheckBox("Enabled")
            {
                EditEnabled = true,
                ParentColumn = treeColumnInfo
            };
            // Initialize the Values field
            valueBox = new NodeTextBox
            {
                EditEnabled = true,
                EditOnClick = true,
                ParentColumn = treeColumnValue,
                DataPropertyName = "Value"
            };
        }

        private bool presetsMode = false;
        /// <summary>
        /// True enables the CheckBoxes and displays all the Channels.
        /// False hides CheckBoxes and disabled Channels.
        /// </summary>
        public bool PresetMode
        {
            get => presetsMode;
            set
            {
                presetsMode = value;
                if (value == true)
                {
                    chkAll.Show();
                    treeView.NodeControls.Insert(0, checkBox);
                    checkBox.EditEnabled = true;
                    treeView.NodeControls.Remove(valueBox);
                }
                else
                {
                    chkAll.Hide();
                    treeView.NodeControls.Remove(checkBox);
                    treeView.NodeControls.Insert(0, valueBox);
                }
            }
        }

        private IEnumerable<IChannel> availableChannels;

        public IEnumerable<IChannel> AvailableChannels
        {
            get => availableChannels;
            set
            {
                availableChannels = value;
                RefreshShownChannels();
            }
        }

        public ISimulation Simulation { get; set; }

        public IDataRepository DataRepository { get; set; }

        #region Tree Population

        /// <summary>
        /// This will show all the Channels found in the DataSource, and keep them in a TreeView
        /// Repopulates the tree
        /// </summary>
        public void RefreshShownChannels()
        {
            // Begin the refresh
            treeView.BeginUpdate();
            // Clear Up the old Data
            treeModel.Nodes.Clear();
            // Different filling behaviour on preset mode and regular mode
            if (PresetMode)
            {
                foreach (IChannel current in AvailableChannels)
                {
                    // Insert Entry in TreeView
                    InsertChannelEntryInTreeView(current);
                }
                // Check if folder nodes are enabled
                foreach (ChannelNode current in treeModel.Nodes)
                {
                    CheckFolderEnabledRecursive(current);
                }
            }
            else // Regular mode
            {
                if (AvailableChannels != null)
                {
                    foreach (IChannel current in AvailableChannels)
                    {
                        // only Enabled channels shall be displayed if no preset mode
                        if (current.Enabled)
                        {
                            // Insert Entry in TreeView
                            InsertChannelEntryInTreeView(current);
                        }
                    }
                }
            }
            // End the refresh
            treeView.EndUpdate();
        }

        /// <summary>
        /// This method will parse the name and will add it in the right spot of the TreeView
        /// </summary>
        private void InsertChannelEntryInTreeView(IChannel channel)
        {
            // Split the String into the needed pieces
            String[] subName = VariableNameParser.SplitVariableName(channel.Name);

            // Ignore this case
            if (subName.Length < 1)
            {
                return;
            }

            // Insert
            if (subName.Length == 1)
            {
                // Create a new ChanelNode
                ChannelNode entry = new ChannelNode(channel, Simulation, DataRepository);
                treeModel.Nodes.Add(entry);
                // Stop inserting!
                return;
            }

            // Go deeper
            if (subName.Length > 1)
            {
                // Get the current "folder name"
                String currentName = subName[0];
                // Create a new subName
                subName = VariableNameParser.CutFirst(subName);
                // Try to find the folder node
                foreach (ChannelNode currentNode in treeModel.Nodes)
                {
                    // Check if the Name of the Node equals the current first part of the name
                    if (currentName.Equals(currentNode.Name))
                    {
                        // Insert in this node
                        InsertChannelInNode(currentNode, channel, subName);
                        // BREAK
                        return;
                    }
                }
                // no folder found -> create a new one
                IChannel helperChannel = new Channel(currentName, 0, "", false).WithModelInstanceName("");
                ChannelNode helperNode = new ChannelNode(helperChannel, Simulation, DataRepository);
                treeModel.Nodes.Add(helperNode);
                // Now insert the Channel under this node, this is Root -> 1
                InsertChannelInNode(helperNode, channel, subName);
            }
        }

        /// <summary>
        /// Recursiv parse through subnodes to find the right spot for insertion
        /// </summary>
        /// <param name="node"></param>
        /// <param name="channel"></param>
        /// <param name="subName"> Sub</param>
        private void InsertChannelInNode(ChannelNode topNode, IChannel channel, String[] subName)
        {
            // Ignore this case
            if (subName.Length < 1)
            {
                return;
            }
            // Insert
            if (subName.Length == 1)
            {
                // Create a new ChanelNode
                ChannelNode entry = new ChannelNode(channel, Simulation, DataRepository);
                topNode.Nodes.Add(entry);
                // Stop inserting!
                return;
            }
            // Go deeper
            if (subName.Length > 1)
            {
                // Get the current "folder name"
                String currentName = subName[0];
                // Create a new subName
                subName = VariableNameParser.CutFirst(subName);
                // Try to find the folder node
                foreach (ChannelNode currentNode in topNode.Nodes)
                {
                    // Check if the Name of the Node equals the current first part of the name
                    if (currentName.Equals(currentNode.Name))
                    {
                        // Insert in this node
                        InsertChannelInNode(currentNode, channel, subName);
                        // BREAK
                        return;
                    }
                }
                // no folder found -> create a new one
                IChannel helperChannel = new Channel(currentName, 0, "", false);
                // Add the Dummy to the DataSource
                ChannelNode helperNode = new ChannelNode(helperChannel, Simulation, DataRepository);
                topNode.Nodes.Add(helperNode);
                // Now insert the Channel under this node, this is Root -> 1
                InsertChannelInNode(helperNode, channel, subName);
            }
        }

        /// <summary>
        /// Will disable folder if all of its subnodes are disabled
        /// </summary>
        public void CheckFoldersEnabled()
        {
            // Check if folder nodes are enabled
            foreach (ChannelNode current in treeModel.Nodes)
            {
                CheckFolderEnabledRecursive(current);
            }
        }

        /// <summary>
        /// This will check the node states of the DummyNodes
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private bool CheckFolderEnabledRecursive(ChannelNode node)
        {
            IChannel channelDescription = node.Channel;
            // Break condition: Leaf
            if (node.IsLeaf)
            {
                return channelDescription.Enabled;
            }
            else
            {
                bool enabled = false;
                foreach (ChannelNode current in node.Nodes)
                {
                    // Only one needs to be enabled to have the need to display the top node
                    if (CheckFolderEnabledRecursive(current)) { enabled = true; }
                }
                channelDescription.Enabled = enabled;
                return enabled;
            }
        }

        /// <summary>
        /// Will set the enabled attribute of all the channels.
        /// </summary>
        /// <param name="state"></param>
        public void SetAllEnabledStates(bool state)
        {
            // Loop top branch
            foreach (ChannelNode current in treeModel.Nodes)
            {
                SetEnabledStatesRecursive(current, state);
            }
            RefreshShownChannels();
        }

        private void SetEnabledStatesRecursive(ChannelNode node, bool state)
        {
            IChannel channel = node.Channel;
            // Break condition: Leaf
            foreach (ChannelNode current in node.Nodes)
            {
                if (!node.IsLeaf)
                {
                    // Go deeper ;P
                    SetEnabledStatesRecursive(current, state);
                }
            }
            channel.Enabled = state;
        }

        public void SetSettableEnabled(bool state)
        {
            // Top level
            foreach (ChannelNode current in treeModel.Nodes)
            {
                SetSettableEnabledRecursive(current, state);
            }
            RefreshShownChannels();
        }

        private void SetSettableEnabledRecursive(ChannelNode node, bool state)
        {
            IChannel channel = node.Channel;
            if (!node.IsLeaf)
            {
                // Next recursion
                foreach (ChannelNode current in node.Nodes)
                {
                    SetSettableEnabledRecursive(current, state);
                }
            }
            else
            {
                // End of recursion
            }
            if (channel.Settable)
            {
                channel.Enabled = state;
            }
            else
            {
                channel.Enabled = false;
            }
        }
        #endregion

        #region UI events: Drag & Selection

        private void TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            ChannelNode node = (e.Item as TreeNodeAdv[])[0].Tag as ChannelNode;
            // Must be an IChannel
            if ((node?.Channel is IChannel))
            {
                treeView.DoDragDrop(node.Channel, DragDropEffects.Copy);
            }
            else
            {
                treeView.DoDragDrop(e.Item, DragDropEffects.None);
            }
        }

        private void TreeView_SelectionChanged(object sender, EventArgs e)
        {
            if (treeView.SelectedNode != null)
            {
                ChannelNode node = treeView.SelectedNode.Tag as ChannelNode;
                // Set the details
                _availableChannelEntry.Channel = node?.Channel;
            }
        }

        private void ChkAll_CheckedChanged(object sender, EventArgs e)
        {
            SetAllEnabledStates(chkAll.Checked);
        }

        private void ChkSettables_Click(object sender, EventArgs e)
        {
            SetSettableEnabled(chkSettables.Checked);
        }

        private void TreeView_SizeChanged(object sender, EventArgs e)
        {
            treeColumnInfo.Width = treeView.Width - treeColumnValue.Width - SystemInformation.VerticalScrollBarWidth;
        }

        #endregion

        #region Refresh

        bool refresh = true;   // Use to indicate if refresh is possible

        /// <summary>
        /// This will not repopulte the tree, only refresh the view to the model
        /// </summary>
        public void RefreshTreeView()
        {
            if (refresh)
            {
                treeView.FullUpdate();
            }
        }

        private void TreeView_NodeMouseClick(object sender, TreeNodeAdvMouseEventArgs e)
        {
            refresh = false;
        }

        private void TreeView_MouseLeave(object sender, EventArgs e)
        {
            // Get the Rectangle of the tree
            var globalRect = treeView.ClientRectangle;
            globalRect.Width -= SystemInformation.VerticalScrollBarWidth;
            globalRect.Height -= 6;
            globalRect = this.RectangleToScreen(globalRect);
            globalRect.Y += 3;

            refresh = !globalRect.Contains(MousePosition);
        }

        private void TreeView_KeyUp(object sender, KeyEventArgs e)
        {
            refresh = true;
        }

        #endregion
    }
}
