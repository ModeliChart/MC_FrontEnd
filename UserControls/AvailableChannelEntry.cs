﻿using ModeliChart.Basics;
using System;
using System.Windows.Forms;

namespace ModeliChart.UserControls
{
    public partial class AvailableChannelEntry : UserControl, IDraggable
    {
        private IChannel channel;
        private readonly Cursor grabbingHand;
        private readonly Cursor openHand;
        private ChannelType _channelType = ChannelType.None;


        public AvailableChannelEntry()
        {
            InitializeComponent();
            grabbingHand = new Cursor(GetType(), "Resources.Grabbed15-19.cur");
            openHand = new Cursor(GetType(), "Resources.openhand.cur");
        }

        public AvailableChannelEntry(IChannel channel)
            : this()
        {
            Channel = channel;
        }

        /// <summary>
        /// Get or set the displayed channel
        /// </summary>
        public IChannel Channel
        {
            get
            {
                return channel;
            }
            set
            {
                if (value != null)
                {
                    ChannelName = value.Name;
                    ChannelDescription = value.Description;
                    channel = value;
                    DisplayChannelType = value.ChannelType;
                    Settable = value.Settable;
                    Unit = value.DisplayedUnit;
                }
            }
        }

        #region Label public acces
        /// <summary>
        /// SET will only change the displayed Text not the channel itself
        /// </summary>
        public string ChannelName
        {
            get { return lName.Text; }
            set
            {
                lName.Text = value;
                lTitle.Text = VariableNameParser.ParseVariableTitle(value);
            }
        }

        /// <summary>
        /// SET will only change the displayed Text not the channel itself
        /// </summary>
        public string ChannelDescription
        {
            get { return lDescription.Text; }
            set { lDescription.Text = value; }
        }

        /// <summary>
        /// SET will only change the displayed icon not the channel itself
        /// </summary>
        public bool Settable
        {
            get {
                if (channel != null)
                {
                    return channel.Settable;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (value == true)
                {
                    this.picSettable.BackgroundImage = Properties.Resources.edit_48;
                }
                else
                {
                    this.picSettable.BackgroundImage = Properties.Resources.eye_48;
                }
            }
        }

        public ChannelType DisplayChannelType
        {
            get
            {
                return _channelType;
            }
            set
            {
                _channelType = value;
                switch (value)
                {
                    case ChannelType.Real:
                        lType.Text = "ℝ";
                        break;
                    case ChannelType.Integer:
                        lType.Text = "ℤ";
                        break;
                    case ChannelType.Enum:
                        lType.Text = "{}";
                        break;
                    case ChannelType.Boolean:
                        lType.Text = "I/O";
                        break;
                    default:
                        lType.Text = "";
                        break;
                }
            }
        }


        /// <summary>
        /// SET will only change the displayed icon not the channel itself
        /// </summary>
        public string Unit
        {
            get { return lUnit.Text; }
            set { lUnit.Text = value; }
        }
        #endregion

        # region Mouse & Drag Events
        private void Description_MouseEnter(object sender, EventArgs e)
        {
        }
        private void Description_MouseLeave(object sender, EventArgs e)
        {
        }
        public void DragSource_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.Hand;
        }
        public void DragSource_MouseDown(object sender, MouseEventArgs e)
        {
            if (channel != null)
            {
                this.DoDragDrop(channel, DragDropEffects.Copy);
            }
        }
        public void DragSource_MouseUp(object sender, MouseEventArgs e)
        {

        }
        public void DragSource_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = System.Windows.Forms.Cursors.Default;
        }
        #endregion

    }
}
